//
//  ReaderDocument.h
//  WireFrame
//
//  Created by Raghavender  on 10/03/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <CoreData/CoreData.h>
#import "AnnotationStore.h"

@class DocumentFolder;

@interface ReaderDocument : NSManagedObject

@property (nonatomic, strong, readwrite) NSString *guid;
@property (nonatomic, strong, readwrite) NSURL *fileURL;
@property (nonatomic, strong, readwrite) NSString *fileName;
@property (nonatomic, strong, readwrite) NSString *filePath;
@property (nonatomic, strong, readwrite) NSString *password;
@property (nonatomic, strong, readwrite) NSNumber *pageCount;
@property (nonatomic, strong, readwrite) NSNumber *pageNumber;
@property (nonatomic, strong, readwrite) NSNumber *fileSize;
@property (nonatomic, strong, readwrite) NSDate *fileDate;
@property (nonatomic, strong, readwrite) NSDate *lastOpen;
@property (nonatomic, strong, readwrite) NSData *tagData;
@property (nonatomic, strong, readwrite) NSManagedObject *folder;
@property (nonatomic, strong, readonly) NSMutableIndexSet *bookmarks;
@property (nonatomic, assign, readwrite) BOOL isChecked;

+ (ReaderDocument *)insertInMOC:(NSManagedObjectContext *)inMOC name:(NSString *)name path:(NSString *)path;
+ (void)deleteInMOC:(NSManagedObjectContext *)inMOC object:(ReaderDocument *)object fm:(NSFileManager *)fm;
+ (void)renameInMOC:(NSManagedObjectContext *)inMOC object:(ReaderDocument *)object name:(NSString *)string;
+ (NSArray *)allInMOC:(NSManagedObjectContext *)inMOC withFolder:(DocumentFolder *)object;
+ (BOOL)existsInMOC:(NSManagedObjectContext *)inMOC name:(NSString *)string;
+ (NSArray *)allInMOC:(NSManagedObjectContext *)inMOC withName:(NSString *)name;
+ (NSArray *)allInMOC:(NSManagedObjectContext *)inMOC;
+ (NSURL*) urlForAnnotatedDocument:(ReaderDocument *)document;

- (void)updateProperties;
- (void)saveReaderDocument;
- (void)saveReaderDocumentWithAnnotations;
- (BOOL)fileExistsAndValid;
- (AnnotationStore*) annotations;
@end

@interface ReaderDocument (CoreDataPrimitiveAccessors)

- (NSURL *)primitiveFileURL;
- (void)setPrimitiveFileURL:(NSURL *)url;

// new code
+ (NSString *)GUID;
+ (NSString *)relativeFilePath:(NSString *)fullFilePath;

@end










