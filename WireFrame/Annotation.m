//
//  Annotation.m
//	ThatPDF v0.3.1
//
//	Created by Brett van Zuiden.
//	Copyright © 2013 Ink. All rights reserved.
//

#import "Annotation.h"

@class Annotation;
@class PathAnnotation;

@implementation Annotation

- (void) drawInContext:(CGContextRef) context {
    //Overridden
}

@end
@implementation PathAnnotation
+ (id) pathAnnotationWithPath:(CGPathRef)path color:(CGColorRef)color fill:(BOOL)fill{
    return [PathAnnotation pathAnnotationWithPath:path color:color lineWidth:3.0 fill:fill];
}

+ (id) pathAnnotationWithPath:(CGPathRef)path color:(CGColorRef)color lineWidth:(CGFloat)width fill:(BOOL)fill {
    PathAnnotation *pa = [[PathAnnotation alloc] init];
    pa.path = path;
    pa.color = color;
    pa.lineWidth = width;
    pa.fill = fill;
    return pa;
}

- (void) drawInContext:(CGContextRef)context {
    CGContextAddPath(context, self.path);
    CGContextSetLineWidth(context, self.lineWidth);
    if (self.fill) {
        CGContextSetFillColorWithColor(context, self.color);
        CGContextFillPath(context);
    } else {
        CGContextSetStrokeColorWithColor(context, self.color);
        CGContextStrokePath(context);
    }
}
@end