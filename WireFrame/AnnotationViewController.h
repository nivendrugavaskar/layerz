//
//  AnnotationViewController.h
//	ThatPDF v0.3.1
//
//	Created by Brett van Zuiden.
//	Copyright © 2013 Ink. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ReaderDocument.h"
#import "Annotation.h"
#import "ReaderContentView.h"
#import "SPUserResizableView.h"
#import "LayersView.h"
#import "LayerIndex.h"
#import "AnnotationViewController.h"

#import "DocumentFolder.h"

#import <MessageUI/MessageUI.h>
// new code
LayerIndex *layerIndex;
// new
BOOL loadgesture;


extern NSString *const AnnotationViewControllerType_None;
extern NSString *const AnnotationViewControllerType_Sign;
extern NSString *const AnnotationViewControllerType_RedPen;
extern NSString *const AnnotationViewControllerType_Text;
extern NSString *const AnnotationViewControllerType_Layer;
extern NSString *const AnnotationViewControllerType_Hand;
extern NSString *const AnnotationViewControllerType_Eraser;

@protocol LayerCountDelegate <NSObject>

@required
- (void)getLayers:(NSMutableArray*)fechedResult;

// new code
-(void)handbuttonchange:(BOOL)right;

@end

@interface AnnotationViewController : UIViewController <UITextViewDelegate,SPUserResizableViewDelegate,UIGestureRecognizerDelegate,NSCopying,MFMailComposeViewControllerDelegate>
{
    SPUserResizableView *currentlyEditingView;
    SPUserResizableView *lastEditedView;
}
@property NSString *annotationType;
@property ReaderDocument *document;
@property NSInteger currentPage;
@property CGFloat pencilLineWidth;
@property CGFloat eraserLineWith;
@property CGFloat markerLineWidth;
@property CGColorRef markerColor;
@property CGColorRef pencilColor;
@property UIView *containerView;

@property (strong, nonatomic) NSData *annotationImageData;
//@property (strong, nonatomic) NSMutableArray *annoImageDataArray;
@property (strong, nonatomic) LayersView *layer;
@property (strong, nonatomic) UIView *arcont;
@property (strong, nonatomic) UIImageView *image;
@property (strong, nonatomic) UITextView *aTextView;
@property (strong, nonatomic) NSString *signLineWidth;
@property (strong, nonatomic) NSMutableArray *layerContainerViewArray;
@property (nonatomic, weak) id <LayerCountDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *deleteArray;
@property (strong, nonatomic) NSMutableArray *tmp;
@property (strong, nonatomic) NSMutableArray *textViewCountArray;
@property  NSInteger index;
// new code
@property (strong,nonatomic) UIButton *crossbutton;
@property (strong,nonatomic) UIView *bluredview;

// new boolean
@property BOOL checkmark,handchecktrack;


- (id) initWithDocument:(ReaderDocument *)document;
- (void) moveToPage:(int)page contentView:(ReaderContentView*) view;
- (void)saveCurrentPage:(int)page;
- (void) hide;
- (void) clear;
- (void) saveToDraft;
//newone
- (void) saveToexport;

// new code
-(void)savetoemail;
//- (void) addingTextView;
// changed the code
-(int)addingTextView;
-(void)deactivatetextbox;

- (void) layerAddToPdf;
- (AnnotationStore*) annotations;
-(void) activateTheTextBox;
-(void) incTextSize;
-(void) decTextSize;
-(void) setFontForText:(NSString*)font;
-(void) changingTextColor:(CGColorRef)_color;
// new method
-(void)sendvalue:(int)indexvalue;
-(void)dismisskeyboard;

@end

@interface SaveObject : NSObject
@property NSInteger pagNumber;
@property NSInteger numOfLayers;
@property (strong, nonatomic) NSData *imageData;
@property (strong, nonatomic) NSMutableArray *aString;
@end

@interface TextViewClass : NSObject
@property float xValue;
@property float yValue;
@property float height;
@property float width;
@property (strong, nonatomic) NSString *textViewText;



@end

