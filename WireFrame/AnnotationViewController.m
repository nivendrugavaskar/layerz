//
//  AnnotationViewController.m
//	ThatPDF v0.3.1
//
//	Created by Brett van Zuiden.
//	Copyright © 2013 Ink. All rights reserved.
//

#import "AnnotationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "LayersView.h"
#import "ReaderViewController.h"
#import "CoreDataManager.h"
#import "PdfDoc.h"
#import "PdfPage.h"
#import "PdfLayers.h"
#import "TextView.h"
#import "LayerStore.h"
#import "LayerData.h"
//#import "LayerIndex.h"
#import "TextViewIndex.h"
#import "ILTranslucentView.h"

// new one
#import "DocumentsUpdate.h"
#import "DocumentFolder.h"
#import "ReaderConstants.h"
#import "LibraryDocumentsView.h"


NSString *const AnnotationViewControllerType_None = @"None";
NSString *const AnnotationViewControllerType_Sign = @"Sign";
NSString *const AnnotationViewControllerType_RedPen = @"RedPen";
NSString *const AnnotationViewControllerType_Text = @"Text";
NSString *const AnnotationViewControllerType_Layer = @"Layer";
NSString *const AnnotationViewControllerType_Hand = @"Hand";
NSString *const AnnotationViewControllerType_Eraser = @"Eraser";


int const ANNOTATION_IMAGE_TAG = 431;
CGFloat const TEXT_FIELD_WIDTH = 300;
CGFloat const TEXT_FIELD_HEIGHT = 32;
int const TEXTVIEWS_COUNT = 50;
static  NSString *defaultFontName;
static  CGColorRef defaultColor;


@implementation SaveObject{
}
@synthesize aString,numOfLayers,pagNumber;

@end

@implementation TextViewClass
@synthesize xValue;
@synthesize yValue;
@synthesize height;
@synthesize width;
@synthesize textViewText;
@end

@interface AnnotationViewController ()

@end

@implementation AnnotationViewController
{
    
    BOOL didMove;
    float xValue;
    float yValue;
    float width;
    float height;
    BOOL  isChecking;
    int defaultFontSize;
    BOOL  checkingDataFromSotre;
    CGPoint lastPoint;
    CGPoint currentPoint;
    UIView *pageView;
    CGColorRef signColor;
    CGColorRef annotationColor;
    CGMutablePathRef currPath;
    NSString *_annotationType;
    NSMutableArray *currentPaths;
    NSMutableArray   *layerCurrentPaths;
    CGMutablePathRef layerCurrPath;
    NSMutableDictionary *dic;
    NSMutableArray *noOfLayerForPdf;
    SaveObject *saveObject;
    LayerStore *layerStore;
    LayersView *layerToAddtoPdf;
    AnnotationStore *annotationStore;
    NSInteger indexOfLayer;
    NSInteger indexOfTextView;
    SPUserResizableView *userResizableViews;
   // LayerIndex *layerIndex;
    NSMutableArray *arrayView;
    UIView *currView;
    UIView *toMove;
    
    // new code
    UITapGestureRecognizer *gesturetrack;
    NSMutableArray *test;
}

@dynamic annotationType;
@synthesize signLineWidth;
@synthesize delegate;
@synthesize layerContainerViewArray;
@synthesize pencilColor;
@synthesize pencilLineWidth;
@synthesize markerColor;
@synthesize markerLineWidth;
@synthesize aTextView;
@synthesize arcont;
@synthesize containerView;
@synthesize layer;
@synthesize image;
@synthesize annotationImageData;
@synthesize deleteArray,tmp;
@synthesize eraserLineWith;
@synthesize textViewCountArray;
// new code
@synthesize crossbutton;
@synthesize bluredview;

- (id) initWithDocument:(ReaderDocument *)readerDocument
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.annotationType = AnnotationViewControllerType_None;
        self.document = readerDocument;
        self.currentPage = 0;
        self.image = [[UIImageView alloc] initWithImage:nil];
        self.image.frame = CGRectMake(0,0,100,100); //so we don't error out
        currentPaths = [NSMutableArray array];
        layerCurrentPaths = [NSMutableArray array];
        annotationStore = [[AnnotationStore alloc] initWithPageCount:[readerDocument.pageCount intValue]];
        layerStore = [[LayerStore alloc] initWithPageCount:[readerDocument.pageCount intValue]];
        [self layerCount:[readerDocument.pageCount intValue]];
        [self textViewCount:TEXTVIEWS_COUNT];
       // [self textViewArray:[readerDocument.pageCount intValue]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isChecking = YES;
    checkingDataFromSotre = YES;
    indexOfLayer = 1;
    indexOfTextView = 1;
    defaultFontSize = [UIFont systemFontSize];
    defaultFontName = @"Helvetica";
    defaultColor = (__bridge CGColorRef)([UIColor blackColor]);
    self.view.userInteractionEnabled = ![self.annotationType isEqualToString:AnnotationViewControllerType_None];
    self.view.opaque = NO;
    self.view.backgroundColor = [UIColor clearColor];
    self.image = [self createImageView];
    [pageView addSubview:self.image];
    self.containerView = [self createView];
    self.containerView.tag = 99;
    [pageView addSubview:self.containerView];
    dic = [[NSMutableDictionary alloc] init];
    self.deleteArray = [[NSMutableArray alloc] init];
    arrayView = [[NSMutableArray alloc] init];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self printTextColor];
}

-(void)printTextColor
{
    NSLog(@"Text Color >>>>>>>>>>>>>>>>>%@",aTextView.textColor);
}

- (UIImageView*) createImageView
{
    UIImageView *temp = [[UIImageView alloc] initWithImage:nil];
    temp.frame = pageView.frame;
    temp.tag = ANNOTATION_IMAGE_TAG;
    return temp;
}

-(UIView*)createView
{
    
    UIView *tmps = [[UIView alloc] init];
    tmps.frame = pageView.frame;
    return tmps;
}

- (NSString*) annotationType
{
    return _annotationType;
}

- (void) setAnnotationType:(NSString *)annotationType
{
    //Close current annotation
    [self finishCurrentAnnotation];
    _annotationType = annotationType;
    self.view.userInteractionEnabled = ![self.annotationType isEqualToString:AnnotationViewControllerType_None];
}

- (void) finishCurrentAnnotation
{
    Annotation* annotation = [self getCurrentAnnotation];

    if (annotation)
    {
        [annotationStore addAnnotation:annotation toPage:self.currentPage];
    }
    
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_Text])
    {
        [self refreshDrawing];
    }

    [currentPaths removeAllObjects];
    currPath = nil;
}

- (AnnotationStore*) annotations
{
    [self finishCurrentAnnotation];
    return annotationStore;
}

- (Annotation*) getCurrentAnnotation
{
    
    if (!currPath && [currentPaths count] == 0)
    {
        return nil;
    }
    
    CGMutablePathRef basePath = CGPathCreateMutable();
    for (UIBezierPath *bpath in currentPaths) {
        CGPathAddPath(basePath, NULL, bpath.CGPath);
    }
    CGPathAddPath(basePath, NULL, currPath);
    
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_RedPen])
    {
        return [PathAnnotation pathAnnotationWithPath:basePath color:markerColor lineWidth:markerLineWidth fill:NO];
    }
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_Sign])
    {
        return [PathAnnotation pathAnnotationWithPath:basePath color:pencilColor lineWidth:pencilLineWidth fill:NO];
    }
    return nil;
}

-(void) getContentView
{
    
    if(!self.layerContainerViewArray)
        self.layerContainerViewArray  = [[NSMutableArray  alloc] init];
    for (int i = 0; i < [self.document.pageCount intValue]; i++)
    {
        self.arcont = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [self.layerContainerViewArray addObject:self.arcont];
    }
}

- (void) moveToPage:(int)page contentView:(ReaderContentView*) view
{
    NSLog(@"%@",layerStore.layerStore);
   // NSLog(@"%@",self.containerView);
    
    if (page != self.currentPage || !pageView)
    {
        static NSInteger previousPage = 0;
        
        [self finishCurrentAnnotation];
        
        self.currentPage = page;
        
        pageView = [view contentView];
        
        if (self.image.superview != nil)
        {
            self.image.image = nil;
        }
        
        self.image = [self createImageView];
        
        [pageView addSubview:self.image];
        
        NSInteger minPage = 1;
        
        NSInteger maxPage = [self.document.pageCount integerValue];
        
        if ((self.currentPage < minPage) || (self.currentPage > maxPage))
        {
            previousPage = self.currentPage;
        }
        else
        {
            if (isChecking)
            {
               // NSLog(@"firsttime");
                previousPage = self.currentPage;
                isChecking = NO;
            }
            else
            {
                if (previousPage > self.currentPage)
                {
                   // NSLog(@"currentpage:%d",self.currentPage);
                    NSMutableArray *fechedArray;
                    // to test
                    
//                    if(previousPage==3)
//                    {
//                        fechedArray = [layerStore.layerStore objectAtIndex:(self.currentPage+1)];
//                    }
//                    else
//                    {
//                        fechedArray = [layerStore.layerStore objectAtIndex:self.currentPage];
//                    }
                    
                    fechedArray = [layerStore.layerStore objectAtIndex:previousPage-1];
                    // always 0
                    [fechedArray removeAllObjects];
                    
                    
                    NSMutableArray *fechedArrayData = [self layerData:self.containerView];
                    // also zero when from 2 to 1
                    // crash here
                    NSMutableArray *fechedArrayIndex = [tmp objectAtIndex:previousPage-1];
                    // also zero
                    
                    // to test
                    
                    for(int i = 0; i< fechedArrayData.count; i++)
                        
                    {
                        LayerData *aLayerData = [fechedArrayData objectAtIndex:i];
                        LayerIndex *layerIndexx = [fechedArrayIndex objectAtIndex:i];
                        aLayerData.layerindex = layerIndexx.layerindex;
                        aLayerData.layerName = layerIndexx.layerName;
                        aLayerData.pageIndex = layerIndexx.pageIndex;
                        
                        for (int a = 0; a < aLayerData.aTextViewData.count; a++)
                        {
                            TextViewData *textViewData = [aLayerData.aTextViewData objectAtIndex:a];
                            TextViewIndex *data = [layerIndexx.aTextViewIndexArray objectAtIndex:a];
                            textViewData.textViewIndex = data.textViewIndex;

                        }
                        [layerStore addLayerCount:aLayerData toPage:previousPage-1];
                    }
                   // NSLog(@"leftcounttest :%d",[[layerStore.layerStore objectAtIndex:self.currentPage] count]);
                    if (self.containerView.superview != nil)
                    {
                        NSMutableArray *arry = [[NSMutableArray alloc] initWithArray:[self.containerView subviews]];
                        for (UIView* aview in arry)
                        {
                            [aview removeFromSuperview];
                        }
                    }
                    
                    [self.containerView removeFromSuperview];
                    self.containerView = [self createView];
                    previousPage = self.currentPage;
                  //  NSLog(@"%@",layerStore.layerStore);
                }
                else
                {
                    // when moving 1 to 3 1st fetching 1st page data and then saving in to array
                    
                    // NSLog(@"%@",layerStore.layerStore);
                    //NSLog(@"rrrr:%d",self.currentPage);
                    // change in code 1st line me
                    NSMutableArray *fechedArray = [layerStore.layerStore objectAtIndex:previousPage-1];
                    
                   // NSMutableArray *fechedArray = [layerStore.layerStore objectAtIndex:self.currentPage-2];
                    [fechedArray removeAllObjects];
                    // send self.container view
                  //  NSLog(@"%@",self.containerView);
                    
                    NSMutableArray *fechedArrayData = [self layerData:self.containerView];
                    /*<__NSArrayM 0x8cebf40>(
                    <LayerData: 0x8ccd380>
                    )*/
                    
                   // NSMutableArray *fechedArrayIndex = [tmp objectAtIndex:self.currentPage-2];
                    // in case og move from 1 to 2 getting value
                    // in case of move from 1 to 3 not getting value
                    // testing
                    NSMutableArray *fechedArrayIndex;
//                    if(self.currentPage==2)
//                    {
//                        fechedArrayIndex= [tmp objectAtIndex:self.currentPage-2];
//                    }
//                    else
//                    {
//                        fechedArrayIndex= [tmp objectAtIndex:self.currentPage-3];
//                    }
                    // here want to implement general case
//                    for(int i=0;i<[self.document.pageCount integerValue];i++)
//                    {
//                        if(i==self.currentPage)
//                        {
//                            
//                        }
//                        
//                    }
                   
                    
                    
                    fechedArrayIndex=[tmp objectAtIndex:previousPage-1];
                    // new code
                    NSMutableArray *track1=[[NSMutableArray alloc]init];
                    for(int k=0;k<fechedArrayIndex.count;k++)
                    {
                        [track1 addObject:@""];
                    }
                    
                    for(int k=0;k<fechedArrayIndex.count;k++)
                    {
                        LayerIndex *tracktextview=[fechedArrayIndex objectAtIndex:k];
                        [track1 replaceObjectAtIndex:k withObject:[fechedArrayData objectAtIndex:tracktextview.layerindex-1]];
                    
                    }
                    fechedArrayData=[[NSMutableArray alloc]init];
                    fechedArrayData=[track1 mutableCopy];
                   // NSLog(@"%@",fechedArrayData);
                    
                    // end

                    
                    
                    for(int i = 0; i< fechedArrayData.count; i++)
                    {
                        LayerData *aLayerData = [fechedArrayData objectAtIndex:i];
                        // crash here
                        @try {
                            LayerIndex *layerIndexx = [fechedArrayIndex objectAtIndex:i];
                            aLayerData.layerindex = layerIndexx.layerindex;
                            aLayerData.layerName = layerIndexx.layerName;
                            aLayerData.pageIndex = layerIndexx.pageIndex;
                            
                            TextViewData *textViewData;
                            for (int a = 0; a < aLayerData.aTextViewData.count; a++)
                            {
                                textViewData = [aLayerData.aTextViewData objectAtIndex:a];
                                TextViewIndex *data = [layerIndexx.aTextViewIndexArray objectAtIndex:a];
                                textViewData.textViewIndex = data.textViewIndex;
                            }
                          //  NSLog(@"layerIndex.aTextViewIndexArray.count:%d",layerIndex.aTextViewIndexArray.count);
                            
                            [layerStore addLayerCount:aLayerData toPage:previousPage-1];
                        }
                        @catch (NSException *exception)
                        {
                            NSLog(@"Crash in specific case");
                        }
                        
                    }
                   // NSLog(@"rightcounttest :%d",[[layerStore.layerStore objectAtIndex:previousPage-1] count]);
                    if (self.containerView.superview != nil)
                    {
                        NSMutableArray *arry = [[NSMutableArray alloc] initWithArray:[self.containerView subviews]];
                        for (UIView* aview in arry)
                        {
                            [aview removeFromSuperview];
                        }
                    }
                    [self.containerView removeFromSuperview];
                    self.containerView = [self createView];
                   // NSLog(@"%@",self.containerView);
                    
                    previousPage = self.currentPage;
                }
            }
            [self loadPdf:page];// track previous record
            [self loadDataFromArray];
        }
        [self updateTableWithIndex];
    }
    
    [self refreshDrawing];
    
    // new code
   // LayerIndex *test=layerIndex;
    if(layerIndex.pageIndex!=self.currentPage)
    {
        layerIndex=nil;
       
    }
    // new code
   // loadgesture=YES;
}

// have to understand here
-(void) updateTableWithIndex
{
    for (UIView *imgView in pageView.subviews)
    {
        // looping and last go to else part
        if ([imgView isKindOfClass:[UIImageView class]])
        {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            [delegate getLayers:array];
            indexOfLayer = 1;
        }
        else
        {
            NSError *error = nil;
            NSMutableArray *indexArray = [[NSMutableArray alloc] init];
            NSMutableArray *fechedArray = [tmp objectAtIndex:self.currentPage-1];
            NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            NSPredicate *predicateIndex = [NSPredicate predicateWithFormat:@"pageIndex == %d AND layerForPage == %@", self.currentPage,self.document.fileName];
            request.predicate = predicateIndex;
            NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                                      initWithKey:@"layerindex" ascending:YES];
            [request setSortDescriptors:[NSArray arrayWithObject:sort]];
            [request setEntity:[NSEntityDescription entityForName:@"PdfLayers" inManagedObjectContext:mainMOC]];
            
            NSArray *results = [mainMOC executeFetchRequest:request error:&error];
            for (PdfLayers *aPdfLayer in results)
            {
                LayerIndex *aLayerIndex = [[LayerIndex alloc] init];
                aLayerIndex.layerindex = [[aPdfLayer valueForKey:@"layerindex"] integerValue];
                aLayerIndex.layerName = [aPdfLayer valueForKey:@"layerName"];
                // new code for page index
                aLayerIndex.pageIndex=[[aPdfLayer valueForKey:@"pageIndex"]integerValue];
                
                NSSet *allTextViews = aPdfLayer.textView;
                NSArray *textViewArray = [[allTextViews allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"textviewIndex" ascending:YES]]];
                
                for (TextView *aTextViews in textViewArray)
                {
                    TextViewIndex *aTextViewIndex = [[TextViewIndex alloc] init];
                    aTextViewIndex.textViewIndex  = [[aTextViews valueForKey:@"textviewIndex"]integerValue];
                    if (!aLayerIndex.aTextViewIndexArray)
                    {
                        aLayerIndex.aTextViewIndexArray = [[NSMutableArray alloc] init];
                    }
                    [aLayerIndex.aTextViewIndexArray addObject:aTextViewIndex];
                }
                [indexArray addObject:aLayerIndex];
            }
            if (fechedArray.count == 0)
            {
                [fechedArray addObjectsFromArray:indexArray];
                [delegate getLayers:fechedArray];
            }
            else
            {
                [delegate getLayers:fechedArray];
            }
        }
    }
}
// new delegate of spuserreseziable
-(void)sendwidth:(float)heightwidth sendpoint:(BOOL)isHeight
{
    NSLog(@"Newwidth/height=%f",heightwidth);
     NSLog(@"textviewtext=%@",aTextView.text);
     NSLog(@"textviewwidth=%f",aTextView.frame.size.width);
     NSLog(@"textviewheight=%f",aTextView.frame.size.height);
    
//    
//    
//    if(isHeight)
//    {
//        
//        [self setSize:[aTextView.text sizeWithFont:aTextView.font constrainedToSize:CGSizeMake(FLT_MAX, heightwidth) lineBreakMode:YES]/*[aTextView sizeThatFits:CGSizeMake(FLT_MAX, heightwidth)]*/];
//    }
//    else
//    {
//        [self setSize:[aTextView.text sizeWithFont:aTextView.font constrainedToSize:CGSizeMake(heightwidth, FLT_MAX) lineBreakMode:YES]/*[aTextView sizeThatFits:CGSizeMake(FLT_MAX, heightwidth)]*/];
//         //[self setSize:[aTextView sizeThatFits:CGSizeMake(heightwidth, FLT_MAX)]];
//    }
    
}



-(void)setSize:(CGSize)textViewSize
{
    [userResizableViews setFrame:CGRectMake(userResizableViews.frame.origin.x, userResizableViews.frame.origin.y, textViewSize.width+20, textViewSize.height+20)];
}

-(void) loadDataFromArray
{
    self.handchecktrack=YES;
    
    [self.containerView removeFromSuperview];
    self.containerView = [self createView];
    NSMutableArray *fechedArray = [layerStore.layerStore objectAtIndex:self.currentPage-1];
    // new code
    arrayView=[[NSMutableArray alloc]init];
    // end

    for (int i = 0; i < fechedArray.count; i++)
    {
        LayerData *aData = [fechedArray objectAtIndex:i];
        self.layer  = [[LayersView alloc] initWithFrame:CGRectMake(0, 0, pageView.frame.size.width, pageView.frame.size.height)];
        self.layer.backgroundColor = [UIColor clearColor];
        self.layer.userInteractionEnabled = YES;
        self.layer.imageView.image  = (UIImage*)[UIImage imageWithData:aData.imageData];
        self.layer.tag = aData.layerindex;
        

        
        
        for (int j = 0 ; j < aData.aTextViewData.count; j++)
        {
            TextViewData *aTextViewData = [aData.aTextViewData objectAtIndex:j];
            CGRect gripFrame = CGRectMake(aTextViewData.xValue, aTextViewData.yValue, aTextViewData.width, aTextViewData.height);
            userResizableViews = [[SPUserResizableView alloc] initWithFrame:gripFrame];
            userResizableViews.backgroundColor = [UIColor clearColor];
            userResizableViews.userInteractionEnabled = NO;
            
            // new code
            userResizableViews.tag=aTextViewData.textViewIndex;
            // end
            
            aTextView = [[UITextView alloc]initWithFrame:gripFrame];
            aTextView.backgroundColor = [UIColor clearColor];
            aTextView.font = [UIFont fontWithName:defaultFontName size:defaultFontSize];
            aTextView.delegate = self;
            aTextView.text = aTextViewData.textViewText;
            
            if (aTextViewData.fontColor)
            {
                NSArray *items = [aTextViewData.fontColor componentsSeparatedByString:@" "];
              //  NSLog(@"float:%f",[[items objectAtIndex:0] floatValue]);
                if (items.count != 1)
                {
                    CGFloat red = [[items objectAtIndex:1] floatValue];
                    CGFloat green = [[items objectAtIndex:2] floatValue];
                    CGFloat blue = [[items objectAtIndex:3] floatValue];
                    CGFloat alpha = [[items objectAtIndex:4] floatValue];
                    [aTextView setTextColor:[UIColor colorWithRed:red green:green blue:blue alpha:alpha]];
                }
            }
            [aTextView setFont:[UIFont fontWithName:aTextViewData.fontName size:aTextViewData.fontSize]];
            userResizableViews.contentView = self.aTextView;
            userResizableViews.delegate = self;
            if(self.handchecktrack==NO)
            {
                 [userResizableViews showEditingHandles];
            }
           
            currentlyEditingView = userResizableViews;
            lastEditedView = userResizableViews;
            [self.layer addSubview:userResizableViews];
            
            // new code
            [arrayView addObject:userResizableViews];
            // end
            
            
//            UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles:)];
//            [gestureRecognizer setDelegate:self];
//            [self.layer addGestureRecognizer:gestureRecognizer];
            
            // adding new code
            UITapGestureRecognizer *gestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles1:)];
            gestureRecognizer1.numberOfTapsRequired = 2;
            [gestureRecognizer1 setDelegate:self];
            [userResizableViews addGestureRecognizer:gestureRecognizer1];
            
            UITapGestureRecognizer *gestureRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles2:)];
            gestureRecognizer2.numberOfTapsRequired = 1;
            [gestureRecognizer2 setDelegate:self];
            [userResizableViews addGestureRecognizer:gestureRecognizer2];
            [gestureRecognizer2 requireGestureRecognizerToFail:gestureRecognizer1];
            // end
        }
        [self.containerView addSubview:self.layer];
        pageView.userInteractionEnabled = YES;
        [pageView addSubview:self.containerView];
    
    }

    //_index=indexvalue;
    // new code
    for (LayersView *checkView in self.containerView.subviews)
    {
        if (_index)
        {
            if ([checkView tag] != _index)
            {
                //Do nothing
                //userResizableViews.hidden=TRUE;
            }
            // hide due to issue
           /* else
            {
                [checkView addSubview:userResizableViews];
                UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles:)];
                gestureRecognizer.numberOfTapsRequired = 1;
                [gestureRecognizer setDelegate:self];
                [checkView addGestureRecognizer:gestureRecognizer];
            }*/
        }
        else if ([checkView tag] == layerIndex.layerindex)
        {
            [checkView addSubview:userResizableViews];
            UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles:)];
            gestureRecognizer.numberOfTapsRequired = 1;
            [gestureRecognizer setDelegate:self];
            [checkView addGestureRecognizer:gestureRecognizer];
        }
        
        currView = checkView;
    }

    // end
   // NSLog(@"%d",layerIndex.layerindex);
    
}



/// addind text view
-(int)addingTextView
{
   

    NSMutableArray *array = [tmp objectAtIndex:self.currentPage-1];
    // new code
    // new code
    int position=-1;
    // new code for reorder
    for(int i=0;i<array.count;i++)
    {
        LayerIndex *trackposition=[array objectAtIndex:i];
        if(trackposition.layerindex==layerIndex.layerindex)
        {
            position=i;
        }
    }
    // end
    
    
    // end
    layerIndex  = [array objectAtIndex:position];
    // end
    
    
    NSError *error = nil;
    if (!layerIndex.aTextViewIndexArray)
    {
        layerIndex.aTextViewIndexArray = [[NSMutableArray alloc] init];
    }
    NSMutableArray *indexArray = [[NSMutableArray alloc] init];
    NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"PdfPage" inManagedObjectContext:mainMOC]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pageName == %@",[[NSString stringWithFormat:@"%@",self.document.fileName] stringByAppendingFormat:@"%d",self.currentPage-1]];
    [request setPredicate:predicate];
    
    NSArray *results = [mainMOC executeFetchRequest:request error:&error];
    PdfPage *page  = [results lastObject];
    NSSet *layerSet  = page.layers;
    NSArray *layerArray = [layerSet allObjects];
    PdfLayers *layers = [layerArray lastObject];
    NSSet *textVieSet = layers.textView;
    NSArray *textViewResult = [textVieSet allObjects];
    
    for (TextView *aTextView1 in textViewResult)
    {
        TextViewIndex *aTextViewIndex = [[TextViewIndex alloc] init];
        aTextViewIndex.textViewIndex = [[aTextView1 valueForKey:@"textviewIndex"] integerValue];
        [indexArray addObject:aTextViewIndex];
    }
    if (indexArray.count > 0)
    {
        if (!layerIndex)
        {
            layerIndex = [[LayerIndex alloc] init];
            layerIndex.aTextViewIndexArray = [[NSMutableArray alloc] init];
        }
        [layerIndex.aTextViewIndexArray addObjectsFromArray:indexArray];
    }
    // hide for some time
    

    // end
    
    TextViewIndex *textViewIndex = [layerIndex.aTextViewIndexArray lastObject];
    
    if (!textViewIndex.textViewIndex)
    {
        indexOfTextView = textViewIndex.textViewIndex;
        indexOfTextView ++;
    }else
    {
        indexOfTextView = textViewIndex.textViewIndex;
        indexOfTextView ++;
    }
    textViewIndex = [[TextViewIndex alloc] init];
    textViewIndex.textViewIndex = indexOfTextView;
    [layerIndex.aTextViewIndexArray addObject:textViewIndex];
   // layerIndex = [array lastObject];
    
    // new code
    
    // NSLog(@"%@",self.containerView.subviews);
    
    // end
    
    CGRect gripFrame = CGRectMake(50, 50, 200, 150);
    userResizableViews = [[SPUserResizableView alloc] initWithFrame:gripFrame];
    
    // new code
    userResizableViews.tag=indexOfTextView;
    // end
    
    userResizableViews.backgroundColor = [UIColor clearColor];
    userResizableViews.userInteractionEnabled = NO;
    aTextView = [[UITextView alloc]initWithFrame:gripFrame];
    aTextView.backgroundColor = [UIColor clearColor];
    aTextView.editable = NO;
    aTextView.font = [UIFont fontWithName:defaultFontName size:defaultFontSize];
    aTextView.tag  = indexOfTextView;
    aTextView.delegate = self;
    userResizableViews.contentView = self.aTextView;
    //    // new code for test
    //    crossbutton=[[UIButton alloc]initWithFrame:CGRectMake(10, 50, 100, 30)];
    //    [crossbutton setTitle:@"cross" forState:UIControlStateNormal];
    //    crossbutton.backgroundColor=[UIColor redColor];
    //    [userResizableViews.contentView addSubview:crossbutton];
    
    
    userResizableViews.delegate = self;
    [arrayView addObject:userResizableViews];
    [userResizableViews showEditingHandles];
    currentlyEditingView = userResizableViews;
    lastEditedView = userResizableViews;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles1:)];
    gestureRecognizer.numberOfTapsRequired = 2;
    [gestureRecognizer setDelegate:self];
    [userResizableViews addGestureRecognizer:gestureRecognizer];
    
    UITapGestureRecognizer *gestureRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles2:)];
    gestureRecognizer2.numberOfTapsRequired = 1;
    [gestureRecognizer2 setDelegate:self];
    [userResizableViews addGestureRecognizer:gestureRecognizer2];
    [gestureRecognizer2 requireGestureRecognizerToFail:gestureRecognizer];
    
    //NSLog(@"%@",self.containerView.subviews);
    
    
    for (LayersView *checkView in self.containerView.subviews)
    {
        if (_index)
        {
            if ([checkView tag] != _index)
            {
                //Do nothing
            }
            else
            {
                [checkView addSubview:userResizableViews];
                UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles:)];
                gestureRecognizer.numberOfTapsRequired = 1;
                [gestureRecognizer setDelegate:self];
                [checkView addGestureRecognizer:gestureRecognizer];
            }
        }
        else if ([checkView tag] == layerIndex.layerindex)
        {
            [checkView addSubview:userResizableViews];
            UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles:)];
            gestureRecognizer.numberOfTapsRequired = 1;
            [gestureRecognizer setDelegate:self];
            [checkView addGestureRecognizer:gestureRecognizer];
        }
        
        currView = checkView;
    }
   // NSLog(@"%@",self.containerView.subviews);
    
    // new code
    return arrayView.count;
}



//------Adding the layers on Pdf page-------//

-(void) layerAddToPdf
{
//    // tmp is an array in which 6 blank array is added
//    NSLog(@"%@",layerIndex.layerName);
   if(self.checkmark==YES)
   {
       UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Must Selected" message:@"Before adding a layer one layer must be selected" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
       [alert show];
       [SVProgressHUD dismiss];
       return;
   }
    
    
    NSLog(@"%d",self.currentPage);
    NSMutableArray *array = [tmp objectAtIndex:self.currentPage-1];
    for (int i = 0; i<= array.count; i++)
    {
        LayerIndex *li  = [array lastObject];
        if (li.layerindex)
        {
            indexOfLayer = li.layerindex;
            indexOfLayer ++;
        }else
        {
            indexOfLayer = 1;
            test=[[NSMutableArray alloc]init];
        }
    }
    // new for limitation
    if(indexOfLayer==6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Maximum" message:@"Can't Add" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [SVProgressHUD dismiss];
        return;
    }
    // end
    
    layerIndex = [[LayerIndex alloc] init];
    layerIndex.layerindex = indexOfLayer;
    layerIndex.layerName  = [@"Layer" stringByAppendingFormat:@"%d",indexOfLayer];
    layerIndex.pageIndex = self.currentPage;
    [array addObject:layerIndex];// adding layer in to array
    [delegate getLayers:array];
    
   

    self.layer = [[LayersView alloc] initWithFrame:CGRectMake(0, 0, pageView.frame.size.width, pageView.frame.size.height)];
    self.layer.backgroundColor = [UIColor clearColor];
    self.layer.userInteractionEnabled = YES;
    self.layer.tag = indexOfLayer;
    
    ILTranslucentView *iLTranslucentView = [[ILTranslucentView alloc] initWithFrame:CGRectMake(0, 0,pageView.frame.size.width, pageView.frame.size.height)];
    [iLTranslucentView setTag:987];
    iLTranslucentView.userInteractionEnabled = NO;
    iLTranslucentView.hidden = YES;
    [self.layer addSubview:iLTranslucentView];
    [self.layer bringSubviewToFront:iLTranslucentView];
    [self.containerView addSubview:self.layer];
    // new code here

   /* self.bluredview=[[UIView alloc]initWithFrame:self.containerView.bounds];
    if(indexOfLayer==1)
    {
        bluredview.backgroundColor=[UIColor clearColor];
        [self.containerView addSubview:bluredview];
    }
    // end*/
   
    pageView.userInteractionEnabled = YES;
    [pageView addSubview:self.containerView];
    
  
    
}
// new code
-(void)sendvalue:(int)indexvalue
{
    // new code
   // NSLog(@"%d",self.layer.tag);
   // NSLog(@"%@",arrayView);
    
    // hide had written for blur
    
    for(int i=0;i<[test count];i++)
    {
        LayerIndex *fetchlayer=[[LayerIndex alloc]init];
        fetchlayer=[test objectAtIndex:i];
        if(indexvalue==self.layer.tag)
        {
            if(i==indexvalue-1)
            {
            if([fetchlayer.aTextViewIndexArray count]>0)
            {
                userResizableViews.hidden=FALSE;
            }
            }
            else
            {
                if([fetchlayer.aTextViewIndexArray count]>0)
                {
                    userResizableViews.hidden=TRUE;
                }
            }
        }
//        else
//        {
//            if([fetchlayer.aTextViewIndexArray count]>0)
//            {
//                userResizableViews.hidden=TRUE;
//            }
//        }
        
        
    }
    
    // new code to solve issue
    
    // previous new code
    
    
    _index=indexvalue;
    for (LayersView *checkView in self.containerView.subviews)
    {
        if (_index)
        {
            if ([checkView tag] != _index)
            {
                //Do nothing
                //userResizableViews.hidden=TRUE;
            }
            else
            {
                [checkView addSubview:userResizableViews];
                UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles:)];
                gestureRecognizer.numberOfTapsRequired = 1;
                [gestureRecognizer setDelegate:self];
                [checkView addGestureRecognizer:gestureRecognizer];
            }
        }
        else if ([checkView tag] == layerIndex.layerindex)
        {
            [checkView addSubview:userResizableViews];
//            UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles:)];
//            gestureRecognizer.numberOfTapsRequired = 1;
//            [gestureRecognizer setDelegate:self];
//            [checkView addGestureRecognizer:gestureRecognizer];
        }
        
        currView = checkView;
    }
    
   /* // new code
    for(int k=0;k<[arrayView count];k++)
    {
        userResizableViews=[arrayView objectAtIndex:k];
        NSLog(@"%d",[userResizableViews.contentView tag]);
        
        if([userResizableViews.contentView tag]==_index)
        {
            userResizableViews.userInteractionEnabled=YES;
        }
        else
        {
            userResizableViews.userInteractionEnabled=NO;
        }
    }
    // end
    */
    

    
}

- (void) makeLayerBlurred:(LayersView *)aLayer
{
    [aLayer bringSubviewToFront:[self.layer viewWithTag:987]];
    [aLayer viewWithTag:987].hidden = NO;
}

//- (void) makeLayerUnBlurred:(LayersView *)layer
//{
//    [layer viewWithTag:987].hidden = YES;
//}

- (void)layerCount:(int)page_count
{
    tmp = [NSMutableArray arrayWithCapacity:page_count];
    for (int i = 0; i < page_count; i++)
    {
        self.deleteArray = [[NSMutableArray alloc] init];
        [tmp addObject:self.deleteArray];
    }
}

//------------Maintaning the Count of TextView For layer----------- //
-(void)textViewCount:(int)textView_count
{
    textViewCountArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < textView_count; i++)
    {
        NSMutableArray *textView_tmp = [[NSMutableArray alloc] init];
        [textViewCountArray addObject:textView_tmp];
    }
}

-(NSMutableArray*)layerData:(UIView*)aLayer
{
   // NSLog(@"%@",layerIndex);
   // NSLog(@"%d",self.currentPage);
  //  NSLog(@"%@",aLayer);
    
    
    
    // new code
    NSLog(@"%@",aLayer.subviews);
    
    for (LayersView *v1 in aLayer.subviews)
    {
    
    for (UIImageView *v in aLayer.subviews)
    {
        if([v isKindOfClass:[UIImageView class]])
        {
            if( [v tag]==[v1 tag])
            {
                [v removeFromSuperview];
                v1.hidden=FALSE;
            }
        }
    }
    }
    // end
   
    
    // new code
    
    
    UIView *finallayer=[[UIView alloc]init];
    NSArray *putfinalsubview=[[NSArray alloc]init];
    NSMutableArray *putfinalsubview1=[[NSMutableArray alloc]initWithCapacity:aLayer.subviews.count];
    
    for(int k=0;k<aLayer.subviews.count;k++)
    {
        [putfinalsubview1 addObject:@""];
    }
    
    for(int i=0;i<aLayer.subviews.count;i++)
    {
        UIView *trackview=[aLayer.subviews objectAtIndex:i];
        NSLog(@"%d",trackview.tag);
        [putfinalsubview1 replaceObjectAtIndex:trackview.tag-1 withObject:trackview];
        
    }
   // NSLog(@"%@",putfinalsubview1);
    putfinalsubview=[putfinalsubview1 copy];
    
   //  NSLog(@"%@",putfinalsubview);

    
    
    // end
   
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    LayerData *layerData;
    // if there is subview
    for (int k=0;k<putfinalsubview.count;k++)
    {
        [finallayer addSubview:[putfinalsubview objectAtIndex:k]];
    }
    

    
    for (UIView *layerView in finallayer.subviews)
    {
        // will not go when from 2 to 1
        layerData = [[LayerData alloc] init];
        for (UIImageView *aView in layerView.subviews)
        {
            // came
            if ([aView isKindOfClass:[UIImageView class]])
            {
                // came
                layerData.imageData = [NSData dataWithData:UIImagePNGRepresentation(aView.image)];
            }
        }
        layerData.aTextViewData = [[NSMutableArray alloc] init];
        for (SPUserResizableView *spUserResizableView in layerView.subviews)
        {
            // came
            TextViewData *textViewData;
            if ([spUserResizableView isKindOfClass:[SPUserResizableView class]])
            {
                // not came
                textViewData = [[TextViewData alloc] init];
                textViewData.xValue = spUserResizableView.frame.origin.x;
                textViewData.yValue = spUserResizableView.frame.origin.y;
                textViewData.width  = spUserResizableView.frame.size.width;
                textViewData.height = spUserResizableView.frame.size.height;
            }
            for (UITextView *textView in spUserResizableView.subviews)
            {
                if ([textView isKindOfClass:[UITextView class]])
                {
                    textViewData.textViewText = [[NSString alloc] initWithFormat:@"%@",textView.text];
                    NSMutableString *string = [[NSMutableString alloc] initWithFormat:@"%@",textView.textColor];
                    if(string==(id) [NSNull null] || [string length]==0 || [string isEqualToString:@"(null)"])
                    {
                        //Do nothing
                    }
                    else
                    {
                        [string deleteCharactersInRange: [string rangeOfString: @"UIDeviceRGBColorSpace"]];
                    }
                    textViewData.fontColor = string;
                    textViewData.fontName = textView.font.fontName;
                    textViewData.fontSize = textView.font.pointSize;
                    [layerData.aTextViewData addObject:textViewData];
                }
            }
        }
        [returnArray addObject:layerData];
    }
    return returnArray;
}




- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView
{
   // NSLog(@"%@",self.aTextView);
    
    // call
    //[currentlyEditingView hideEditingHandles];
    currentlyEditingView = userResizableView;
    
}

- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView
{
    lastEditedView = userResizableView;
//    NSLog(@"userresizable frame=%@",NSStringFromCGRect(userResizableView.frame));
//    NSLog(@"textview width=%f",self.aTextView.frame.size.width);
//     NSLog(@"textview height=%f",self.aTextView.frame.size.height);
//    NSLog(@"textviewtext=%@",self.aTextView.text);
//    
//    
//    if([aTextView.text length]>0)
//    {
//    CGSize textSize = [aTextView.text sizeWithFont:aTextView.font constrainedToSize:CGSizeMake(1000, aTextView.frame.size.height) lineBreakMode: NSLineBreakByWordWrapping];
//    
//    [self setSize:textSize];
//    }
    

    // here will have to do something
}

/*- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    
    if ([currentlyEditingView hitTest:[touch locationInView:currentlyEditingView] withEvent:nil])
    {
        return NO;
    }
    return YES;
}*/

- (void)hideEditingHandles:(UITapGestureRecognizer*)recognizer
{
   if(self.handchecktrack==NO)
   {
       [currentlyEditingView showEditingHandles];
       [lastEditedView showEditingHandles];
   }
    
    //NSLog(@"%d",[userResizableViews.contentView.subviews count]);
   // aTextView.editable = NO;
   // [aTextView resignFirstResponder];
    
    // new code
    for (UIView *view in arrayView)
    {
        
            toMove = view;
            for (UITextView *t in toMove.subviews)
            {
                if ([t isKindOfClass:[UITextView class]])
                {
                    t.editable = NO;
                    [t resignFirstResponder];
                    
                }
            }
    }
    

}

// new method
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}


// new code
- (void)hideEditingHandlesspec:(UITapGestureRecognizer*)recognizer
{
    
    /* for (UITextView *t in currentlyEditingView.subviews)
     {
     
     if ([t isKindOfClass:[UITextView class]])
     {
     t.editable = YES;
     [t becomeFirstResponder];
     [currentlyEditingView hideEditingHandles];
     }
     }*/
    if(self.handchecktrack==NO)
     {
                   [lastEditedView showEditingHandles];
     }
    
   
                    UITextView *t;
                    [currentlyEditingView hideEditingHandlesborder];
                   // NSLog(@"text-->%@",t.text);
                    t.userInteractionEnabled = YES;
                    t.editable = YES;
                    [t becomeFirstResponder];
                    
    
        
    
}

// end

- (void)hideEditingHandles1:(UITapGestureRecognizer*)recognizer
{
    
   /* for (UITextView *t in currentlyEditingView.subviews)
    {
        
        if ([t isKindOfClass:[UITextView class]])
        {
            t.editable = YES;
            [t becomeFirstResponder];
            [currentlyEditingView hideEditingHandles];
        }
    }*/
    
    // new code
    // [currentlyEditingView hideEditingHandles];
    
    // end
    
     [lastEditedView showEditingHandles];
    
    CGPoint firstTouch = [recognizer locationInView:currView];
    
    for (UIView *view in arrayView)
    {
        
        if (CGRectContainsPoint(view.frame, firstTouch))
        {
            toMove = view;
            for (UITextView *t in toMove.subviews)
            {
                if ([t isKindOfClass:[UITextView class]])
                {
                    
                  // [currentlyEditingView hideEditingHandles];
                   // NSLog(@"text-->%@",t.text);
                    t.userInteractionEnabled = YES;
                    t.editable = YES;
                    [t becomeFirstResponder];
                    
                }
                // new code
                break;
                
            }
        }
    }

}

// new method
-(void)dismisskeyboard
{
    [aTextView resignFirstResponder];
}
// end

- (void)hideEditingHandles2:(UITapGestureRecognizer*)recognizer
{
    [lastEditedView showEditingHandles];
    [currentlyEditingView showEditingHandles];
    aTextView.editable = YES;
    [aTextView resignFirstResponder];
    
    // new code
 
}

-(void)activateTheTextBox
{
    // new code
   // userResizableViews.userInteractionEnabled=YES;
    // new code
//    gesturetrack=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideEditingHandles1:)];
//    gesturetrack.numberOfTapsRequired = 2;
//    [gesturetrack setDelegate:self];
//    [self.view addGestureRecognizer:gesturetrack];
    
   
    pageView.superview.superview.userInteractionEnabled = YES;
    
    for (LayersView *layerView in self.containerView.subviews)
    {
        for (SPUserResizableView *userResizableView in layerView.subviews)
        {
            // came 1st time
            if ([userResizableView isKindOfClass:[SPUserResizableView class]])
            {
                // not came
                // came 2nd loop
                 [userResizableView showEditingHandles];
                
                userResizableView.userInteractionEnabled = YES;
            }
        }
    }
}
// new
// new code
-(void)deactivatetextbox
{
    // new code
    currentlyEditingView=nil;
    // end
    
    for (LayersView *layerView in self.containerView.subviews)
    {
        for (SPUserResizableView *userResizableView in layerView.subviews)
        {
            if ([userResizableView isKindOfClass:[SPUserResizableView class]])
            {
                for(int k=0;k<userResizableView.subviews.count;k++)
                {
                    UITextView *testtrack=[userResizableView.subviews objectAtIndex:k];
                    if([testtrack isKindOfClass:[UITextView class]])
                    {
                    // new code for trimming
                    NSString *trimmedString = [testtrack.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                    if([trimmedString isEqualToString:@""])
                    {
                        int position=-1;
                        for(int p=0;p<[layerIndex.aTextViewIndexArray count];p++)
                        {
                            TextViewIndex *trackblanktextview=[layerIndex.aTextViewIndexArray objectAtIndex:p];
                            if([userResizableView tag]==trackblanktextview.textViewIndex)
                            {
                                position=p;
                            }
                        }
                        [userResizableView removeFromSuperview];
                        if(position>=0)
                        {
                            [layerIndex.aTextViewIndexArray removeObjectAtIndex:position];
                        }
                        
                        NSLog(@"%@",layerIndex.aTextViewIndexArray);
                        
                    }
                    }
                }
                
                // new code
                
                
                
                // new line
                [userResizableView hideEditingHandlesborder];
                // end
                userResizableView.userInteractionEnabled = NO;
            }
        }
    }
}


- (void)textViewDidChange:(UITextView *)myTextView
{
    CGRect rect = aTextView.frame;
    rect.size.height = aTextView.contentSize.height;
    
    
    if (aTextView.frame.size.height < rect.size.height)
    {
        userResizableViews.frame  = CGRectMake(userResizableViews.frame.origin.x, userResizableViews.frame.origin.y, userResizableViews.frame.size.width, rect.size.height +20);
        aTextView.frame = rect;
    }
}


-(void) incTextSize
{
    for (UIView *txt in [[[aTextView superview] superview] subviews])
    {
        if ([txt isKindOfClass:[UIView class]])
        {
            for (UITextView *t in [txt subviews])
            {
                if ([t isFirstResponder] == YES)
                {
                    if(defaultFontSize < 50)
                    {
                        defaultFontSize = defaultFontSize + 2;
                        [t setFont:[UIFont systemFontOfSize:defaultFontSize]];
                        [t setFont:[UIFont fontWithName:defaultFontName size:defaultFontSize]];
                        [t setNeedsLayout];
                    }
                }
            }
        }
    }
}
-(void) decTextSize
{
    for (UIView *txt in [[[aTextView superview] superview] subviews])
    {
        if ( [txt isKindOfClass:[UIView class]])
        {
            for (UITextView *t in [txt subviews])
            {
                if ([t isFirstResponder] == YES)
                {
                    if(defaultFontSize > 14)
                    {
                        defaultFontSize = defaultFontSize - 2;
                        [t setFont:[UIFont systemFontOfSize:defaultFontSize]];
                        [t setFont:[UIFont fontWithName:defaultFontName size:defaultFontSize]];
                        [t setNeedsLayout];
                    }
                }
            }
        }
    }
}
-(void) setFontForText:(NSString*)font
{
    defaultFontName = font;
    for (UIView *txt in [[[aTextView superview] superview] subviews])
    {
        if ( [txt isKindOfClass:[UIView class]])
        {
            for (UITextView *t in [txt subviews])
            {
                if ([t isFirstResponder] == YES)
                {
                    [t setFont:[UIFont fontWithName:font size:defaultFontSize]];
                    [t setNeedsLayout];
                }
            }
        }
    }
}

-(void) changingTextColor:(CGColorRef)_color
{
    defaultColor = (__bridge CGColorRef)([UIColor colorWithCGColor:_color]);
    for (UIView *txt in [[[aTextView superview] superview] subviews])
    {
        if ( [txt isKindOfClass:[UIView class]])
        {
            for (UITextView *t in [txt subviews])
            {
                if ([t isFirstResponder] == YES)
                {
                    [t setTextColor:[UIColor colorWithCGColor:_color]];
                    [t setNeedsLayout];
                }
            }
        }
    }
}
-(void)loadPdf:(int) page
{
    
    
    NSMutableArray *fetchedArray = [layerStore.layerStore objectAtIndex:self.currentPage-1];
    // here fetching data from layer store at index 0
    // new code
    if([fetchedArray count]==0)
    {
        NSMutableArray *arrayList = [[NSMutableArray alloc] init];
        NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
        NSError *error;
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"PdfDoc"];
        NSArray *fetchedObjects = [mainMOC executeFetchRequest:fetchRequest error:&error];
        
        for (PdfDoc *pdfDoc in fetchedObjects)
        {
            // will come inside when from 2 to 1
            
            if ([self.document.fileName isEqualToString:[pdfDoc valueForKey:@"pdfName"]])
            {
                NSArray *count = [pdfDoc.page allObjects];
                
                for (int i = 0; i< count.count; i++)
                {
                    // not come when from 2 to 1
                    PdfPage *page = [count objectAtIndex:i];
                    if ([page.pageNumber isEqual:[NSNumber numberWithInteger:self.currentPage]])
                    {
                       // NSLog(@"i:%d",i);
                        self.image.image = [UIImage imageWithData:[page valueForKey:@"pageAnnoImage"]];
                        NSArray *array  = [page.layers allObjects];
                        if (!array)
                        {
                            //Do nothing
                        }
                        for ( int j = 0; j < array.count;j++)
                        {
                            LayerData *layerData = [[LayerData alloc] init];
                            
                            PdfLayers *layers  = [array objectAtIndex:j];
                            layerData.imageData = [layers valueForKey:@"layerAnnotationImage"];
                            layerData.layerindex = [[layers valueForKey:@"layerindex"] integerValue];
                            layerData.layerName = [layers valueForKey:@"layerName"];
                            // new code
                            layerData.pageIndex=[[layers valueForKey:@"pageIndex"] integerValue];
                            
                            layerData.aTextViewData = [[NSMutableArray alloc] init];
                            NSArray *textViewArray = [layers.textView allObjects];
                            TextViewData *aTextViewData;
                            for (int n = 0;  n < textViewArray.count; n++)
                            {
                                aTextViewData = [[TextViewData alloc] init];
                                TextView *textView  = [textViewArray objectAtIndex:n];
                                aTextViewData.xValue = [[textView valueForKey:@"textviewCoordinateX"] floatValue];
                                aTextViewData.yValue = [[textView valueForKey:@"textviewCoordinateY"] floatValue];
                                aTextViewData.width = [[textView valueForKey:@"width"] floatValue];
                                aTextViewData.height = [[textView valueForKey:@"height"] floatValue];
                                aTextViewData.textViewIndex = [[textView valueForKey:@"textviewIndex"] integerValue];;
                                aTextViewData.textViewText = textView.textViewStirng;
                                aTextViewData.fontName = [textView valueForKey:@"fontName"];
                                aTextViewData.fontColor = [textView valueForKey:@"fontColor"];
                                aTextViewData.fontSize = [[textView valueForKey:@"fontSize"] integerValue];
                                [layerData.aTextViewData addObject:aTextViewData];
                            }
                            [arrayList addObject:layerData];
                        }
                        [fetchedArray removeAllObjects];
                        [fetchedArray addObjectsFromArray:arrayList];
                    }
                }
            }
        }
    }
    else
    {
        
    }
    
    
}
- (void) clear
{

    image.image = nil;
    currPath = nil;
    [currentPaths removeAllObjects];
    [annotationStore empty];
}

- (void) hide
{
    [self.view removeFromSuperview];
}

//-------Saving the data to coredata -----------//

-(void)currentPageDataSave
{
    
    NSMutableArray *fechedArray = [layerStore.layerStore objectAtIndex:self.currentPage-1];
    [fechedArray removeAllObjects];
    
    NSMutableArray *fechedArrayData = [self layerData:self.containerView];
    NSMutableArray *fechedArrayIndex = [tmp objectAtIndex:self.currentPage-1];
    
    for(int i = 0; i< fechedArrayData.count; i++)
    {
        LayerData *aLayerData = [fechedArrayData objectAtIndex:i];
        // here crash out of bound
        @try {
            LayerIndex *layerIndexx = [fechedArrayIndex objectAtIndex:i];
            aLayerData.layerindex = layerIndexx.layerindex;
            aLayerData.layerName = layerIndexx.layerName;
            aLayerData.pageIndex = layerIndexx.pageIndex;
            
            TextViewData *textViewData;
            for (int a = 0; a < aLayerData.aTextViewData.count; a++)
            {
                textViewData = [aLayerData.aTextViewData objectAtIndex:a];
                TextViewIndex *data = [layerIndexx.aTextViewIndexArray objectAtIndex:a];
                textViewData.textViewIndex = data.textViewIndex;
            }
            [layerStore addLayerCount:aLayerData toPage:self.currentPage-1];
        }
        
        @catch (NSException *exception)
        {
            NSLog(@"Exception:%@",exception.description);
        }
    }
    if (self.containerView.superview != nil)
    {
        NSMutableArray *arry = [[NSMutableArray alloc] initWithArray:[self.containerView subviews]];
        for (UIView* aview in arry)
        {
            [aview removeFromSuperview];
        }
    }
    [self.containerView removeFromSuperview];
    self.containerView = [self createView];
    [self loadDataFromArray];
    
}
// new method for export
- (void) saveToexport
{
    [self currentPageDataSave];
    // NSLog(@"document name=%@",self.document);
    NSLog(@"document url=%@",self.document.fileURL);
    NSURL *localURL = self.document.fileURL;
    CFURLRef docURLRef = (__bridge CFURLRef)localURL; // File URL
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL(docURLRef);
    const size_t numberOfPages = CGPDFDocumentGetNumberOfPages(pdf);
    NSLog(@"Total Page=%zu",numberOfPages);
    // new code
    UIView *makeimage=[[UIView alloc]init];
    makeimage=[self createView];
    NSMutableArray *storelayerimages;
    LayersView *layertrack;
    
    // new code
    NSString *fileName =@"";
    //fileName = [[NSString stringWithFormat:@"newone"] stringByAppendingFormat:@"%@",@".pdf"];
    fileName=self.document.fileName;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
    UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
    // new code for loop
    for (int loopForPdf = 0; loopForPdf < [self.document.pageCount integerValue]; loopForPdf++)
    {
        NSMutableArray *fechedArray = [layerStore.layerStore objectAtIndex:loopForPdf];
        // new code
        storelayerimages=[[NSMutableArray alloc]init];
        // end
        
        //	Get the current page and page frame
        size_t page=loopForPdf+1;
        CGPDFPageRef pdfPage = CGPDFDocumentGetPage(pdf, page);
        const CGRect pageFrame = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
        UIGraphicsBeginPDFPageWithInfo(pageFrame, nil);
        
        //	Draw the page (flipped)
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSaveGState(ctx);
        CGContextScaleCTM(ctx, 1, -1);
        CGContextTranslateCTM(ctx, 0, -pageFrame.size.height);
        CGContextDrawPDFPage(ctx, pdfPage);
        CGContextRestoreGState(ctx);
        // end
        
        for (int i = 0; i < fechedArray.count; i++)
        {
            LayerData *aData = [fechedArray objectAtIndex:i];
            layertrack  = [[LayersView alloc] initWithFrame:CGRectMake(0, 0, pageView.frame.size.width, pageView.frame.size.height)];
            layertrack.backgroundColor = [UIColor clearColor];
            layertrack.userInteractionEnabled = YES;
            layertrack.imageView.image  = (UIImage*)[UIImage imageWithData:aData.imageData];
            layertrack.tag = aData.layerindex;
            for (int j = 0 ; j < aData.aTextViewData.count; j++)
            {
                TextViewData *aTextViewData = [aData.aTextViewData objectAtIndex:j];
                CGRect gripFrame = CGRectMake(aTextViewData.xValue, aTextViewData.yValue, aTextViewData.width, aTextViewData.height);
                userResizableViews = [[SPUserResizableView alloc] initWithFrame:gripFrame];
                userResizableViews.backgroundColor = [UIColor clearColor];
                userResizableViews.userInteractionEnabled = NO;
                aTextView = [[UITextView alloc]initWithFrame:gripFrame];
                aTextView.backgroundColor = [UIColor clearColor];
                aTextView.font = [UIFont fontWithName:defaultFontName size:defaultFontSize];
                aTextView.delegate = self;
                aTextView.text = aTextViewData.textViewText;
                
                if (aTextViewData.fontColor)
                {
                    NSArray *items = [aTextViewData.fontColor componentsSeparatedByString:@" "];
                    //  NSLog(@"float:%f",[[items objectAtIndex:0] floatValue]);
                    if (items.count != 1)
                    {
                        CGFloat red = [[items objectAtIndex:1] floatValue];
                        CGFloat green = [[items objectAtIndex:2] floatValue];
                        CGFloat blue = [[items objectAtIndex:3] floatValue];
                        CGFloat alpha = [[items objectAtIndex:4] floatValue];
                        [aTextView setTextColor:[UIColor colorWithRed:red green:green blue:blue alpha:alpha]];
                    }
                }
                [aTextView setFont:[UIFont fontWithName:aTextViewData.fontName size:aTextViewData.fontSize]];
                userResizableViews.contentView = self.aTextView;
                userResizableViews.delegate = self;
                [userResizableViews showEditingHandles];
                currentlyEditingView = userResizableViews;
                lastEditedView = userResizableViews;
                [layertrack addSubview:userResizableViews];
                
                // new code
                [storelayerimages addObject:userResizableViews];
                
            }
            
            UIImage *finalimage=[[UIImage alloc]init];
            finalimage=[self getUIImageFromThisUIView:layertrack];
            // here i will implement blured concept
            
            for(int k=0;k<fechedArray.count-i;k++)
            {
                finalimage=[self imageWithGaussianBlur:finalimage];
            }
            
            // end
            
            UIImage *pdfimage=[[UIImage alloc]init];
            NSMutableData *imagedata=(NSMutableData*)UIImagePNGRepresentation(finalimage);
            pdfimage=[UIImage imageWithData:imagedata];
            [pdfimage drawInRect:pageFrame];
            
        }
    }
    // end
    UIGraphicsEndPDFContext();
    CGPDFDocumentRelease(pdf);
    pdf = nil;
    
    // delete in core data
    [self deletecoredata];
//    // new code
//     NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
//    DocumentFolder *sampleFolder;
//    NSString *documentsPath = [DocumentsUpdate documentsPath];
//    NSString *folderName = NSLocalizedString(@"Samples", @"name"); // Localized default folder name
//    
//    sampleFolder = [DocumentFolder insertInMOC:mainMOC name:folderName type:DocumentFolderTypeSamples]; // Insert it
//    
//    ReaderDocument *leaseDocument = [ReaderDocument insertInMOC:mainMOC name:fileName path:documentsPath];
//    leaseDocument.folder = sampleFolder;
  //   [[CoreDataManager sharedInstance] saveMainManagedObjectContext];
    // new code
    DocumentFolder *sampleFolder;
    ReaderDocument *object;
    NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
    NSError *error = nil;
    sampleFolder = [DocumentFolder folderInMOC:mainMOC type:DocumentFolderTypeSamples];
    
    // new one
    NSFetchRequest *request1 = [NSFetchRequest fetchRequestWithEntityName:@"ReaderDocument"];
    NSArray *matched1 = [mainMOC executeFetchRequest:request1 error:&error];
    if (matched1)
    {
        for(int i=0;i<[matched1 count];i++)
        {
            object=[matched1 objectAtIndex:i];
            if([object.fileName isEqualToString:fileName])
            {
                
                object.fileName = fileName; // Document file name
                
                object.guid = [ReaderDocument GUID]; // Document GUID
                
                object.pageNumber = [NSNumber numberWithInteger:1]; // Start on page 1
                
                object.filePath = [ReaderDocument relativeFilePath:documentsDirectory]; // Relative path to file
                
                object.lastOpen = [NSDate dateWithTimeIntervalSinceReferenceDate:0.0]; // Last opened
                
                object.fileDate = [NSDate dateWithTimeIntervalSinceReferenceDate:0.0]; // File date
                
                object.fileSize = [NSNumber numberWithUnsignedLongLong:0ull];
                
                if (![mainMOC save:&error])
                {
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                }
            }
        }
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; // User defaults
    [userDefaults setObject:nil forKey:kReaderSettingsCurrentFolder];
   
    
}
// new one
-(void)deletecoredata
{
    NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
    PdfDoc *pdfDoc = nil;
    NSError *error = nil;
    // delete pdf doc
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"PdfDoc"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"pdfName == %@",self.document.fileName]];
    NSArray *matched = [mainMOC executeFetchRequest:request error:&error];
    if (matched)
    {
        pdfDoc = [matched lastObject];
        if (pdfDoc)
        {
            [mainMOC deleteObject:pdfDoc];
        }
    }
    // delete pdf page
    NSMutableSet *set;
    for (int loopForPdf = 0; loopForPdf < [self.document.pageCount integerValue]; loopForPdf++)
    {
        PdfPage *pdfPage = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"PdfPage" inManagedObjectContext:mainMOC]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pageName == %@",[[NSString stringWithFormat:@"%@",self.document.fileName] stringByAppendingFormat:@"%d",loopForPdf]];
        [request setPredicate:predicate];
        NSError *error = nil;
        NSArray *results = [mainMOC executeFetchRequest:request error:&error];
        
        set = [[NSMutableSet alloc] initWithSet:pdfDoc.page];
        pdfPage = [results lastObject];
        
        NSMutableArray *currentView = [layerStore.layerStore objectAtIndex:loopForPdf];
        if (pdfPage)
        {
            if (currentView.count == 0 )
            {
                //Do nothing
            }
            else
            {
                [pdfPage setValue:[NSNumber numberWithInteger:loopForPdf+1] forKey:@"pageNumber"];
                self.annotationImageData = [NSData dataWithData:UIImagePNGRepresentation(self.image.image)];
                [pdfPage setValue:self.annotationImageData forKey:@"pageAnnoImage"];
                pdfPage = [results objectAtIndex:0];
                NSArray *allLayers = [pdfPage.layers allObjects];
                
                if(allLayers.count>0)
                {
                    pdfPage.layers=nil;
                }
                
            }
            [mainMOC deleteObject:pdfPage];
        }
    }
    
    // lastly save
    if (![mainMOC save:&error])
    {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    // new code
    
    
}

// new blur method
- (UIImage *)imageWithGaussianBlur:(UIImage *)imagesend
{
    float weight[5] = {0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162};
    for (int i = 0 ; i < 5 ; i++) {
        weight[i] = weight[i] * .9;
    }
    // Blur horizontally
    UIGraphicsBeginImageContext(imagesend.size);
    [imagesend drawInRect:CGRectMake(0, 0, imagesend.size.width, imagesend.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
    for (int x = 1; x < 5; ++x) {
        [imagesend drawInRect:CGRectMake(x, 0, imagesend.size.width, imagesend.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[x]];
        [imagesend drawInRect:CGRectMake(-x, 0, imagesend.size.width, imagesend.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[x]];
    }
    UIImage *horizBlurredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // Blur vertically
    UIGraphicsBeginImageContext(imagesend.size);
    [horizBlurredImage drawInRect:CGRectMake(0, 0, imagesend.size.width, imagesend.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
    for (int y = 1; y < 5; ++y) {
        [horizBlurredImage drawInRect:CGRectMake(0, y, imagesend.size.width, imagesend.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
        [horizBlurredImage drawInRect:CGRectMake(0, -y, imagesend.size.width, imagesend.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
    }
    UIImage *blurredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //
    return blurredImage;
}

// new code
-(void)savetoemail
{
    //    [self currentPageDataSave];
    //    // new code put in to a method
    //    [self saveincoredata];
    
    [self currentPageDataSave];
    // NSLog(@"document name=%@",self.document);
    NSLog(@"document url=%@",self.document.fileURL);
    NSURL *localURL = self.document.fileURL;
    CFURLRef docURLRef = (__bridge CFURLRef)localURL; // File URL
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL(docURLRef);
    const size_t numberOfPages = CGPDFDocumentGetNumberOfPages(pdf);
    NSLog(@"Total Page=%zu",numberOfPages);
    // new code
    UIView *makeimage=[[UIView alloc]init];
    makeimage=[self createView];
    NSMutableArray *storelayerimages;
    LayersView *layertrack;
    
    // new code
    NSString *fileName =@"";
    //fileName = [[NSString stringWithFormat:@"newone"] stringByAppendingFormat:@"%@",@".pdf"];
    fileName=self.document.fileName;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
    UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
    // new code for loop
    for (int loopForPdf = 0; loopForPdf < [self.document.pageCount integerValue]; loopForPdf++)
    {
        NSMutableArray *fechedArray = [layerStore.layerStore objectAtIndex:loopForPdf];
        // new code
        storelayerimages=[[NSMutableArray alloc]init];
        // end
        
        //	Get the current page and page frame
        size_t page=loopForPdf+1;
        CGPDFPageRef pdfPage = CGPDFDocumentGetPage(pdf, page);
        const CGRect pageFrame = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
        UIGraphicsBeginPDFPageWithInfo(pageFrame, nil);
        
        //	Draw the page (flipped)
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSaveGState(ctx);
        CGContextScaleCTM(ctx, 1, -1);
        CGContextTranslateCTM(ctx, 0, -pageFrame.size.height);
        CGContextDrawPDFPage(ctx, pdfPage);
        CGContextRestoreGState(ctx);
        // end
        
        for (int i = 0; i < fechedArray.count; i++)
        {
            LayerData *aData = [fechedArray objectAtIndex:i];
            layertrack  = [[LayersView alloc] initWithFrame:CGRectMake(0, 0, pageView.frame.size.width, pageView.frame.size.height)];
            layertrack.backgroundColor = [UIColor clearColor];
            layertrack.userInteractionEnabled = YES;
            layertrack.imageView.image  = (UIImage*)[UIImage imageWithData:aData.imageData];
            layertrack.tag = aData.layerindex;
            for (int j = 0 ; j < aData.aTextViewData.count; j++)
            {
                TextViewData *aTextViewData = [aData.aTextViewData objectAtIndex:j];
                CGRect gripFrame = CGRectMake(aTextViewData.xValue, aTextViewData.yValue, aTextViewData.width, aTextViewData.height);
                userResizableViews = [[SPUserResizableView alloc] initWithFrame:gripFrame];
                userResizableViews.backgroundColor = [UIColor clearColor];
                userResizableViews.userInteractionEnabled = NO;
                aTextView = [[UITextView alloc]initWithFrame:gripFrame];
                aTextView.backgroundColor = [UIColor clearColor];
                aTextView.font = [UIFont fontWithName:defaultFontName size:defaultFontSize];
                aTextView.delegate = self;
                aTextView.text = aTextViewData.textViewText;
                
                if (aTextViewData.fontColor)
                {
                    NSArray *items = [aTextViewData.fontColor componentsSeparatedByString:@" "];
                    //  NSLog(@"float:%f",[[items objectAtIndex:0] floatValue]);
                    if (items.count != 1)
                    {
                        CGFloat red = [[items objectAtIndex:1] floatValue];
                        CGFloat green = [[items objectAtIndex:2] floatValue];
                        CGFloat blue = [[items objectAtIndex:3] floatValue];
                        CGFloat alpha = [[items objectAtIndex:4] floatValue];
                        [aTextView setTextColor:[UIColor colorWithRed:red green:green blue:blue alpha:alpha]];
                    }
                }
                [aTextView setFont:[UIFont fontWithName:aTextViewData.fontName size:aTextViewData.fontSize]];
                userResizableViews.contentView = self.aTextView;
                userResizableViews.delegate = self;
                [userResizableViews showEditingHandles];
                currentlyEditingView = userResizableViews;
                lastEditedView = userResizableViews;
                [layertrack addSubview:userResizableViews];
                
                // new code
                [storelayerimages addObject:userResizableViews];
                
            }
            
            UIImage *finalimage=[[UIImage alloc]init];
            finalimage=[self getUIImageFromThisUIView:layertrack];
            // here i will implement blured concept
            
            for(int k=0;k<fechedArray.count-i;k++)
            {
                finalimage=[self imageWithGaussianBlur:finalimage];
            }
            
            // end
            
            UIImage *pdfimage=[[UIImage alloc]init];
            NSMutableData *imagedata=(NSMutableData*)UIImagePNGRepresentation(finalimage);
            pdfimage=[UIImage imageWithData:imagedata];
            [pdfimage drawInRect:pageFrame];
            
        }
    }
    // end
    UIGraphicsEndPDFContext();
    CGPDFDocumentRelease(pdf);
    pdf = nil;
    
    // delete in core data
    [self deletecoredata];
    [self emailpdf];
}


- (void) saveToDraft
{
    [self currentPageDataSave];
//    // new code put in to a method
    [self saveincoredata];
    
    
    
}
// new code for email
-(void)emailpdf
{
    NSString *body;
    body=@"";
    // new code
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init] ;
    controller.mailComposeDelegate = self;
    
    NSString *filename=self.document.fileName;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *pdfpath = [paths objectAtIndex:0];
    NSString *pdfFileName = [pdfpath stringByAppendingPathComponent:filename];
    
    
    NSURL *url = [NSURL fileURLWithPath:pdfFileName];
        
    NSData *attachment = [NSData dataWithContentsOfURL:url options:(NSDataReadingMapped|NSDataReadingUncached) error:nil];
        
    if (attachment!=nil)
    {
            //[controller setWantsFullScreenLayout:YES];
            [controller addAttachmentData:attachment mimeType:@"text/pdf" fileName:filename];
    }
        
        attachment=nil;
   
    
    //NSString *subject=@"Please find attached Gas Certificate ";
    
    NSString *subject=[[NSString alloc]init];
    NSString *subjecttitle=[[NSString alloc]init];
    subjecttitle=@"Please find attached ";
    subjecttitle=[subjecttitle stringByAppendingString:[NSString stringWithFormat:@" %@",filename]];
    //NSlog(@"%@",subjecttitle);
    subject=[subject stringByAppendingString:subjecttitle];
    //NSlog(@"%@",subject);
    [controller setSubject:subject];
    
    body=@" 'As requested please find attached PDF.' ";
    body=[body stringByAppendingString:@"\n"];
    
     NSMutableArray *arrEmail=[[NSMutableArray alloc]init] ;
    [arrEmail addObject:@"nivendru.gavaskar@gmail.com"];
    [controller setMessageBody:body isHTML:NO];
    //here we have to check
    [controller setToRecipients:arrEmail];
    
    if (controller)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent)
    {
       
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Mail sent successfully" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
            alert.tag=1001;
            [alert show];
        
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
// end

-(void)saveincoredata
{
    NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
    PdfDoc *pdfDoc = nil;
    NSError *error = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"PdfDoc"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"pdfName == %@",self.document.fileName]];
    NSArray *matched = [mainMOC executeFetchRequest:request error:&error];
    if (matched)
    {
        pdfDoc = [matched lastObject];
        if (!pdfDoc)
        {
            pdfDoc = [NSEntityDescription insertNewObjectForEntityForName:@"PdfDoc" inManagedObjectContext:mainMOC];
            [pdfDoc setValue:self.document.fileName forKey:@"pdfName"];
            [pdfDoc setValue:self.document.pageCount forKey:@"numberOfPages"];
            [pdfDoc setValue:self.document.filePath forKey:@"pdfPath"];
        }
    }
    NSMutableSet *set;
    NSMutableSet *textViewSet;
    for (int loopForPdf = 0; loopForPdf < [self.document.pageCount integerValue]; loopForPdf++)
    {
        PdfPage *pdfPage = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"PdfPage" inManagedObjectContext:mainMOC]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pageName == %@",[[NSString stringWithFormat:@"%@",self.document.fileName] stringByAppendingFormat:@"%d",loopForPdf]];
        [request setPredicate:predicate];
        NSError *error = nil;
        NSArray *results = [mainMOC executeFetchRequest:request error:&error];
        
        set = [[NSMutableSet alloc] initWithSet:pdfDoc.page];
        pdfPage = [results lastObject];
        
        NSMutableArray *currentView = [layerStore.layerStore objectAtIndex:loopForPdf];
        if (!pdfPage)
        {
            if (currentView.count == 0 )
            {
                //Do nothing
            }
            else
            {
                pdfPage = [NSEntityDescription insertNewObjectForEntityForName:@"PdfPage" inManagedObjectContext:mainMOC];
                [pdfPage setValue:[NSNumber numberWithInteger:loopForPdf+1] forKey:@"pageNumber"];
                self.annotationImageData = [NSData dataWithData:UIImagePNGRepresentation(self.image.image)];
                [pdfPage setValue:self.annotationImageData forKey:@"pageAnnoImage"];
                [pdfPage setValue:[[NSString stringWithFormat:@"%@",self.document.fileName] stringByAppendingFormat:@"%d",loopForPdf] forKey:@"pageName"];
                
                NSFetchRequest *pdfLayerRequest = [[NSFetchRequest alloc] init];
                [pdfLayerRequest setEntity:[NSEntityDescription entityForName:@"PdfLayers" inManagedObjectContext:mainMOC]];
                NSArray *pdfLayerResult = [mainMOC executeFetchRequest:pdfLayerRequest error:&error];
                NSMutableSet *layerSet = [[NSMutableSet alloc] initWithSet:pdfPage.layers];
                PdfLayers *pdfLayer = [pdfLayerResult lastObject];
                NSArray *allLayers = [pdfPage.layers allObjects];
                
                if (allLayers.count == 0)
                {
                    for (LayerData *layerView in currentView)
                    {
                        pdfLayer = [NSEntityDescription insertNewObjectForEntityForName:@"PdfLayers" inManagedObjectContext:mainMOC];
                        [pdfLayer setValue:layerView.imageData forKey:@"layerAnnotationImage"];
                        [pdfLayer setValue:[NSNumber numberWithInt:layerView.layerindex] forKey:@"layerindex"];
                        [pdfLayer setValue:layerView.layerName forKey:@"layerName"];
                        [pdfLayer setValue:self.document.fileName forKey:@"layerForPage"];
                        [pdfLayer setValue:[NSNumber numberWithInt:layerView.pageIndex] forKey:@"pageIndex"];
                        
                        NSFetchRequest *textViewRequest = [[NSFetchRequest alloc] init];
                        [textViewRequest setEntity:[NSEntityDescription entityForName:@"TextView" inManagedObjectContext:mainMOC]];
                        NSMutableArray *textViewResult = [[mainMOC executeFetchRequest:textViewRequest error:&error] mutableCopy];
                        textViewSet = [[NSMutableSet alloc] initWithSet:pdfLayer.textView];
                        TextView *textView = [textViewResult lastObject];
                        NSArray *allTextViews = [pdfLayer.textView allObjects];
                        
                        if (allTextViews.count == 0)
                        {
                            for (TextViewData *textViewData in layerView.aTextViewData)
                            {
                                textView = [NSEntityDescription insertNewObjectForEntityForName:@"TextView" inManagedObjectContext:mainMOC];
                                [textView setValue:[NSNumber numberWithFloat:textViewData.xValue] forKey:@"textviewCoordinateX"];
                                [textView setValue:[NSNumber numberWithFloat:textViewData.yValue] forKey:@"textviewCoordinateY"];
                                [textView setValue:[NSNumber numberWithFloat:textViewData.width] forKey:@"width"];
                                [textView setValue:[NSNumber numberWithFloat:textViewData.height] forKey:@"height"];
                                [textView setValue:[NSNumber numberWithInt:textViewData.textViewIndex] forKey:@"textviewIndex"];
                                [textView setValue:textViewData.textViewText forKey:@"textViewStirng"];
                                [textView setValue:textViewData.fontColor forKey:@"fontColor"];
                                [textView setValue:textViewData.fontName forKey:@"fontName"];
                                [textView setValue:[NSNumber numberWithInt:textViewData.fontSize] forKey:@"fontSize"];
                                [textViewSet addObject:textView];
                                pdfLayer.textView = textViewSet;
                            }
                        }
                        [layerSet addObject:pdfLayer];
                        pdfPage.layers = layerSet;
                    }
                    [set addObject:pdfPage];
                    pdfDoc.page = set;
                }
            }
        }
        else
        {
            if (currentView.count == 0 )
            {
                //Do nothing
            }
            else
            {
                [pdfPage setValue:[NSNumber numberWithInteger:loopForPdf+1] forKey:@"pageNumber"];
                self.annotationImageData = [NSData dataWithData:UIImagePNGRepresentation(self.image.image)];
                [pdfPage setValue:self.annotationImageData forKey:@"pageAnnoImage"];
                pdfPage = [results objectAtIndex:0];
                NSArray *allLayers = [pdfPage.layers allObjects];
                int count = 0;
                for (int i = 0; i < allLayers.count; i++)
                {
                    PdfLayers *pdfLayer = [allLayers objectAtIndex:i];
                    LayerData *layerData = [currentView objectAtIndex:i];
                    [pdfLayer setValue:layerData.imageData forKey:@"layerAnnotationImage"];
                    [pdfLayer setValue:[NSNumber numberWithInt:layerData.layerindex] forKey:@"layerindex"];
                    [pdfLayer setValue:layerData.layerName forKey:@"layerName"];
                    [pdfLayer setValue:[NSNumber numberWithInt:layerData.pageIndex] forKey:@"pageIndex"];
                    NSArray *allTextViews = [pdfLayer.textView allObjects];
                    int textViewCount = 0;
                    for (int j = 0; j < allTextViews.count; j++)
                    {
                        TextView *textView = [allTextViews objectAtIndex:j];
                        TextViewData *textViewData = [layerData.aTextViewData objectAtIndex:j];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.xValue] forKey:@"textviewCoordinateX"];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.yValue] forKey:@"textviewCoordinateY"];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.width] forKey:@"width"];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.height] forKey:@"height"];
                        [textView setValue:[NSNumber numberWithInt:textViewData.textViewIndex] forKey:@"textviewIndex"];
                        [textView setValue:textViewData.textViewText forKey:@"textViewStirng"];
                        [textView setValue:textViewData.fontColor forKey:@"fontColor"];
                        [textView setValue:textViewData.fontName forKey:@"fontName"];
                        [textView setValue:[NSNumber numberWithInt:textViewData.fontSize] forKey:@"fontSize"];
                        textViewCount++;
                    }
                    if (textViewCount != layerData.aTextViewData.count)
                    {
                        TextViewData *textViewData = [layerData.aTextViewData objectAtIndex:textViewCount];
                        TextView *textView = [NSEntityDescription insertNewObjectForEntityForName:@"TextView" inManagedObjectContext:mainMOC];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.xValue] forKey:@"textviewCoordinateX"];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.yValue] forKey:@"textviewCoordinateY"];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.width] forKey:@"width"];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.height] forKey:@"height"];
                        [textView setValue:[NSNumber numberWithInt:textViewData.textViewIndex] forKey:@"textviewIndex"];
                        [textView setValue:textViewData.textViewText forKey:@"textViewStirng"];
                        [textView setValue:textViewData.fontColor forKey:@"fontColor"];
                        [textView setValue:textViewData.fontName forKey:@"fontName"];
                        [textView setValue:[NSNumber numberWithInt:textViewData.fontSize] forKey:@"fontSize"];
                        [pdfLayer addTextViewObject:textView];
                    }
                    count++;
                }
                if (count != currentView.count)
                {
                    LayerData *layerData = [currentView objectAtIndex:count];
                    PdfLayers *pdfLayer = [NSEntityDescription insertNewObjectForEntityForName:@"PdfLayers" inManagedObjectContext:mainMOC];
                    [pdfLayer setValue:layerData.imageData forKey:@"layerAnnotationImage"];
                    [pdfLayer setValue:[NSNumber numberWithInt:layerData.layerindex] forKey:@"layerindex"];
                    [pdfLayer setValue:layerData.layerName forKey:@"layerName"];
                    
                    for (TextViewData *textViewData in layerData.aTextViewData)
                    {
                        TextView *textView = [NSEntityDescription insertNewObjectForEntityForName:@"TextView" inManagedObjectContext:mainMOC];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.xValue] forKey:@"textviewCoordinateX"];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.yValue] forKey:@"textviewCoordinateY"];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.width] forKey:@"width"];
                        [textView setValue:[NSNumber numberWithFloat:textViewData.height] forKey:@"height"];
                        [textView setValue:[NSNumber numberWithInt:textViewData.textViewIndex] forKey:@"textviewIndex"];
                        [textView setValue:textViewData.textViewText forKey:@"textViewStirng"];
                        [textView setValue:textViewData.fontColor forKey:@"fontColor"];
                        [textView setValue:textViewData.fontName forKey:@"fontName"];
                        [textView setValue:[NSNumber numberWithInt:textViewData.fontSize] forKey:@"fontSize"];
                        [pdfLayer addTextViewObject:textView];
                    }
                    [pdfPage addLayersObject:pdfLayer];
                }
            }
        }
        if (![mainMOC save:&error])
        {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
}

// new code for bluring view

-(UIImage*)getUIImageFromThisUIView:(UIView*)aUIView
{
    UIGraphicsBeginImageContext(aUIView.bounds.size);
    // UIGraphicsBeginImageContextWithOptions(aUIView.bounds.size, aUIView.opaque, 0.0);
    [aUIView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}

- (void) refreshDrawing
{
    
    UIGraphicsBeginImageContextWithOptions(pageView.frame.size, NO, 1.5f);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    [self.image.image drawInRect:pageView.bounds];
    [annotationStore drawAnnotationsForPage:self.currentPage inContext:currentContext];
    CGContextSetShouldAntialias(currentContext, YES);
    CGContextSetLineJoin(currentContext, kCGLineJoinRound);
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_RedPen])
    {
    CGContextSetLineCap(currentContext, kCGLineCapRound);
    CGContextSetLineWidth(currentContext,markerLineWidth );
    CGContextSetStrokeColorWithColor(currentContext, markerColor);
    }
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_Sign])
    {
    CGContextSetLineCap(currentContext, kCGLineCapRound);
    CGContextSetLineWidth(currentContext, pencilLineWidth);
    CGContextSetStrokeColorWithColor(currentContext, pencilColor);
    }
    for (UIBezierPath *path in currentPaths)
    {
        CGContextAddPath(currentContext, path.CGPath);
    }
    CGContextAddPath(currentContext, currPath);
    CGContextStrokePath(currentContext);
    self.image.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [touches anyObject];
    
    //    for (UIView *aView in pageView.subviews) {
    //
    //        if (![aView isKindOfClass:[UIImageView class]]) {
    
    layer.lastPoint = [touch locationInView:self.layer];
    
    
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_Text])
    {
        return;
    }
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_Hand])
    {
        // Do Nothing
    }

    if (self.layer.currPath)
    {
        [self.layer.currentPaths addObject:[UIBezierPath bezierPathWithCGPath:self.layer.currPath]];
    }
    self.layer.currPath = CGPathCreateMutable();
    CGPathMoveToPoint(self.layer.currPath, NULL, self.layer.lastPoint.x, self.layer.lastPoint.y);
    
    //        }else{
    //
    //            lastPoint = [touch locationInView:pageView];
    //            if ([self.annotationType isEqualToString:AnnotationViewControllerType_Text]) {
    //                //Do Nothing
    //            }
    //            if (currPath) {
    //                [currentPaths addObject:[UIBezierPath bezierPathWithCGPath:currPath]];
    //            }
    //            currPath = CGPathCreateMutable();
    //            CGPathMoveToPoint(currPath, NULL, lastPoint.x, lastPoint.y);
    //        }
    didMove = NO;
    //    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    didMove = YES;
    UITouch *touch = [touches anyObject];
    
    //    NSLog(@"pa:%@",pageView.subviews);
    //
    //    for (UIView *aView in pageView.subviews) {
    //
    //        if (![aView isKindOfClass:[UIImageView class]]) {
    //
    self.layer.currentPoint = [touch locationInView:self.layer];
    
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_Hand])
    {
        
        //Do Nothing
        
    }
    else
    {
        
        //----------Update path-----------//
        CGPathAddLineToPoint(self.layer.currPath, NULL, self.layer.currentPoint.x, self.layer.currentPoint.y);
        
        if ([self.annotationType isEqualToString:AnnotationViewControllerType_Sign])
        {
            NSLog(@"%@",self.pencilColor);
            if(!self.pencilColor)
            {
            self.pencilColor=[UIColor redColor].CGColor;
            }
            
            //----------Drawing the lines on layers------------//
            // new code
            
            NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
            id p=[def valueForKey:@"PencileValue"];
            if([p floatValue]==0.0)
            {
                self.pencilLineWidth=1.0;
                
            }
            // end
            
            
            [self.layer drawLinesInLayers:self.pencilColor withPencilWidth:self.pencilLineWidth];
            
        }
        else if ([self.annotationType isEqualToString:AnnotationViewControllerType_RedPen])
        {
                        
            NSLog(@"marker");
            const CGFloat *components;
            if(!self.markerColor)
            {
                self.markerColor=[UIColor redColor].CGColor;
                components = CGColorGetComponents(self.markerColor);
            }
            else
            {
            components = CGColorGetComponents(self.markerColor);
            }
            
            
            NSString *colorAsString = [NSString stringWithFormat:@"%f,%f,%f,%f", components[0], components[1], components[2], components[3]];
           
            NSArray *component = [colorAsString componentsSeparatedByString:@","];
            CGFloat r = [[component objectAtIndex:0] floatValue];
            CGFloat g = [[component objectAtIndex:1] floatValue];
            CGFloat b = [[component objectAtIndex:2] floatValue];
            CGFloat a = [[component objectAtIndex:3] floatValue];
           
            UIColor *color = [UIColor colorWithRed:r green:g blue:b alpha:a-0.90];
            
            // new code
            
           // NSLog(@"%f",self.markerLineWidth);
            
            // end
            
            
            [self.layer drawLinesInLayers:color.CGColor withPencilWidth:self.markerLineWidth];
            
        }
        else if([self.annotationType isEqualToString:AnnotationViewControllerType_Eraser])
        {
            self.layer.currentPaths = nil;
            [self.layer eraseLines:eraserLineWith];
            
        }else
        {
            // Do Nothing
        }
    }
   // self.layer.lastPoint = self.layer.currentPoint;
    
    
    //        }else{
    //
    //
    //            currentPoint = [touch locationInView:pageView];
    //            if ([self.annotationType isEqualToString:AnnotationViewControllerType_Hand]) {
    //
    //                //userResizableViews.userInteractionEnabled = YES;
    //
    //            }else{
    //
    //                if ([self.annotationType isEqualToString:AnnotationViewControllerType_RedPen] || [self.annotationType isEqualToString:AnnotationViewControllerType_Sign]) {
    //
    //                    //-------- Drawing line on pdf page------//
    //                    //Update path
    //                    CGPathAddLineToPoint(currPath, NULL, currentPoint.x, currentPoint.y);
    //                    [self refreshDrawing];
    //
    //                }else if([self.annotationType isEqualToString:AnnotationViewControllerType_Eraser]){
    //                    //----------Erase Lines on pdf page-------//
    //                    [self eraseLines];
    //
    //                }else{
    //
    //                    // Do Nothing
    //                }
    //            }
    //            lastPoint = currentPoint;
    //        }
    //    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [touches anyObject];
    
    //    for (UIView *aView in pageView.subviews) {
    //
    //        if (![aView isKindOfClass:[UIImageView class]]) {
    //
    //
    //
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_Text])
    {
        return;
    }
    if ([self.annotationType isEqualToString:AnnotationViewControllerType_Hand])
    {
        //Do Nothing
    }
    if (!didMove)
    {
        
        self.layer.currentPoint = [touch locationInView:self.layer];
        CGPathAddEllipseInRect(self.layer.currPath, NULL, CGRectMake(self.layer.currentPoint.x - 2.f, self.layer.currentPoint.y - 2.f, 4.f, 4.f));
        
        if ([self.annotationType isEqualToString:AnnotationViewControllerType_Sign] )
        {
            //------- Drawing lines on layers------------------//
            [self.layer drawLinesInLayers:self.pencilColor withPencilWidth:self.pencilLineWidth];
            
        }else if ([self.annotationType isEqualToString:AnnotationViewControllerType_RedPen]){
            
            NSLog(@"marker ended");
            const CGFloat *components = CGColorGetComponents(self.markerColor);
            NSString *colorAsString = [NSString stringWithFormat:@"%f,%f,%f,%f", components[0], components[1], components[2], components[3]];
            
            NSArray *component = [colorAsString componentsSeparatedByString:@","];
            CGFloat r = [[component objectAtIndex:0] floatValue];
            CGFloat g = [[component objectAtIndex:1] floatValue];
            CGFloat b = [[component objectAtIndex:2] floatValue];
            CGFloat a = [[component objectAtIndex:3] floatValue];
            
            UIColor *color = [UIColor colorWithRed:r green:g blue:b alpha:a-0.90];
            
            [self.layer drawLinesInLayers:color.CGColor withPencilWidth:self.markerLineWidth];
            
        }
        else if([self.annotationType isEqualToString:AnnotationViewControllerType_Eraser])
        {
            //-------------Erase the lines on layer-----------//
            [self.layer eraseLines:self.eraserLineWith];
            
        }else
        {
            // Do Nothing
        }
    }
    
    //        }else{
    //            if ([self.annotationType isEqualToString:AnnotationViewControllerType_Text]) {
    //                return;
    //            }
    //            if (!didMove) {
    //
    //                currentPoint = [touch locationInView:pageView];
    //
    //
    //                if ([self.annotationType isEqualToString:AnnotationViewControllerType_RedPen] || [self.annotationType isEqualToString:AnnotationViewControllerType_Sign]) {
    //
    //                    //-------- Drawing lines on pdf page------//
    //                    CGPathAddEllipseInRect(currPath, NULL, CGRectMake(currentPoint.x - 2.f, currentPoint.y - 2.f, 4.f, 4.f));
    //                    [self refreshDrawing];
    //
    //                }else if([self.annotationType isEqualToString:AnnotationViewControllerType_Eraser]){
    //                    //----------Erase Lines on pdf page-------//
    //                    [self eraseLines];
    //
    //                }else{
    //
    //                    // Do Nothing
    //                }
    //            }
    //        }
    didMove = NO;
    //    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)eraseLines{

    UIGraphicsBeginImageContextWithOptions(pageView.frame.size, NO, 1.5f);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    [self.image.image drawInRect:pageView.bounds];
    CGContextSetShouldAntialias(currentContext, YES);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 20);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, currentPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    
    //-----------Draw Paths-------------------//
    CGContextStrokePath(currentContext);
    //-------------Saving---------------------//
    self.image.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //self.annotationImageData = [NSData dataWithData:UIImagePNGRepresentation(self.image.image)];
}
// SPUserResizableViewDelegate delete method
- (void)deleteResizableViewWithTag:(int)tag
{
    NSLog(@"deleteResizableViewWithTag");
    NSLog(@"%@",layerIndex);
    
    
    for (UIView *removeimage in self.containerView.subviews)
    {
        
        if([removeimage isKindOfClass:[UIImageView class]])
        {
            if([removeimage tag]==layerIndex.layerindex)
            {
                [removeimage removeFromSuperview];
            }
        }
    }
    
    
    for ( LayersView *trackcurrentview in self.containerView.subviews)
    {
        if([trackcurrentview isKindOfClass:[LayersView class]])
        {
            if([trackcurrentview tag]==layerIndex.layerindex)
            {
                for (UIView *v in trackcurrentview.subviews)
                {
                    if([v isKindOfClass:[userResizableViews class]])
                    {
                        if([v tag]==tag)
                        {
                            [v removeFromSuperview];
                        }
                    }
                }
            }
        }
    }
    
    // new code
    
    int positionremove=-1;
    
    for (int k=0;k<[layerIndex.aTextViewIndexArray count];k++)
    {
        TextViewIndex *text=[layerIndex.aTextViewIndexArray objectAtIndex:k];
        if(text.textViewIndex==tag)
        {
            positionremove=k;
        }
    }
    
    // end
    if(positionremove>=0)
    {
        [layerIndex.aTextViewIndexArray removeObjectAtIndex:positionremove];
    }
    
    NSLog(@"%d",[layerIndex.aTextViewIndexArray count]);
    
    if([layerIndex.aTextViewIndexArray count]==0)
    {
        [delegate handbuttonchange:YES];
    }
    
    
}

@end
