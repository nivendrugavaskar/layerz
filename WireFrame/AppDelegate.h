//
//  AppDelegate.h
//  WireFrame
//
//  Created by Raghavender  on 10/03/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
- (void)prePopulateCoreData;
@end
