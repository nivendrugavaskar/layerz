//
//  AppDelegate.m
//  WireFrame
//
//  Created by Raghavender  on 10/03/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "AppDelegate.h"
#import "CoreDataManager.h"
#import "DocumentFolder.h"
#import "DocumentsUpdate.h"
#import "ReaderDocument.h"
#import "LibraryDocumentsView.h"


@implementation AppDelegate{
    LibraryDocumentsView *documentsView;
}


- (void)prePopulateCoreData
{
    
    DocumentFolder *sampleFolder;
    
	NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
    
    // open for simulator
    /*
//    
//    if ([DocumentFolder existsInMOC:mainMOC type:DocumentFolderTypeDefault] == NO) // Add default folder
//	{
//		NSString *folderName = NSLocalizedString(@"Documents", @"name"); // Localized default folder name
//        
//        [DocumentFolder insertInMOC:mainMOC name:folderName type:DocumentFolderTypeDefault];
//	}
//    if ([DocumentFolder existsInMOC:mainMOC type:DocumentFolderTypeSamples] == NO) // Add samples folder
//	{
//		NSString *folderName = NSLocalizedString(@"Samples", @"name"); // Localized default folder name
//		
//        sampleFolder = [DocumentFolder insertInMOC:mainMOC name:folderName type:DocumentFolderTypeSamples]; // Insert it
//        [[DocumentsUpdate sharedInstance] addSampleDocumentsInFolder:sampleFolder];
//	}*/
    
    
  //   close for simulator test
    
    NSMutableArray *newfiles=[self allfiles];
    
    // new code
    if([newfiles count]==0)
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; // User defaults
        [userDefaults setObject:nil forKey:@"CurrentFolder"];
        [userDefaults synchronize];
//        
        //new code
        NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
        DocumentFolder *doc=nil;
        NSError *error = nil;
        NSFetchRequest *request = [NSFetchRequest new]; // Fetch request instance
        [request setEntity:[NSEntityDescription entityForName:@"DocumentFolder" inManagedObjectContext:mainMOC]];
        //[request setPredicate:[NSPredicate predicateWithFormat:@"type == DocumentFolderTypeSamples"]]; // Type predicate
       
        [request setPropertiesToFetch:@[@"name"]];
        
        NSArray *matched = [mainMOC executeFetchRequest:request error:&error];
        
       // NSMutableArray *countries = [matched valueForKey:@"name"];
        
        if (matched)
        {
            for(int k=0;k<[matched count];k++)
            {
            doc = [matched objectAtIndex:k];
            if (doc)
            {
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; // User defaults
                    [userDefaults setObject:nil forKey:@"CurrentFolder"];
                    [userDefaults synchronize];
                
                // new code
               // CurrentDocument
               // [userDefaults objectForKey:kReaderSettingsCurrentDocument]
                
                    
                    //end
                    [doc setValue:nil forKey:@"documents"];
                    [mainMOC deleteObject:doc];
               
            }
            }
        }
        if (![mainMOC save:&error])
        {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        // end
        
      //  kind	DocumentFolderType	DocumentFolderTypeSamples	DocumentFolderTypeSamples
        
    }
    else
    {
    for(int i=0;i<[newfiles count];i++)
    {
    int location=[[newfiles objectAtIndex:i] rangeOfString:@"Documents/"].location;
    NSString *filenametest=[[newfiles objectAtIndex:i] substringFromIndex:location+10];
    
        // new code
         DocumentFolder *doc=nil;
        NSError *error = nil;
        NSFetchRequest *request = [NSFetchRequest new]; // Fetch request instance
        [request setEntity:[NSEntityDescription entityForName:@"DocumentFolder" inManagedObjectContext:mainMOC]];
       // [request setPredicate:[NSPredicate predicateWithFormat:@"name=="]]; // Type predicate
        [request setPropertiesToFetch:@[@"name"]];
        
       
        
        NSArray *matched = [mainMOC executeFetchRequest:request error:&error];
        
        NSMutableArray *countries = [matched valueForKey:@"name"];
        
        
        
        
        if (matched)
        {
            for(int k=0;k<[matched count];k++)
            {
                doc = [matched objectAtIndex:k];
                if (doc)
                {
                    // new code
                    if(![filenametest isEqualToString:[countries objectAtIndex:k]])
                    {
                        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; // User defaults
                        [userDefaults setObject:nil forKey:@"CurrentFolder"];
                        [userDefaults synchronize];

                    //end
                    [doc setValue:nil forKey:@"documents"];
                    [mainMOC deleteObject:doc];
                    }
                }
            }
        }
        
        if (![mainMOC save:&error])
        {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        //end
        
    
    if([DocumentFolder existsInMOC:mainMOC name:filenametest]==NO)
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; // User defaults
        [userDefaults setObject:nil forKey:@"CurrentFolder"];
        [userDefaults synchronize];
       // NSString *folderName = NSLocalizedString(@"Samples", @"name");
        sampleFolder = [DocumentFolder insertInMOC:mainMOC name:filenametest type:DocumentFolderTypeSamples]; // Insert it
        [[DocumentsUpdate sharedInstance] addSampleDocumentsInFolder1: sampleFolder: newfiles];
        
        // new code
        //[DocumentFolder insertInMOC:<#(NSManagedObjectContext *)#> name:<#(NSString *)#> type:<#(DocumentFolderType)#>]
    }
    }
    }
    //end
    
   // NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
    DocumentFolder *doc=nil;
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest new]; // Fetch request instance
    [request setEntity:[NSEntityDescription entityForName:@"DocumentFolder" inManagedObjectContext:mainMOC]];
    //[request setPredicate:[NSPredicate predicateWithFormat:@"type == DocumentFolderTypeSamples"]]; // Type predicate
    NSArray *matched = [mainMOC executeFetchRequest:request error:&error];

    
    
   // [self getAllDocuments:sampleFolder];
    
}

// new method to get data
-(NSMutableArray *)allfiles
{
    // new code
    NSMutableArray *retval = [NSMutableArray array];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *publicDocumentsDir = [paths objectAtIndex:0];
    
    NSError *error;
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:publicDocumentsDir error:&error];
    if (files == nil)
    {
        NSLog(@"Error reading contents of documents directory: %@", [error localizedDescription]);
        return retval;
    }
    
    for (NSString *file in files) {
        if ([file.pathExtension compare:@"pdf" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            NSString *fullPath = [publicDocumentsDir stringByAppendingPathComponent:file];
            [retval addObject:fullPath];
        }
    }
    
    return retval;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self prePopulateCoreData];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
