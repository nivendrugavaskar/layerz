//
//  SevenColorView.h
//  调色板
//
//  Created by long on 13-6-18.
//  Copyright (c) 2013年 long. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ColorViewDelegate
@required

@optional

- (void)ColorViewChangeColor:(CGColorRef)_color;

@end

@interface ColorView : UIView{
    
    UIButton *btn[7];
    
    UILabel *label[7];
}

@property (strong, nonatomic) id<ColorViewDelegate> ColorViewDelegate;

@end
