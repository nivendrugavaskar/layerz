//
//  ColorView.m

//
//  Created by long on 13-6-18.
//  Copyright (c) 2013 long. All rights reserved.
//

#import "ColorView.h"

@implementation ColorView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        for (int i=1; i<6; i++) {

            btn[i] = [UIButton buttonWithType:UIButtonTypeCustom];
            
            btn[i].frame = CGRectMake(5+(i-1)*33, 0, 25, 30);
         
            btn[1].backgroundColor = [UIColor redColor];
            btn[2].backgroundColor = [UIColor orangeColor];
            btn[3].backgroundColor = [UIColor yellowColor];
            btn[4].backgroundColor = [UIColor greenColor];
            btn[5].backgroundColor = [UIColor blueColor];
 
            btn[i].tag = i;
            
            [btn[i] addTarget:self action:@selector(btn:) forControlEvents:UIControlEventTouchUpInside];
            
            [self addSubview:btn[i]];
           
        }

    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)btn:(id)sender
{

    UIButton *b = (UIButton *)sender;

    switch (b.tag) {
        case 1:
            [self.ColorViewDelegate ColorViewChangeColor:[UIColor redColor].CGColor];
            break;
        case 2:
            [self.ColorViewDelegate ColorViewChangeColor:[UIColor orangeColor].CGColor];
            break;
        case 3:
            [self.ColorViewDelegate ColorViewChangeColor:[UIColor yellowColor].CGColor];
            break;
        case 4:
            [self.ColorViewDelegate ColorViewChangeColor:[UIColor greenColor].CGColor];
            break;
        case 5:
            [self.ColorViewDelegate ColorViewChangeColor:[UIColor blueColor].CGColor];
            break;
                default:
            break;
    }
    
}
@end
