//
//  EraserPopOverView.h
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EraserPopOverView : UIView

@property (strong, nonatomic) UILabel  *aEraserWidthLabel;
@property (strong, nonatomic) UISlider *aEraserSlider;
@end
