//
//  EraserPopOverView.m
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "EraserPopOverView.h"

@implementation EraserPopOverView
@synthesize aEraserSlider;
@synthesize aEraserWidthLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        self.aEraserWidthLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 120, 30)];
        self.aEraserWidthLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.aEraserWidthLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.aEraserWidthLabel.text = @"Width";
        [self addSubview:self.aEraserWidthLabel];
        
        self.aEraserSlider = [[UISlider alloc] initWithFrame:CGRectMake(110, 10, 170, 15)];
        [self.aEraserSlider setThumbImage:[UIImage imageNamed:@"slider_gripper"] forState:UIControlStateNormal];
        [self.aEraserSlider setMinimumTrackImage:[UIImage imageNamed:@"slider_select"] forState:UIControlStateNormal];
        [self.aEraserSlider setMaximumTrackImage:[UIImage imageNamed:@"slider_base"] forState:UIControlStateNormal];
        
        self.aEraserSlider.minimumValue = 1.0;
        self.aEraserSlider.maximumValue = 30.0;
        self.aEraserSlider.continuous = YES;
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        self.aEraserSlider.value  = [[def valueForKey:@"EraserValue"] floatValue];
        [self addSubview:self.aEraserSlider];

    
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context,2.0);
    CGContextSetBlendMode(context, kCGBlendModeClear);
    CGContextSetStrokeColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextBeginPath(context);
    CGContextStrokePath(context);
    CGContextFlush(context);

}


@end
