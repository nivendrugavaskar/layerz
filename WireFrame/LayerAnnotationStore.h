//
//  LayerAnnotationStore.h
//  Layerz
//
//  Created by Raghavender  on 07/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LayerAnnotations.h"

@interface LayerAnnotationStore : NSObject

- (id)initWithPageCount:(int)page_count;
- (void) addAnnotation:(LayerAnnotations*)annotation toPage:(int)page;
- (void) addPath:(CGPathRef)path withColor:(CGColorRef)color fill:(BOOL)fill toPage:(int)page;
- (void) addAnnotations:(LayerAnnotationStore*)annotations;

- (void) undoAnnotationOnPage:(int)page;

- (NSArray*) annotationsForPage:(int)page;
- (void) drawAnnotationsForPage:(int)page inContext:(CGContextRef) context;

@end
