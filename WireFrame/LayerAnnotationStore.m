//
//  LayerAnnotationStore.m
//  Layerz
//
//  Created by Raghavender  on 07/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "LayerAnnotationStore.h"
#import "LayerAnnotations.h"

@implementation LayerAnnotationStore
{
    
    NSArray *annotations;
}
- (id)initWithPageCount:(int)page_count {
    NSMutableArray *temp = [NSMutableArray arrayWithCapacity:page_count];
    for (int i = 0; i < page_count; i++) {
        [temp addObject:[NSMutableArray array]];
    }
    annotations = [NSArray arrayWithArray:temp];
    return self;
}

- (void) addAnnotation:(LayerAnnotations*)annotation toPage:(int)page {
    NSMutableArray *pageAnnotations = [annotations objectAtIndex:(page-1)];
    //Each page is a queue, first annotation at 0
    [pageAnnotations addObject:annotation];
}

- (void) addPath:(CGPathRef)path withColor:(CGColorRef)color lineWidth:(CGFloat)width fill:(BOOL)fill toPage:(int)page {
    [self addAnnotation:[LayerPathAnnotation pathAnnotationWithPath:path color:color lineWidth:width fill:fill] toPage:page];
}

- (void) addPath:(CGPathRef)path withColor:(CGColorRef)color fill:(BOOL)fill toPage:(int)page {
    [self addAnnotation:[LayerPathAnnotation pathAnnotationWithPath:path color:color fill:fill] toPage:page];
}
- (void) addAnnotations:(LayerAnnotationStore *)newAnnotations {
    int count = [annotations count];
    for (int page = 1; page <= count; page++) {
        NSMutableArray *pageAnnotations = [annotations objectAtIndex:(page - 1)];
        NSArray *otherAnnotations = [newAnnotations annotationsForPage:page];
        [pageAnnotations addObjectsFromArray:otherAnnotations];
    }
}

- (void) undoAnnotationOnPage:(int)page {
    if (page - 1 >= [annotations count]) {
        return;
    }
    
    NSMutableArray* pageAnnotations = [annotations objectAtIndex:(page-1)];
    if ([pageAnnotations count] > 0) {
        [pageAnnotations removeLastObject];
    }
}

- (void)empty {
    int count = [annotations count];
    NSMutableArray *temp = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        [temp addObject:[NSMutableArray array]];
    }
    annotations = [NSArray arrayWithArray:temp];
}

- (NSArray*) annotationsForPage:(int)page {
    if (page - 1 >= [annotations count]) {
        NSLog(@"We wanted index %d but only have %d items", page - 1 , [annotations count]);
        return [NSArray array];
    }
    return [annotations objectAtIndex:(page-1)];
}

- (void) drawAnnotationsForPage:(int)page inContext:(CGContextRef) context {
    NSArray *pageAnnotations = [self annotationsForPage:page];
    if (!pageAnnotations) {
        return;
    }
    for (LayerAnnotations *anno in pageAnnotations) {
        [anno drawInContext:context];
    }
}

@end
