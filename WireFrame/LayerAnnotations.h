//
//  LayerAnnotations.h
//  Layerz
//
//  Created by Raghavender  on 07/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LayerAnnotations : NSObject

- (void) drawInContext:(CGContextRef) context;

@end
@interface LayerPathAnnotation: LayerAnnotations
@property CGPathRef path;
@property CGColorRef color;
@property BOOL fill;
@property CGFloat lineWidth;
+ (id) pathAnnotationWithPath:(CGPathRef)path color:(CGColorRef)color fill:(BOOL)fill;
+ (id) pathAnnotationWithPath:(CGPathRef)path color:(CGColorRef)color lineWidth:(CGFloat)width fill:(BOOL)fill;
@end