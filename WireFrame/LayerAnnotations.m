//
//  LayerAnnotations.m
//  Layerz
//
//  Created by Raghavender  on 07/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "LayerAnnotations.h"


@class LayerAnnotations;
@class LayerPathAnnotation;

@implementation LayerAnnotations

- (void) drawInContext:(CGContextRef) context {
    //Overridden
}

@end
@implementation LayerPathAnnotation
+ (id) pathAnnotationWithPath:(CGPathRef)path color:(CGColorRef)color fill:(BOOL)fill{
    return [LayerPathAnnotation pathAnnotationWithPath:path color:color lineWidth:3.0 fill:fill];
}

+ (id) pathAnnotationWithPath:(CGPathRef)path color:(CGColorRef)color lineWidth:(CGFloat)width fill:(BOOL)fill {
    LayerPathAnnotation *pa = [[LayerPathAnnotation alloc] init];
    pa.path = path;
    pa.color = color;
    pa.lineWidth = width;
    pa.fill = fill;
    return pa;
}

- (void) drawInContext:(CGContextRef)context {
    CGContextAddPath(context, self.path);
    CGContextSetLineWidth(context, self.lineWidth);
    if (self.fill) {
        CGContextSetFillColorWithColor(context, self.color);
        CGContextFillPath(context);
    } else {
        CGContextSetStrokeColorWithColor(context, self.color);
        CGContextStrokePath(context);
    }
}
@end