//
//  LayerData.h
//  Layerz
//
//  Created by Raghavender  on 15/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LayerData : NSObject

@property (strong, nonatomic) NSData *pageImageData;
@property (strong, nonatomic) NSData *imageData;
@property  NSInteger layerindex;
@property  NSInteger pageIndex;
@property (strong, nonatomic) NSString *layerName;
@property (strong, nonatomic) NSMutableArray *aTextViewData;
@end

@interface TextViewData : NSObject
@property float xValue;
@property float yValue;
@property float height;
@property float width;
@property NSInteger fontSize;
@property NSInteger textViewIndex;
@property (strong, nonatomic) NSString *fontName;
@property (strong, nonatomic) NSString *fontColor;
@property (strong, nonatomic) NSString *textViewText;
@end