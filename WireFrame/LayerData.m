//
//  LayerData.m
//  Layerz
//
//  Created by Raghavender  on 15/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "LayerData.h"

@implementation TextViewData

@synthesize xValue;
@synthesize yValue;
@synthesize height;
@synthesize width;
@synthesize textViewText;
@synthesize textViewIndex;
@synthesize fontSize;
@synthesize fontColor;
@synthesize fontName;

@end


@implementation LayerData

@synthesize layerindex;
@synthesize layerName;
@synthesize imageData;
@synthesize pageIndex;
@synthesize pageImageData;

@end
