//
//  LayerIndex.h
//  Layerz
//
//  Created by Raghavender  on 22/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LayerIndex : NSObject

@property  NSInteger layerindex;
@property  NSInteger pageIndex;
@property (strong, nonatomic) NSString *layerName;
@property (strong, nonatomic) NSMutableArray *aTextViewIndexArray;

@end
