//
//  LayerStore.h
//  Layerz
//
//  Created by Raghavender  on 05/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LayerData.h"

@interface LayerStore : NSObject

@property (strong, nonatomic) NSMutableArray *layerStore;

- (id)initWithPageCount:(int)page_count;
- (NSArray*) layerCountForPage:(int)page;
- (void) addLayerCount:(LayerData*)layerIndexObj toPage:(int)page;

@end
