//
//  LayerStore.m
//  Layerz
//
//  Created by Raghavender  on 05/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "LayerStore.h"
#import "LayerData.h"


@implementation LayerStore
{
//    NSArray *layerStore;
}
@synthesize layerStore;

- (id)initWithPageCount:(int)page_count {
   
    NSMutableArray *tempr = [NSMutableArray arrayWithCapacity:page_count];
    for (int i = 0; i < page_count; i++)
    {
        [tempr addObject:[NSMutableArray array]];
    }
     self.layerStore = [NSMutableArray arrayWithArray:tempr];
    return self;
    
}
- (void) addLayerCount:(LayerData*)layerIndexObj toPage:(int)page
{
    
    NSMutableArray *pageAnnotations = [self.layerStore objectAtIndex:page];
    [pageAnnotations addObject:layerIndexObj];
    
}
- (NSArray*) layerCountForPage:(int)page
{
    
//    if (page - 1 >= [layerStore count]) {
//        
//        return [NSArray array];
//    }
    return [layerStore objectAtIndex:(page-1)];
}
@end
