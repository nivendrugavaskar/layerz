//
//  LayersView.h
//  WireFrame
//
//  Created by Raghavender  on 19/03/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface LayersView : UIView

@property CGMutablePathRef currPath;
@property CGPoint lastPoint;
@property CGPoint currentPoint;
@property CGContextRef currentContext;
@property CGColorRef pencilColor;
@property CGFloat pencilLineWidth;
@property (strong, nonatomic)  UIImageView *imageView;
@property (strong, nonatomic)  NSData *layerImageData;
@property (strong, nonatomic)  NSMutableArray *currentPaths;

-(void) drawLinesInLayers:(CGColorRef)color withPencilWidth:(CGFloat)width;
-(void) eraseLines:(CGFloat)width;
@end
