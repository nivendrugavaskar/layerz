//
//  LayersView.m
//  WireFrame
//
//  Created by Raghavender  on 19/03/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "LayersView.h"
#import <QuartzCore/QuartzCore.h>


CGFloat const TEXT_VIEW_WIDTH = 400;
CGFloat const TEXT_VIEW_HEIGHT = 190;

@implementation LayersView{
    
    BOOL didMove;
    BOOL editing;
    
}
@synthesize imageView;
@synthesize currPath;
@synthesize currentPaths;
@synthesize layerImageData;
@synthesize lastPoint;
@synthesize currentPoint;
@synthesize currentContext;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        editing = NO;
        self.imageView = [self createImageView];
        [self addSubview:self.imageView];
        currentPaths = [NSMutableArray array];
    }
    return self;
}

- (UIImageView*) createImageView {
    UIImageView *temp = [[UIImageView alloc] initWithImage:nil];
    temp.frame = self.frame;
    return temp;
}

- (void) drawRect:(CGRect)rect
{
    [self.imageView.image drawInRect:self.bounds];
}

-(void) drawLinesInLayers:(CGColorRef)color withPencilWidth:(CGFloat)width
{
   /*
    // Draw the path
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 1.5f);
    currentContext = UIGraphicsGetCurrentContext();
    [self.imageView.image drawInRect:self.bounds];
    CGContextSetShouldAntialias(currentContext, YES);
    CGContextSetLineJoin(currentContext, kCGLineJoinRound);
    
    //Setup style
    CGContextSetLineCap(currentContext, kCGLineCapRound);
    CGContextSetLineWidth(currentContext, width);
    CGContextSetStrokeColorWithColor(currentContext, color);
    
    //Draw Paths
    for (UIBezierPath *path in currentPaths) {
        CGContextAddPath(currentContext, path.CGPath);
    }
    CGContextAddPath(currentContext, currPath);
    CGContextStrokePath(currentContext);
    //Saving
    self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setNeedsDisplay];*/
    
    // new code
    
    
    
    NSLog(@"width %f",width);
    UIGraphicsBeginImageContext(self.bounds.size);
    [self.imageView.image drawInRect:self.bounds];
    
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetStrokeColorWithColor(UIGraphicsGetCurrentContext(), color);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), width);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), self.lastPoint.x, self.lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), self.currentPoint.x, self.currentPoint.y);
    
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.lastPoint = self.currentPoint;
    [self setNeedsDisplay];
    
    
}
-(void) eraseLines:(CGFloat)width
{
    // new code
    
    NSLog(@"%f",width);
    if(width==0.0)
    {
        width=1.0;
    }
    
    
    //--------Erase the Annotations lines on layer------------//
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 1.5f);
    [self.imageView.image drawInRect:self.bounds];
    currentContext = UIGraphicsGetCurrentContext();

    CGContextSetBlendMode(currentContext, kCGBlendModeClear);
    CGContextSetLineCap(currentContext, kCGLineCapRound);
    CGContextSetLineWidth(currentContext, width);
    CGContextBeginPath(currentContext);
    CGContextSetBlendMode(currentContext, kCGBlendModeClear);
    CGContextMoveToPoint(currentContext, self.lastPoint.x, self.lastPoint.y);
    CGContextAddLineToPoint(currentContext, self.currentPoint.x, self.currentPoint.y);
    CGContextStrokePath(currentContext);
    self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    lastPoint = currentPoint;
    [self setNeedsDisplay];
}

@end
