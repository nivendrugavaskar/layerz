//
//	LibraryDirectoryView.m
//	Viewer v1.0.2
//
//	Created by Julius Oklamcak on 2012-09-01.
//	Copyright © 2011-2013 Julius Oklamcak. All rights reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights to
//	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
//	of the Software, and to permit persons to whom the Software is furnished to
//	do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "ReaderConstants.h"
#import "LibraryDirectoryView.h"
#import "CoreDataManager.h"
#import "DocumentFolder.h"
#import "UIXToolbarView.h"

#import <QuartzCore/QuartzCore.h>

@interface LibraryDirectoryView () <UIPopoverControllerDelegate>
@end

@implementation LibraryDirectoryView
{

	UIPopoverController *popoverController;

	NSArray *directories;

	UIXToolbarView *theToolbar;

	ReaderThumbsView *theThumbsView;



	BOOL editMode;
}

#pragma mark Constants

#define TOOLBAR_HEIGHT 64.0f

#define THUMB_WIDTH_LARGE_DEVICE 192
#define THUMB_HEIGHT_LARGE_DEVICE 120

#define THUMB_WIDTH_SMALL_DEVICE 160
#define THUMB_HEIGHT_SMALL_DEVICE 104

#pragma mark Properties

@synthesize delegate;
@synthesize ownViewController;

#pragma mark Support methods


#pragma mark LibraryDirectoryView instance methods

- (id)initWithFrame:(CGRect)frame
{
	if ((self = [super initWithFrame:frame]))
	{
		self.autoresizesSubviews = YES;
		self.userInteractionEnabled = YES;
		self.contentMode = UIViewContentModeRedraw;
		self.autoresizingMask = UIViewAutoresizingNone;
		self.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
        
        
		CGRect viewRect = self.bounds; // View's bounds

		CGRect toolbarRect = viewRect; toolbarRect.size.height = TOOLBAR_HEIGHT;

		theToolbar = [[UIXToolbarView alloc] initWithFrame:toolbarRect]; // At top
        BOOL large = ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad);

		[self addSubview:theToolbar]; // Add toolbar to container view

		CGRect thumbsRect = viewRect; UIEdgeInsets insets = UIEdgeInsetsZero;

		if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
		{
			thumbsRect.origin.y += TOOLBAR_HEIGHT; thumbsRect.size.height -= TOOLBAR_HEIGHT;
		}
		else // Set UIScrollView insets for non-UIUserInterfaceIdiomPad case
		{
			insets.top = TOOLBAR_HEIGHT;
		}

		theThumbsView = [[ReaderThumbsView alloc] initWithFrame:thumbsRect]; // Rest of view

		theThumbsView.contentInset = insets; theThumbsView.scrollIndicatorInsets = insets;

		//theThumbsView.delegate = self; // Set the ReaderThumbsView delegate to self

		[self insertSubview:theThumbsView belowSubview:theToolbar]; // Add to container view

		NSInteger thumbWidth = (large ? THUMB_WIDTH_LARGE_DEVICE : THUMB_WIDTH_SMALL_DEVICE); // Width

		NSInteger thumbHeight = (large ? THUMB_HEIGHT_LARGE_DEVICE : THUMB_HEIGHT_SMALL_DEVICE); // Height

		[theThumbsView setThumbSize:CGSizeMake(thumbWidth, thumbHeight)]; // Thumb size based on device

		
	}

	return self;
}

- (void)reloadDirectory
{
	directories = nil; // Release any old directory list

	NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];

    
	directories = [DocumentFolder allInMOC:mainMOC]; // Get current directory list // fetching data from core data

	[theThumbsView reloadThumbsContentOffset:CGPointZero];
}
- (void)handleMemoryWarning
{
	// TBD
}

@end
