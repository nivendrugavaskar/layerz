//
//  MarkerPopOverView.h
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorView.h"

@protocol MarkerColorViewDelegate

@required
- (void)markerColorViewChangeColor:(CGColorRef)_color;

@end

@interface MarkerPopOverView : UIView <ColorViewDelegate>

@property (strong, nonatomic) UILabel  *markerWidthLabel;
@property (strong, nonatomic) UILabel  *markerColorLabel;
@property (strong, nonatomic) UISlider *aMarkerSlider;
@property (strong, nonatomic) id<MarkerColorViewDelegate> MarkerColorViewDelegate;

@end
