//
//  MarkerPopOverView.m
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "MarkerPopOverView.h"
#import "ColorView.h"

@implementation MarkerPopOverView
@synthesize aMarkerSlider;
@synthesize markerColorLabel;
@synthesize markerWidthLabel;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        self.markerWidthLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 120, 30)];
        self.markerWidthLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.markerWidthLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.markerWidthLabel.text = @"Width";
        [self addSubview:self.markerWidthLabel];
        
        
        self.markerColorLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 80, 300, 30)];
        self.markerColorLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.markerColorLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.markerColorLabel.text = @"Marker Color";
        [self addSubview:self.markerColorLabel];
        
        
        self.aMarkerSlider = [[UISlider alloc] initWithFrame:CGRectMake(110, 20, 170, 15)];
        [self.aMarkerSlider setThumbImage:[UIImage imageNamed:@"slider_gripper"] forState:UIControlStateNormal];
        [self.aMarkerSlider setMinimumTrackImage:[UIImage imageNamed:@"slider_select"] forState:UIControlStateNormal];
        [self.aMarkerSlider setMaximumTrackImage:[UIImage imageNamed:@"slider_base"] forState:UIControlStateNormal];
        
        self.aMarkerSlider.minimumValue = 0.0;
        self.aMarkerSlider.maximumValue = 30.0;
        self.aMarkerSlider.continuous = YES;
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        self.aMarkerSlider.value  = [[def valueForKey:@"MarkerValue"] floatValue];
        [self addSubview:self.aMarkerSlider];
        
        ColorView *colorView = [[ColorView alloc] initWithFrame:CGRectMake(140, 80, 170, 30)];
        colorView.ColorViewDelegate = self;
        [self addSubview:colorView];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.274 green:0.274 blue:0.274 alpha:1.0].CGColor);
    CGContextMoveToPoint(context, 0, 65);
    CGContextAddLineToPoint(context, 360, 65);
    CGContextStrokePath(context);
}
-(void) ColorViewChangeColor:(CGColorRef)_color{
    
    [self.MarkerColorViewDelegate markerColorViewChangeColor:_color];
}

@end
