//
//  PdfDoc.h
//  WireFrame
//
//  Created by Raghavender  on 09/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PdfPage;

@interface PdfDoc : NSManagedObject

@property (nonatomic, retain) NSNumber * numberOfPages;
@property (nonatomic, retain) NSString * pdfName;
@property (nonatomic, retain) NSString * pdfPath;
@property (nonatomic, retain) NSNumber * isPdfCreated;
@property (nonatomic, retain) NSSet *page;
@end

@interface PdfDoc (CoreDataGeneratedAccessors)

- (void)addPageObject:(PdfPage *)value;
- (void)removePageObject:(PdfPage *)value;
- (void)addPage:(NSSet *)values;
- (void)removePage:(NSSet *)values;

@end


