//
//  PdfDoc.m
//  WireFrame
//
//  Created by Raghavender  on 09/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "PdfDoc.h"
#import "PdfPage.h"


@implementation PdfDoc

@dynamic numberOfPages;
@dynamic pdfName;
@dynamic pdfPath;
@dynamic isPdfCreated;
@dynamic page;


@end
