//
//  PdfLayers.h
//  Layerz
//
//  Created by Raghavender  on 02/06/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PdfPage, TextView;

@interface PdfLayers : NSManagedObject

@property (nonatomic, retain) NSData * layerAnnotationImage;
@property (nonatomic, retain) NSString * layerForPage;
@property (nonatomic, retain) NSNumber * layerindex;
@property (nonatomic, retain) NSString * layerName;
@property (nonatomic, retain) NSNumber * noOfTextViews;
@property (nonatomic, retain) NSNumber * pageIndex;
@property (nonatomic, retain) PdfPage *pageno;
@property (nonatomic, retain) NSSet *textView;
@end

@interface PdfLayers (CoreDataGeneratedAccessors)

- (void)addTextViewObject:(TextView *)value;
- (void)removeTextViewObject:(TextView *)value;
- (void)addTextView:(NSSet *)values;
- (void)removeTextView:(NSSet *)values;

@end
