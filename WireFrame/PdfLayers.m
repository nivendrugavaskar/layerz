//
//  PdfLayers.m
//  Layerz
//
//  Created by Raghavender  on 02/06/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "PdfLayers.h"
#import "PdfPage.h"
#import "TextView.h"


@implementation PdfLayers

@dynamic layerAnnotationImage;
@dynamic layerForPage;
@dynamic layerindex;
@dynamic layerName;
@dynamic noOfTextViews;
@dynamic pageIndex;
@dynamic pageno;
@dynamic textView;

@end
