//
//  PdfPage.h
//  Layerz
//
//  Created by Raghavender  on 26/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PdfDoc, PdfLayers;

@interface PdfPage : NSManagedObject

@property (nonatomic, retain) NSData   * pageAnnoImage;
@property (nonatomic, retain) NSNumber * pageNumber;
@property (nonatomic, retain) NSNumber * pageNumberOfLayers;
@property (nonatomic, retain) NSString * pageName;
@property (nonatomic, retain) NSSet    * layers;
@property (nonatomic, retain) PdfDoc   * nameOfPdf;
@end

@interface PdfPage (CoreDataGeneratedAccessors)

- (void)addLayersObject:(PdfLayers *)value;
- (void)removeLayersObject:(PdfLayers *)value;
- (void)addLayers:(NSSet *)values;
- (void)removeLayers:(NSSet *)values;

@end
