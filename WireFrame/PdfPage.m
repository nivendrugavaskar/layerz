//
//  PdfPage.m
//  Layerz
//
//  Created by Raghavender  on 26/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "PdfPage.h"
#import "PdfDoc.h"
#import "PdfLayers.h"


@implementation PdfPage

@dynamic pageAnnoImage;
@dynamic pageNumber;
@dynamic pageNumberOfLayers;
@dynamic pageName;
@dynamic layers;
@dynamic nameOfPdf;

@end
