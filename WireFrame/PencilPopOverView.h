//
//  PencilPopOverView.h
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorView.h"

@protocol PencilColorViewDelegate

@required
- (void)pencilColorViewChangeColor:(CGColorRef)_color;

@end

@interface PencilPopOverView : UIView <ColorViewDelegate>

@property (strong, nonatomic) UILabel  *pencilWidthLabel;
@property (strong, nonatomic) UILabel  *pencilColor;
@property (strong, nonatomic) UISlider *aPencilSlider;

@property (strong, nonatomic) id<PencilColorViewDelegate> PencilColorViewDelegate;
@end
