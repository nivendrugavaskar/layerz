//
//  PencilPopOverView.m
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "PencilPopOverView.h"
#import "ColorView.h"

@implementation PencilPopOverView
@synthesize pencilWidthLabel;
@synthesize pencilColor;
@synthesize aPencilSlider;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.pencilWidthLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 120, 30)];
        self.pencilWidthLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.pencilWidthLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.pencilWidthLabel.text = @"Width";
        [self addSubview:self.pencilWidthLabel];
        
        
        self.pencilColor = [[UILabel alloc] initWithFrame:CGRectMake(10, 80, 300, 30)];
        self.pencilColor.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.pencilColor.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.pencilColor.text = @"Pencil Color";
        [self addSubview:self.pencilColor];
        
        
        self.aPencilSlider = [[UISlider alloc] initWithFrame:CGRectMake(110, 20, 170, 15)];
        [self.aPencilSlider setThumbImage:[UIImage imageNamed:@"slider_gripper"] forState:UIControlStateNormal];
        [self.aPencilSlider setMinimumTrackImage:[UIImage imageNamed:@"slider_select"] forState:UIControlStateNormal];
        [self.aPencilSlider setMaximumTrackImage:[UIImage imageNamed:@"slider_base"] forState:UIControlStateNormal];
        
        self.aPencilSlider.minimumValue = 1.0;
        self.aPencilSlider.maximumValue = 5.0;
        self.aPencilSlider.continuous = YES;
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        self.aPencilSlider.value = [[def valueForKey:@"PencileValue"] floatValue];
        [self addSubview:self.aPencilSlider];
        ColorView *colorView = [[ColorView alloc] initWithFrame:CGRectMake(130, 80, 170, 30)];
        colorView.ColorViewDelegate = self;
        [self addSubview:colorView];
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
   
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.274 green:0.274 blue:0.274 alpha:1.0].CGColor);
    CGContextMoveToPoint(context, 0, 65);
    CGContextAddLineToPoint(context, 360, 65);
    CGContextStrokePath(context);

}
-(void)ColorViewChangeColor:(CGColorRef)_color{
    
    [self.PencilColorViewDelegate pencilColorViewChangeColor:_color];
}

@end
