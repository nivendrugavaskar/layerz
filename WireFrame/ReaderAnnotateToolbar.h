//
//	ReaderAnnotateToolbar.h
//	ThatPDF v0.3.1
//
//	Created by Brett van Zuiden.
//	Copyright © 2013 Ink. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIXToolbarView.h"


@class ReaderAnnotateToolbar;
@class ReaderDocument;




@protocol ReaderAnnotateToolbarDelegate <NSObject>

@required // Delegate protocols

- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar shareButton:(UIButton *)button;
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar backButton:(UIButton *)button;
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar pencilButton:(UIButton *)button;
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar redPenButton:(UIButton *)button;
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar layerButton:(UIButton *)button;
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar aTextButton:(UIButton *)button;
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar eraserButton:(UIButton *)button;
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar handButton:(UIButton *)button;
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar doneButton:(UIButton *)button;

// new method
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar sendbutton:(UIButton *)button;
// new protocol



@end

@interface ReaderAnnotateToolbar : UIXToolbarView
{
@public
    UIButton *pencilButton;
    UIButton *markerButton;
    UIButton *textButton;
    UIButton *atextButton;
    UIButton *layersButton;
    UIButton *eraserButton;
    UIButton *handButton;
    UIButton *backButton;
    UIButton *doneButton;
    UIImage *buttonH;
    UIImage *buttonN;
    
    
    // new code
    UILabel *currentlayername;
}

@property (nonatomic, unsafe_unretained, readwrite) id <ReaderAnnotateToolbarDelegate> delegate;

- (id)initWithFrame:(CGRect)frame;
// new one
- (void)makeHandiconchange;

- (void)makedoneiconchange;


- (void)makeHandActive;
- (void)hideToolbar;
- (void)showToolbar;
@end
