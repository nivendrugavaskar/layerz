//
//  ReaderAnnotateToolbar.m
//	ThatPDF v0.3.1
//
//	Created by Brett van Zuiden.
//	Copyright © 2013 Ink. All rights reserved.
//

#import "ReaderAnnotateToolbar.h"
//#import <INK/Ink.h>

#pragma mark Constants

#define BUTTON_X 8.0f
#define BUTTON_Y 20.0f
#define BUTTON_SPACE 8.0f
#define BUTTON_HEIGHT 30.0f

#define DONE_BUTTON_WIDTH 44.0f
#define CANCEL_BUTTON_WIDTH 44.0f
#define RED_PEN_BUTTON_WIDTH 44.0f
#define SIGN_BUTTON_WIDTH 44.0f
#define TEXT_BUTTON_WIDTH 44.0f
#define ERASE_BUTTON_WIDTH 44.0f
#define HAND_BUTTON_WIDTH 44.0f

#define UNDO_BUTTON_WIDTH 44.0f

#define TITLE_HEIGHT 28.0f

@implementation ReaderAnnotateToolbar
{
    
   
    

}

#pragma mark Properties

@synthesize delegate;

#pragma mark ReaderAnnotateToolbar instance methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
		CGFloat viewWidth = self.bounds.size.width;

		CGFloat titleX = BUTTON_X; CGFloat titleWidth = (viewWidth - (titleX + titleX));
		CGFloat leftButtonX = BUTTON_X; // Left button start X position
		
        backButton = [UIButton buttonWithType:UIButtonTypeCustom];
		backButton.frame = CGRectMake(leftButtonX, BUTTON_Y, CANCEL_BUTTON_WIDTH, BUTTON_HEIGHT);
        [backButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
		[backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
		backButton.autoresizingMask = UIViewAutoresizingNone;
        backButton.exclusiveTouch = YES;

        
		[self addSubview:backButton];
        leftButtonX += (CANCEL_BUTTON_WIDTH + BUTTON_SPACE);
        
		titleX += (CANCEL_BUTTON_WIDTH + BUTTON_SPACE); titleWidth -= (CANCEL_BUTTON_WIDTH + BUTTON_SPACE);
        
        titleX += BUTTON_SPACE * 2;
        leftButtonX += BUTTON_SPACE * 2;
        
        atextButton = [UIButton buttonWithType:UIButtonTypeCustom];
		atextButton.frame = CGRectMake(leftButtonX, BUTTON_Y, UNDO_BUTTON_WIDTH, BUTTON_HEIGHT);
        [atextButton setImage:[UIImage imageNamed:@"type"] forState:UIControlStateNormal];
		[atextButton addTarget:self action:@selector(aTextButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
		atextButton.autoresizingMask = UIViewAutoresizingNone;
        atextButton.exclusiveTouch = YES;

		
        [self addSubview:atextButton]; leftButtonX += (UNDO_BUTTON_WIDTH + BUTTON_SPACE);
        titleX += (DONE_BUTTON_WIDTH + BUTTON_SPACE); titleWidth -= (DONE_BUTTON_WIDTH + BUTTON_SPACE);
        
        titleX += BUTTON_SPACE * 2;
        leftButtonX += BUTTON_SPACE * 2;
        
        pencilButton = [UIButton buttonWithType:UIButtonTypeCustom];
        pencilButton.frame = CGRectMake(leftButtonX, BUTTON_Y, UNDO_BUTTON_WIDTH, BUTTON_HEIGHT);
        [pencilButton setImage:[UIImage imageNamed:@"pencil"] forState:UIControlStateNormal];
        [pencilButton addTarget:self action:@selector(pencilButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        pencilButton.autoresizingMask = UIViewAutoresizingNone;
        pencilButton.exclusiveTouch = YES;

        
        [self addSubview:pencilButton];
        leftButtonX += (SIGN_BUTTON_WIDTH + BUTTON_SPACE);
        titleX += (SIGN_BUTTON_WIDTH + BUTTON_SPACE);
        titleWidth -= (SIGN_BUTTON_WIDTH + BUTTON_SPACE);
        
        titleX += BUTTON_SPACE * 2;
        leftButtonX += BUTTON_SPACE * 2;
        
        markerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        markerButton.frame = CGRectMake(leftButtonX, BUTTON_Y, RED_PEN_BUTTON_WIDTH, BUTTON_HEIGHT);
        [markerButton setImage:[UIImage imageNamed:@"marker"] forState:UIControlStateNormal];
        [markerButton addTarget:self action:@selector(redPenButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        markerButton.autoresizingMask = UIViewAutoresizingNone;
        markerButton.exclusiveTouch = YES;


        [self addSubview:markerButton];
        leftButtonX += (RED_PEN_BUTTON_WIDTH + BUTTON_SPACE);
        titleX += (RED_PEN_BUTTON_WIDTH + BUTTON_SPACE);
        titleWidth -= (RED_PEN_BUTTON_WIDTH + BUTTON_SPACE);
        
        titleX += BUTTON_SPACE * 2;
        leftButtonX += BUTTON_SPACE * 2;
        
        eraserButton = [UIButton buttonWithType:UIButtonTypeCustom];
        eraserButton.frame = CGRectMake(leftButtonX, BUTTON_Y, ERASE_BUTTON_WIDTH, BUTTON_HEIGHT);
        [eraserButton setImage:[UIImage imageNamed:@"eraser"] forState:UIControlStateNormal];
        [eraserButton addTarget:self action:@selector(eraserButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        eraserButton.autoresizingMask = UIViewAutoresizingNone;
        eraserButton.exclusiveTouch = YES;

        
        [self addSubview:eraserButton];
        leftButtonX += (ERASE_BUTTON_WIDTH + BUTTON_SPACE);
        titleX += (ERASE_BUTTON_WIDTH + BUTTON_SPACE);
        titleWidth -= (ERASE_BUTTON_WIDTH + BUTTON_SPACE);
        
        titleX += BUTTON_SPACE * 2;
        leftButtonX += BUTTON_SPACE * 2;
        
        handButton = [UIButton buttonWithType:UIButtonTypeCustom];
        handButton.frame = CGRectMake(leftButtonX, BUTTON_Y, HAND_BUTTON_WIDTH, BUTTON_HEIGHT);
        [handButton setImage:[UIImage imageNamed:@"hand.png"] forState:UIControlStateNormal];
        [handButton addTarget:self action:@selector(handButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        handButton.autoresizingMask = UIViewAutoresizingNone;
        handButton.exclusiveTouch = YES;
        [self addSubview:handButton];
        leftButtonX += (HAND_BUTTON_WIDTH + BUTTON_SPACE);
        titleX += (HAND_BUTTON_WIDTH + BUTTON_SPACE);
        titleWidth -= (HAND_BUTTON_WIDTH + BUTTON_SPACE);
        
        titleX += BUTTON_SPACE * 2;
        leftButtonX += BUTTON_SPACE * 2;
        
        doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        doneButton.frame = CGRectMake(leftButtonX, BUTTON_Y, HAND_BUTTON_WIDTH, BUTTON_HEIGHT);
       // [doneButton setTitle:@"Done" forState:UIControlStateNormal];
        [doneButton setImage:[UIImage imageNamed:@"done"] forState:UIControlStateNormal];
        [doneButton setImage:[UIImage imageNamed:@"done_hi"] forState:UIControlStateHighlighted];
        [doneButton addTarget:self action:@selector(doneButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        doneButton.autoresizingMask = UIViewAutoresizingNone;
        doneButton.exclusiveTouch = YES;
        [self addSubview:doneButton];
        // will open if below not
        //leftButtonX += (HAND_BUTTON_WIDTH + BUTTON_SPACE);
        
//        
//         // new code
//        leftButtonX += (DONE_BUTTON_WIDTH + BUTTON_SPACE);
//        titleX += (DONE_BUTTON_WIDTH + BUTTON_SPACE);
//        titleWidth -= (DONE_BUTTON_WIDTH + BUTTON_SPACE);
//        titleX += BUTTON_SPACE * 2;
//        leftButtonX += BUTTON_SPACE * 2;
        
//        CGRect titleRect = CGRectMake(leftButtonX, BUTTON_Y, 100, TITLE_HEIGHT);
//    
//        currentlayername = [[UILabel alloc] initWithFrame:titleRect];
//        currentlayername.textAlignment = NSTextAlignmentRight;
//        currentlayername.font = [UIFont systemFontOfSize:19.0f];
//        currentlayername.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//        currentlayername.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
//        currentlayername.textColor = [UIColor colorWithWhite:0.0f alpha:1.0f];
//        currentlayername.shadowColor = [UIColor colorWithWhite:0.65f alpha:1.0f];
//        currentlayername.backgroundColor = [UIColor clearColor];
//        currentlayername.shadowOffset = CGSizeMake(0.0f, 1.0f);
//        currentlayername.adjustsFontSizeToFitWidth = YES;
//        currentlayername.minimumScaleFactor = 14.0f/19.f;
//        currentlayername.text = @"Layer Name";
//         currentlayername.hidden=TRUE;
//        [self addSubview:currentlayername];
        
        
        leftButtonX += (100 + BUTTON_SPACE);
        
        
        
        
        // end code on 20/01/2015
        
        //right side
        CGFloat rightButtonX = viewWidth; // Right button start X position
        rightButtonX -= (DONE_BUTTON_WIDTH + BUTTON_SPACE);
        
        UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        shareButton.frame = CGRectMake(rightButtonX, BUTTON_Y, DONE_BUTTON_WIDTH, BUTTON_HEIGHT);
		[shareButton addTarget:self action:@selector(shareButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [shareButton setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
        shareButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        shareButton.exclusiveTouch = YES;
        [self addSubview:shareButton];
        titleWidth -= (DONE_BUTTON_WIDTH + BUTTON_SPACE);
        
        
        rightButtonX -= (SIGN_BUTTON_WIDTH + BUTTON_SPACE);
        layersButton = [UIButton buttonWithType:UIButtonTypeCustom];
        layersButton.frame = CGRectMake(rightButtonX-50, BUTTON_Y, TEXT_BUTTON_WIDTH, BUTTON_HEIGHT);
        [layersButton setImage:[UIImage imageNamed:@"layer"] forState:UIControlStateNormal];
        [layersButton addTarget:self action:@selector(layerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        layersButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        layersButton.exclusiveTouch = YES;
        [self addSubview:layersButton];
        
        // new code
        rightButtonX -= (SIGN_BUTTON_WIDTH + BUTTON_SPACE);
        CGRect titleRect = CGRectMake(rightButtonX-110, BUTTON_Y, 100, BUTTON_HEIGHT);
        
        currentlayername = [[UILabel alloc] initWithFrame:titleRect];
        currentlayername.textAlignment = NSTextAlignmentRight;
        currentlayername.font = [UIFont systemFontOfSize:19.0f];
        currentlayername.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        currentlayername.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        currentlayername.textColor = [UIColor colorWithWhite:0.0f alpha:1.0f];
        currentlayername.shadowColor = [UIColor colorWithWhite:0.65f alpha:1.0f];
        currentlayername.backgroundColor = [UIColor clearColor];
        currentlayername.shadowOffset = CGSizeMake(0.0f, 1.0f);
        currentlayername.adjustsFontSizeToFitWidth = YES;
        currentlayername.minimumScaleFactor = 14.0f/19.f;
        currentlayername.text = @"Layer Name";
        currentlayername.hidden=TRUE;
        [self addSubview:currentlayername];
        
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
		{
			CGRect titleRect = CGRectMake(titleX, BUTTON_Y, titleWidth, TITLE_HEIGHT);
            
			UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleRect];
            
			titleLabel.textAlignment = NSTextAlignmentCenter;
			titleLabel.font = [UIFont systemFontOfSize:19.0f];
			titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
			titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
			titleLabel.textColor = [UIColor colorWithWhite:0.0f alpha:1.0f];
			titleLabel.shadowColor = [UIColor colorWithWhite:0.65f alpha:1.0f];
			titleLabel.backgroundColor = [UIColor clearColor];
			titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
			titleLabel.adjustsFontSizeToFitWidth = YES;
			titleLabel.minimumScaleFactor = 14.0f/19.f;
			titleLabel.text = @"Add annotations";
            
			//[self addSubview:titleLabel];
            
           
            
            
            
		}
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

// new code
- (void)makedoneiconchange
{
    [doneButton setImage:[UIImage imageNamed:@"done_hi"] forState:UIControlStateNormal];
}
- (void)makeHandiconchange
{
    [self setNeedsDisplay];
    
    // new code
    [backButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [atextButton setImage:[UIImage imageNamed:@"type"]
                 forState:UIControlStateNormal];
    [pencilButton setImage:[UIImage imageNamed:@"pencil"] forState:UIControlStateNormal];
    [markerButton setImage:[UIImage imageNamed:@"marker"] forState:UIControlStateNormal];
    [eraserButton setImage:[UIImage imageNamed:@"eraser"] forState:UIControlStateNormal];
    [handButton setImage:[UIImage imageNamed:@"hand.png"] forState:UIControlStateNormal];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [layersButton setImage:[UIImage imageNamed:@"layer"] forState:UIControlStateNormal];
    [doneButton setImage:[UIImage imageNamed:@"done"] forState:UIControlStateNormal];
    // end
    // new code
    
    
    [delegate tappedInAnnotateToolbar:self sendbutton:handButton];
}


- (void)makeHandActive
{
    [delegate tappedInAnnotateToolbar:self handButton:handButton];
    
}

- (void)hideToolbar
{
	if (self.hidden == NO)
	{
		[UIView animateWithDuration:0.25 delay:0.0
                            options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                         animations:^(void)
         {
             self.alpha = 0.0f;
         }
                         completion:^(BOOL finished)
         {
             self.hidden = YES;
         }
         ];
	}
}

- (void)showToolbar
{
	if (self.hidden == YES)
	{        
		[UIView animateWithDuration:0.25 delay:0.0
                            options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                         animations:^(void)
         {
             self.hidden = NO;
             self.alpha = 1.0f;
         }
                         completion:NULL
         ];
	}
}


#pragma mark UIButton action methods

- (void)shareButtonTapped:(UIButton *)button
{
    
    
	[delegate tappedInAnnotateToolbar:self shareButton:button];
}
- (void)backButtonTapped:(UIButton *)button
{
    // here i want to store array data in core data for current page
    
    
    
    
	[delegate tappedInAnnotateToolbar:self backButton:button];
}
-(void)aTextButtonTapped:(UIButton *)button
{
    [delegate tappedInAnnotateToolbar:self aTextButton:button];
}
- (void)layerButtonTapped:(UIButton *)button
{
    [delegate tappedInAnnotateToolbar:self layerButton:button];

}
- (void)eraserButtonTapped:(UIButton *)button
{
    [delegate tappedInAnnotateToolbar:self eraserButton:button];
}
- (void)pencilButtonTapped:(UIButton *)button
{
	[delegate tappedInAnnotateToolbar:self pencilButton:button];
}
- (void)redPenButtonTapped:(UIButton *)button
{
	[delegate tappedInAnnotateToolbar:self redPenButton:button];
}
-(void)handButtonTapped:(UIButton*)button
{
    [delegate tappedInAnnotateToolbar:self handButton:button];
}
-(void)doneButtonTapped:(UIButton*)button
{
    [delegate tappedInAnnotateToolbar:self doneButton:button];
}

@end
