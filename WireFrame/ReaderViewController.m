//
//	ReaderViewController.m
//	Reader v2.6.0
//
//	Created by Julius Oklamcak on 2011-07-01.
//	Copyright © 2011-2013 Julius Oklamcak. All rights reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights to
//	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
//	of the Software, and to permit persons to whom the Software is furnished to
//	do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "ReaderConstants.h"
#import "ReaderViewController.h"
#import "ThumbsViewController.h"
#import "ReaderMainToolbar.h"
#import "ReaderAnnotateToolbar.h"
#import "ReaderMainPagebar.h"
#import "ReaderContentView.h"
#import "ReaderThumbCache.h"
#import "ReaderThumbQueue.h"
#import "DocumentsUpdate.h"
#import "LayersView.h"
#import "CoreDataManager.h"
#import "PdfDoc.h"
#import "PdfPage.h"
#import "PdfLayers.h"
#import "TextPopOverView.h"
#import "PencilPopOverView.h"
#import "MarkerPopOverView.h"
#import "EraserPopOverView.h"
#import "SharePopOverView.h"
#import "LayersView.h"
#import "LayerIndex.h"
#import "UIXTextEntry.h"

#import <MessageUI/MessageUI.h>

// new
#import <QuartzCore/QuartzCore.h>

#import "UIImage+StackBlur.h"
// new one



@interface ReaderViewController () <UIScrollViewDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate,
									ReaderMainToolbarDelegate, ReaderAnnotateToolbarDelegate, ReaderMainPagebarDelegate, ReaderContentViewDelegate, ThumbsViewControllerDelegate,UIPopoverControllerDelegate,UITableViewDataSource,UITableViewDelegate,LayerCountDelegate,UIAlertViewDelegate>
@end

@implementation ReaderViewController
{
	ReaderDocument *document;
	UIScrollView *theScrollView;
	ReaderMainToolbar *mainToolbar;
	ReaderMainPagebar *mainPagebar;
	NSMutableDictionary *contentViews;
	UIPrintInteractionController *printInteraction;
	NSInteger currentPage;
	CGSize lastAppearSize;
	NSDate *lastHideTime;
	BOOL isVisible;
    UIButton *editLayerButton;
    int fontSize;
    UIXTextEntry *renameAlertView;
    PencilPopOverView *pencilPopOverView;
    EraserPopOverView *eraserPopOverView;
    MarkerPopOverView *markerPopOverView;
    CGFloat previousScale;
    
    // new code
    BOOL tapimagehannd;
    BOOL layeraddcount; // to track layer is added or not
    BOOL handimageenable;
    // new trackbutton
    UIButton *tracktoolbutton;
    BOOL notdidselect,justview,deletecase;
    UITableView *tracktable;
    UIButton *trackbutton;
    // new code
    NSInteger trackindexpath;
    UIAlertView *alertx;
    
    // new boolean
    BOOL iserasepress;

}

@synthesize annotateToolbar;

#pragma mark Constants

#define PAGING_VIEWS 3
#define TOOLBAR_HEIGHT 64.0f
#define PAGEBAR_HEIGHT 48.0f
#define TAP_AREA_SIZE 48.0f

#pragma mark Properties

@synthesize delegate;
@synthesize layerCount;
@synthesize layerTableView;
@synthesize popoverController;


#pragma mark Support methods

- (void)updateScrollViewContentSize
{
	NSInteger count = [document.pageCount integerValue];

	if (count > PAGING_VIEWS)
        count = PAGING_VIEWS; // Limit

	CGFloat contentHeight = theScrollView.bounds.size.height;
	CGFloat contentWidth = (theScrollView.bounds.size.width * count);

	theScrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
}

- (void)updateScrollViewContentViews
{
	[self updateScrollViewContentSize]; // Update the content size

	NSMutableIndexSet *pageSet = [NSMutableIndexSet indexSet]; // Page set

	[contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
		^(id key, id object, BOOL *stop)
		{
			ReaderContentView *contentView = object;
            [pageSet addIndex:contentView.tag];
		}
	];

	__block CGRect viewRect = CGRectZero;
    viewRect.size = theScrollView.bounds.size;

	__block CGPoint contentOffset = CGPointZero;
    NSInteger page = [document.pageNumber integerValue];

	[pageSet enumerateIndexesUsingBlock: // Enumerate page number set
		^(NSUInteger number, BOOL *stop)
		{
			NSNumber *key = [NSNumber numberWithInteger:number]; // # key
			ReaderContentView *contentView = [contentViews objectForKey:key];

			contentView.frame = viewRect;
            
            if (page == number) {
                contentOffset = viewRect.origin;
            }

			viewRect.origin.x += viewRect.size.width; // Next view frame position
		}
	];

	if (CGPointEqualToPoint(theScrollView.contentOffset, contentOffset) == false)
	{
		theScrollView.contentOffset = contentOffset; // Update content offset
	}
}

- (void)cancelButtonTappedInTextEntry:(UIXTextEntry *)textEntry
{
    
}


- (void)showDocumentPage:(NSInteger)page
{
    // new code
     for (UIImageView *remove in self.annotationController.containerView.subviews)
     {
     if([remove isKindOfClass:[UIImageView class]])
     {
     [remove removeFromSuperview];
     }
     }
    
    // new code tyo solve specific pencil marker issue
    
    [self setAnnotationMode:AnnotationViewControllerType_None];
    NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
    [def setObject:[NSNumber numberWithInt:0.0] forKey:@"PencileValue"];
    [def synchronize];
    
    [self setAnnotationMode:AnnotationViewControllerType_None];
    NSUserDefaults *def1 =[NSUserDefaults standardUserDefaults];
    [def1 setObject:[NSNumber numberWithInt:3.0] forKey:@"MarkerValue"];
    [def1 synchronize];
    
    
    // to set default colour black
    self.annotationController.pencilColor=nil;
    self.annotationController.markerColor=nil;
    
    // end
    
    // new code
    [annotateToolbar makeHandiconchange];
    // new code
    [tracktoolbutton setImage:[UIImage imageNamed:@"hand.png"] forState:UIControlStateNormal];
    tapimagehannd=NO;
    handimageenable=NO;
    notdidselect=NO;
    
    // new code
    justview=NO;
    self.annotationController.checkmark=NO;
    
    // new code 20/01/2015
    annotateToolbar->currentlayername.hidden=TRUE;
   
    
    
    
	if (page != currentPage) // Only if different
	{
		NSInteger minValue;
        NSInteger maxValue;
		NSInteger maxPage = [document.pageCount integerValue];
		NSInteger minPage = 1;
        
		if ((page < minPage) || (page > maxPage))
        {
            return;
        }
        
		if (maxPage <= PAGING_VIEWS) // Few pages
		{
			minValue = minPage;
			maxValue = maxPage;
		}
		else // Handle more pages
		{
			int spread = 1;
            minValue = (page - spread);
			maxValue = (page + spread);
            
			if (minValue < minPage)
            {
                minValue+=spread;
                maxValue+=spread;
            }
			else
				if (maxValue > maxPage)
                {
                    minValue-=spread;
                    maxValue-=spread;
                }
		}
        
		NSMutableIndexSet *newPageSet = [NSMutableIndexSet new];
        
		NSMutableDictionary *unusedViews = [contentViews mutableCopy];
        
		CGRect viewRect = CGRectZero;
        viewRect.size = theScrollView.bounds.size;
        
        // move from 1 to 2 page
		for (NSInteger number = minValue; number <= maxValue; number++)
		{
			NSNumber *key = [NSNumber numberWithInteger:number]; // # key
            
			ReaderContentView *contentView = [contentViews objectForKey:key];
            
            /*  {
             3 = "<ReaderContentView: 0x8cd4cb0; baseClass = UIScrollView; frame = (2048 0; 1024 768); clipsToBounds = YES; autoresize = W+H; autoresizesSubviews = NO; userInteractionEnabled = NO; tag = 3; gestureRecognizers = <NSArray: 0x8cd0f60>; layer = <CALayer: 0x8cd1ac0>; contentOffset: {-4, -4}>";
             1 = "<ReaderContentView: 0x8cd0750; baseClass = UIScrollView; frame = (0 0; 1024 768); clipsToBounds = YES; autoresize = W+H; autoresizesSubviews = NO; userInteractionEnabled = NO; tag = 1; gestureRecognizers = <NSArray: 0x8cbece0>; layer = <CALayer: 0x8c9a050>; contentOffset: {-4, -4}>";
             2 = "<ReaderContentView: 0x8dc9350; baseClass = UIScrollView; frame = (1024 0; 1024 768); clipsToBounds = YES; autoresize = W+H; autoresizesSubviews = NO; userInteractionEnabled = NO; tag = 2; gestureRecognizers = <NSArray: 0x8dd2520>; layer = <CALayer: 0x8dca7a0>; contentOffset: {-4, -4}>";
             }*/
            
            
            
            
            
            // NSLog(@"%@",[contentViews objectForKey:key]);
            
			if (contentView == nil) // Create a brand new document content view
			{
				NSURL *fileURL = document.fileURL;
                NSString *phrase = document.password; // Document properties
                
				contentView = [[ReaderContentView alloc] initWithFrame:viewRect fileURL:fileURL page:number password:phrase annotations:[document annotations]];
                
				[theScrollView addSubview:contentView];
                [contentViews setObject:contentView forKey:key];
                /*
                 {
                 4 = "<ReaderContentView: 0x8dd09b0; baseClass = UIScrollView; frame = (2048 0; 1024 768); clipsToBounds = YES; autoresize = W+H; autoresizesSubviews = NO; userInteractionEnabled = NO; tag = 4; gestureRecognizers = <NSArray: 0x8ca8c10>; layer = <CALayer: 0x8ca9960>; contentOffset: {-4, -4}>";
                 3 = "<ReaderContentView: 0x8dc2060; baseClass = UIScrollView; frame = (1024 0; 1024 768); clipsToBounds = YES; autoresize = W+H; autoresizesSubviews = NO; userInteractionEnabled = NO; tag = 3; gestureRecognizers = <NSArray: 0x8dc2250>; layer = <CALayer: 0x8da96e0>; contentOffset: {-4, -4}>";
                 1 = "<ReaderContentView: 0x8c80740; baseClass = UIScrollView; frame = (0 0; 1024 768); clipsToBounds = YES; autoresize = W+H; autoresizesSubviews = NO; userInteractionEnabled = NO; tag = 1; gestureRecognizers = <NSArray: 0x8c65600>; layer = <CALayer: 0x8c84d50>; contentOffset: {-4, -4}>";
                 2 = "<ReaderContentView: 0x8dbf130; baseClass = UIScrollView; frame = (0 0; 1024 768); clipsToBounds = YES; autoresize = W+H; autoresizesSubviews = NO; userInteractionEnabled = NO; tag = 2; gestureRecognizers = <NSArray: 0x8da2bb0>; layer = <CALayer: 0x8da8bd0>; contentOffset: {-4, -4}>";
                 }
                 */
                
				contentView.message = self;
                [newPageSet addIndex:number];
			}
			else // Reposition the existing content view
			{
                [contentView zoomReset];
                contentView.transform=CGAffineTransformIdentity;
                contentView.frame = viewRect;
				[unusedViews removeObjectForKey:key];
                
			}
            
			viewRect.origin.x += viewRect.size.width;
		}
        
        // save
        // [self.annotationController saveCurrentPage:page];
        
		[unusedViews enumerateKeysAndObjectsUsingBlock: // Remove unused views
         ^(id key, id object, BOOL *stop)
         {
             [contentViews removeObjectForKey:key];
             
             ReaderContentView *contentView = object;
             
             [contentView removeFromSuperview];
         }
         ];
        
		unusedViews = nil; // Release unused views
        
		CGFloat viewWidthX1 = viewRect.size.width;
		CGFloat viewWidthX2 = (viewWidthX1 * 2.0f);
        
		CGPoint contentOffset = CGPointZero;
        
		if (maxPage >= PAGING_VIEWS)
		{
			if (page == maxPage)
				contentOffset.x = viewWidthX2;
			else
				if (page != minPage)
					contentOffset.x = viewWidthX1;
		}
		else
			if (page == (PAGING_VIEWS - 1))
				contentOffset.x = viewWidthX1;
        
		if (CGPointEqualToPoint(theScrollView.contentOffset, contentOffset) == false)
		{
			theScrollView.contentOffset = contentOffset; // Update content offset
		}
        
		if ([document.pageNumber integerValue] != page) // Only if different
		{
			document.pageNumber = [NSNumber numberWithInteger:page]; // Update page number
		}
        
		NSURL *fileURL = document.fileURL; NSString *phrase = document.password; NSString *guid = document.guid;
        
		if ([newPageSet containsIndex:page] == YES) // Preview visible page first
		{
			NSNumber *key = [NSNumber numberWithInteger:page]; // # key
            
			ReaderContentView *targetView = [contentViews objectForKey:key];
            
			[targetView showPageThumb:fileURL page:page password:phrase guid:guid];
            
			[newPageSet removeIndex:page]; // Remove visible page from set
		}
        
		[newPageSet enumerateIndexesWithOptions:NSEnumerationReverse usingBlock: // Show previews
         ^(NSUInteger number, BOOL *stop)
         {
             NSNumber *key = [NSNumber numberWithInteger:number]; // # key
             
             ReaderContentView *targetView = [contentViews objectForKey:key];
             
             [targetView showPageThumb:fileURL page:number password:phrase guid:guid];
         }
         ];
        
		newPageSet = nil; // Release new page set
        
		[mainPagebar updatePagebar]; // Update the pagebar display
        
		//[self updateToolbarBookmarkIcon]; // Update bookmark
        
		currentPage = page; // Track current page number
        
        //Updating annotations
        
        // when move to page 2 key=2
        
        // new code
        
        
        NSNumber *key = [NSNumber numberWithInteger:page]; // # key
        ReaderContentView *newContentView = [contentViews objectForKey:key];
        [self.annotationController moveToPage:page  contentView:newContentView];
        
//        // new code
//        layerIndex=[self.layerCount lastObject];
//        
//        
//        [self performSelector:@selector(createtablesdata1) withObject:self afterDelay:0.00001];
        
        
        
	}
}

- (void)showDocument:(id)object
{
	[self updateScrollViewContentSize]; // Set content size

	[self showDocumentPage:[document.pageNumber integerValue]];

	document.lastOpen = [NSDate date]; // Update last opened date

	isVisible = YES; // iOS present modal bodge
}

#pragma mark UIViewController methods

- (id)initWithReaderDocument:(ReaderDocument *)object
{
	id reader = nil; // ReaderViewController object

	if ((object != nil) && ([object isKindOfClass:[ReaderDocument class]]))
	{
		if ((self = [super initWithNibName:nil bundle:nil])) // Designated initializer
		{
			NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];

			[notificationCenter addObserver:self selector:@selector(applicationWill:) name:UIApplicationWillTerminateNotification object:nil];

			[notificationCenter addObserver:self selector:@selector(applicationWill:) name:UIApplicationWillResignActiveNotification object:nil];
            
            [notificationCenter addObserver:self selector:@selector(handleAnnotationModeNotification:) name:DocumentsSetAnnotationModeSignNotification object:nil];
            [notificationCenter addObserver:self selector:@selector(handleAnnotationModeNotification:) name:DocumentsSetAnnotationModeRedPenNotification object:nil];
            [notificationCenter addObserver:self selector:@selector(handleAnnotationModeNotification:) name:DocumentsSetAnnotationModeOffNotification object:nil];
            
			[object updateProperties];
            document = object; // Retain the supplied ReaderDocument object for our use

			[ReaderThumbCache touchThumbCacheWithGUID:object.guid]; // Touch the document thumb cache directory

			reader = self; // Return an initialized ReaderViewController object
		}
	}
    
	return reader;
}

- (void)viewDidLoad
{
	[super viewDidLoad];

    	assert(document != nil); // Must have a valid ReaderDocument

	self.view.backgroundColor = [UIColor whiteColor];
   
    // Default font size
    fontSize = 16.0;
    
	CGRect viewRect = self.view.bounds; // View controller's view bounds

	theScrollView = [[UIScrollView alloc] initWithFrame:viewRect]; // All
	theScrollView.scrollsToTop = NO;
	theScrollView.pagingEnabled = YES;
    theScrollView.multipleTouchEnabled = YES;
	theScrollView.delaysContentTouches = NO;
	theScrollView.showsVerticalScrollIndicator = NO;
	theScrollView.showsHorizontalScrollIndicator = NO;
	theScrollView.contentMode = UIViewContentModeRedraw;
	theScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	theScrollView.backgroundColor = [UIColor clearColor];
	theScrollView.userInteractionEnabled = YES;
	theScrollView.autoresizesSubviews = NO;
	theScrollView.delegate = self;
	[self.view addSubview:theScrollView];

	CGRect toolbarRect = viewRect;
	toolbarRect.size.height = TOOLBAR_HEIGHT;

	mainToolbar = [[ReaderMainToolbar alloc] initWithFrame:toolbarRect document:document]; // At top

	mainToolbar.delegate = self;

	[self.view addSubview:mainToolbar];
    
    annotateToolbar = [[ReaderAnnotateToolbar alloc] initWithFrame:toolbarRect]; // At top for annotating
    
	annotateToolbar.delegate = self;
    //hidden by default
    annotateToolbar.hidden = YES;
    annotateToolbar.userInteractionEnabled = YES;
	[self.view addSubview:annotateToolbar];

	CGRect pagebarRect = viewRect;
	pagebarRect.size.height = PAGEBAR_HEIGHT;
	pagebarRect.origin.y = (viewRect.size.height - PAGEBAR_HEIGHT);

	mainPagebar = [[ReaderMainPagebar alloc] initWithFrame:pagebarRect document:document]; // At bottom

	mainPagebar.delegate = self;
	[self.view addSubview:mainPagebar];
    

    
   // UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scaleImage:)];
   // [self.view addGestureRecognizer:pinchGesture];
    
   /* for (id gestureRecognizer in theScrollView.gestureRecognizers)
    {
        if ([gestureRecognizer  isKindOfClass:[UIPanGestureRecognizer class]])
        {
            UIPanGestureRecognizer *panGR = gestureRecognizer;
            panGR.minimumNumberOfTouches = 2;
        }
    }*/
    
    //Annotation view controller
    self.annotationController = [[AnnotationViewController alloc] initWithDocument:document];
    self.annotationController.delegate = self;
	contentViews = [NSMutableDictionary new];
    lastHideTime = [NSDate date];
    
    // make edit button
    editLayerButton = [UIButton buttonWithType:UIButtonTypeCustom];
}

- (void)scaleImage:(UIPinchGestureRecognizer *)recognizer
{
    
    NSInteger page = [document.pageNumber integerValue];
    NSNumber *key = [NSNumber numberWithInteger:page];
    ReaderContentView *targetView = [contentViews objectForKey:key];
    
    if([recognizer state] == UIGestureRecognizerStateBegan)
    {
        previousScale = [recognizer scale];
    }
    
    if ([recognizer state] == UIGestureRecognizerStateBegan ||
        [recognizer state] == UIGestureRecognizerStateChanged)
    {
        CGFloat currentScale = [[targetView.layer valueForKeyPath:@"transform.scale"] floatValue];
        
        const CGFloat kMaxScale = targetView.maximumZoomScale;
        const CGFloat kMinScale = targetView.minimumZoomScale;
        CGFloat newScale = 1.0 - (previousScale - [recognizer scale]);
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([targetView transform], newScale, newScale);
        targetView.transform = transform;
        previousScale = [recognizer scale];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    self.annotationController.markerLineWidth = 3.0;
    self.annotationController.pencilLineWidth = 1.0;
	[super viewWillAppear:animated];

    //1st time zero
	if (CGSizeEqualToSize(lastAppearSize, CGSizeZero) == false)
	{
		if (CGSizeEqualToSize(lastAppearSize, self.view.bounds.size) == false)
		{
			[self updateScrollViewContentViews]; // Update content views
		}

		lastAppearSize = CGSizeZero; // Reset view size tracking
	}
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];

	if (CGSizeEqualToSize(theScrollView.contentSize, CGSizeZero)) // First time if both equal then enter
	{
		[self performSelector:@selector(showDocument:) withObject:nil afterDelay:0.02];
	}

#if (READER_DISABLE_IDLE == TRUE) // Option

	[UIApplication sharedApplication].idleTimerDisabled = YES;

#endif // end of READER_DISABLE_IDLE Option
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];

	lastAppearSize = self.view.bounds.size; // Track view size

#if (READER_DISABLE_IDLE == TRUE) // Option

	[UIApplication sharedApplication].idleTimerDisabled = NO;

#endif // end of READER_DISABLE_IDLE Option
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void)viewDidUnload
{
#ifdef DEBUG
	NSLog(@"%s", __FUNCTION__);
#endif

	mainToolbar = nil; annotateToolbar = nil; mainPagebar = nil;

	theScrollView = nil; contentViews = nil; lastHideTime = nil;

	lastAppearSize = CGSizeZero; currentPage = 0;

	[super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	if (isVisible == NO) return; // iOS present modal bodge

	if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
	{
		if (printInteraction != nil) [printInteraction dismissAnimated:NO];
	}
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
	if (isVisible == NO) return; // iOS present modal bodge

	[self updateScrollViewContentViews]; // Update content views

	lastAppearSize = CGSizeZero; // Reset view size tracking
}

/*
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	//if (isVisible == NO) return; // iOS present modal bodge

	//if (fromInterfaceOrientation == self.interfaceOrientation) return;
}
*/

-(void)didReceiveMemoryWarning
{
#ifdef DEBUG
	NSLog(@"%s", __FUNCTION__);
#endif

	[super didReceiveMemoryWarning];
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark UIScrollViewDelegate methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	__block NSInteger page = 0;

	CGFloat contentOffsetX = scrollView.contentOffset.x;

	[contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
		^(id key, id object, BOOL *stop)
		{
			ReaderContentView *contentView = object;

			if (contentView.frame.origin.x == contentOffsetX)
			{
				page = contentView.tag; *stop = YES;
			}
		}
	];

	if (page != 0)
    {
        NSLog(@"%d",currentPage);
        if(currentPage!=page)
        {
       [self showDocumentPage:page];
        }
        // Show the page
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
	[self showDocumentPage:theScrollView.tag]; // Show page

    theScrollView.tag = 0; // Clear page number tag
}

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)recognizer shouldReceiveTouch:(UITouch *)touch
{
	if ([touch.view isKindOfClass:[UIScrollView class]]) return YES;

	return NO;
}

#pragma mark UIGestureRecognizer action methods

- (void)decrementPageNumber
{
	if (theScrollView.tag == 0) // Scroll view did end
	{
		NSInteger page = [document.pageNumber integerValue];
		NSInteger maxPage = [document.pageCount integerValue];
		NSInteger minPage = 1; // Minimum

		if ((maxPage > minPage) && (page != minPage))
		{
			CGPoint contentOffset = theScrollView.contentOffset;

			contentOffset.x -= theScrollView.bounds.size.width; // -= 1

			[theScrollView setContentOffset:contentOffset animated:YES];

			theScrollView.tag = (page - 1); // Decrement page number
		}
	}
}

- (void)incrementPageNumber
{
	if (theScrollView.tag == 0) // Scroll view did end
	{
		NSInteger page = [document.pageNumber integerValue];
		NSInteger maxPage = [document.pageCount integerValue];
		NSInteger minPage = 1; // Minimum

		if ((maxPage > minPage) && (page != maxPage))
		{
			CGPoint contentOffset = theScrollView.contentOffset;

			contentOffset.x += theScrollView.bounds.size.width; // += 1

			[theScrollView setContentOffset:contentOffset animated:YES];

			theScrollView.tag = (page + 1); // Increment page number
		}
	}
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
   	if (recognizer.state == UIGestureRecognizerStateRecognized)
	{
		CGRect viewRect = recognizer.view.bounds; // View bounds

		CGPoint point = [recognizer locationInView:recognizer.view];

		CGRect areaRect = CGRectInset(viewRect, TAP_AREA_SIZE, 0.0f); // Area

		if (CGRectContainsPoint(areaRect, point)) // Single tap is inside the area
		{
			NSInteger page = [document.pageNumber integerValue]; // Current page #

			NSNumber *key = [NSNumber numberWithInteger:page]; // Page number key

			ReaderContentView *targetView = [contentViews objectForKey:key];

			id target = [targetView processSingleTap:recognizer]; // Target

			if (target != nil) // Handle the returned target object
			{
				if ([target isKindOfClass:[NSURL class]]) // Open a URL
				{
					NSURL *url = (NSURL *)target; // Cast to a NSURL object

					if (url.scheme == nil) // Handle a missing URL scheme
					{
						NSString *www = url.absoluteString; // Get URL string

						if ([www hasPrefix:@"www"] == YES) // Check for 'www' prefix
						{
							NSString *http = [NSString stringWithFormat:@"http://%@", www];

							url = [NSURL URLWithString:http]; // Proper http-based URL
						}
					}

					if ([[UIApplication sharedApplication] openURL:url] == NO)
					{
						#ifdef DEBUG
							NSLog(@"%s '%@'", __FUNCTION__, url); // Bad or unknown URL
						#endif
					}
				}
				else // Not a URL, so check for other possible object type
				{
					if ([target isKindOfClass:[NSNumber class]]) // Goto page
					{
						NSInteger value = [target integerValue]; // Number

						[self showDocumentPage:value]; // Show the page
					}
            }
			}
			else // Nothing active tapped in the target content view
			{
				if ([lastHideTime timeIntervalSinceNow] < -0.75) // Delay since hide
				{
					if ((mainToolbar.hidden == YES) || (mainPagebar.hidden == YES))
					{
						[mainToolbar showToolbar];
                        [mainPagebar showPagebar]; // Show
					}
				}
			}

			return;
		}

		CGRect nextPageRect = viewRect;
		nextPageRect.size.width = TAP_AREA_SIZE;
		nextPageRect.origin.x = (viewRect.size.width - TAP_AREA_SIZE);
		if (CGRectContainsPoint(nextPageRect, point)) // page++ area
		{
			[self incrementPageNumber]; return;
		}
		CGRect prevPageRect = viewRect;
		prevPageRect.size.width = TAP_AREA_SIZE;
		if (CGRectContainsPoint(prevPageRect, point)) // page-- area
		{
			[self decrementPageNumber]; return;
		}
	}
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    
	if (recognizer.state == UIGestureRecognizerStateRecognized)
	{
		CGRect viewRect = recognizer.view.bounds; // View bounds

		CGPoint point = [recognizer locationInView:recognizer.view];

		CGRect zoomArea = CGRectInset(viewRect, TAP_AREA_SIZE, TAP_AREA_SIZE);

		if (CGRectContainsPoint(zoomArea, point)) // Double tap is in the zoom area
		{
			NSInteger page = [document.pageNumber integerValue]; // Current page #

			NSNumber *key = [NSNumber numberWithInteger:page]; // Page number key

			ReaderContentView *targetView = [contentViews objectForKey:key];

			switch (recognizer.numberOfTouchesRequired) // Touches count
			{
				case 1: // One finger double tap: zoom ++
				{
                    
					[targetView zoomIncrement]; break;
                    
				}

				case 2: // Two finger double tap: zoom --
				{
					[targetView zoomDecrement]; break;
				}
			}

			return;
		}

		CGRect nextPageRect = viewRect;
		nextPageRect.size.width = TAP_AREA_SIZE;
		nextPageRect.origin.x = (viewRect.size.width - TAP_AREA_SIZE);

		if (CGRectContainsPoint(nextPageRect, point)) // page++ area
		{
			[self incrementPageNumber]; return;
		}

		CGRect prevPageRect = viewRect;
		prevPageRect.size.width = TAP_AREA_SIZE;

		if (CGRectContainsPoint(prevPageRect, point)) // page-- area
		{
			[self decrementPageNumber]; return;
		}
	}
}

#pragma mark ReaderContentViewDelegate methods

- (void)contentView:(ReaderContentView *)contentView touchesBegan:(NSSet *)touches
{
	if ((mainToolbar.hidden == NO) || (mainPagebar.hidden == NO))
	{
        // new code
        if(tapimagehannd==NO)
        {
        
		if (touches.count == 1) // Single touches only
		{
            
            UITouch *touch = [touches anyObject]; // Touch info
			CGPoint point = [touch locationInView:self.view]; // Touch location

			CGRect areaRect = CGRectInset(self.view.bounds, TAP_AREA_SIZE, TAP_AREA_SIZE);

			if (CGRectContainsPoint(areaRect, point) == true) return;
            
        }else if(touches.count == 2)
        {
           
        }
		//[mainToolbar hideToolbar]; [mainPagebar hidePagebar]; // Hide
		lastHideTime = [NSDate date];
	}
    }
}

#pragma mark ReaderMainToolbarDelegate methods

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar libraryButton:(UIButton *)button
{
#if (READER_STANDALONE == FALSE) // Option

    dispatch_queue_t defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(defaultQueue, ^{
        [document saveReaderDocumentWithAnnotations]; // Save any ReaderDocument object changes
        document.password = nil; // Clear out any document password
        
    });
    
	[[ReaderThumbQueue sharedInstance] cancelOperationsWithGUID:document.guid];

	//[[ReaderThumbCache sharedInstance] removeAllObjects]; // Empty the thumb cache

    if ([delegate respondsToSelector:@selector(dismissReaderViewController:)] == YES)
    {
        [delegate dismissReaderViewController:self]; // Dismiss the ReaderViewController
    }
    else // We have a "Delegate must respond to -dismissReaderViewController: error"
    {
        NSAssert(NO, @"Delegate must respond to -dismissReaderViewController:");
    }
    

#endif // end of READER_STANDALONE Option
}
- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar annotateButton:(UIButton *)button
{
    
    [self setAnnotationMode:AnnotationViewControllerType_None];
    [self startAnnotation];
 
    
    
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar markButton:(UIButton *)button
{
	if (printInteraction != nil) [printInteraction dismissAnimated:YES];

	NSInteger page = [document.pageNumber integerValue];

	if ([document.bookmarks containsIndex:page]) // Remove bookmark
	{
		[mainToolbar setBookmarkState:NO]; [document.bookmarks removeIndex:page];
	}
	else // Add the bookmarked page index to the bookmarks set
	{
		[mainToolbar setBookmarkState:YES]; [document.bookmarks addIndex:page];
	}
}

#pragma mark ReaderAnnotateToolbarDelegate methods

- (void) setAnnotationMode:(NSString*)mode
{
    self.annotationController.annotationType = mode;
}

- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar pencilButton:(UIButton *)button
{
    
    // new code
    [annotateToolbar makeHandiconchange];
    // new code
    [annotateToolbar makedoneiconchange];
    
    NSLog(@"%@",layerIndex.layerName);
    NSLog(@"%d",[layerCount count]);
    
    
   [self layercountcheck]; // to check layer added or not
   
    // new code
    if(!layerIndex.layerName)
    {
        [self alertshow1];
        return;
    }
    // end
    
    if(layeraddcount==YES)
    {
     // new code
    [button setImage:[UIImage imageNamed:@"pencil_hi"] forState:UIControlStateNormal];
        
        
    
    [self setAnnotationMode:AnnotationViewControllerType_Sign];
    UIViewController* popoverContent = [[UIViewController alloc]init];
    pencilPopOverView = [[PencilPopOverView alloc] initWithFrame:CGRectMake(0, 0, 300, 130)];
    pencilPopOverView.PencilColorViewDelegate = self;
    [pencilPopOverView.aPencilSlider addTarget:self action:@selector(pencilSliderAction:) forControlEvents:UIControlEventValueChanged];
    pencilPopOverView.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
    
    popoverContent.view = pencilPopOverView;
    popoverContent.contentSizeForViewInPopover = CGSizeMake(300, 130);
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popoverContent];
    self.popoverController.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
    [self.popoverController presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionUp animated:YES];
        
        // new code
        
        theScrollView.scrollEnabled=NO;
        NSInteger page = [document.pageNumber integerValue];
        NSNumber *key = [NSNumber numberWithInteger:page];
        ReaderContentView *targetView = [contentViews objectForKey:key];
        targetView.scrollEnabled=NO;
        // end
        
    }
    else
    {
         [self alertshow];
          return;
        //button.userInteractionEnabled=NO;
    }
    
   
   
}

- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar redPenButton:(UIButton *)button
{
    
    
    // new code
     [annotateToolbar makeHandiconchange];
    // new code
    [annotateToolbar makedoneiconchange];

    
    if(tracktoolbutton!=button)
    {
        [tracktoolbutton setImage:[UIImage imageNamed:@"hand.png"] forState:UIControlStateNormal];
        tapimagehannd=NO;
    }
    // end
    
    [self layercountcheck]; // to check layer added or not
    
    // new code
    if(!layerIndex.layerName)
    {
        [self alertshow1];
        return;
    }
    
    // end

    
    if(layeraddcount)
    {
        // new code
        [button setImage:[UIImage imageNamed:@"marker_hi"] forState:UIControlStateNormal];

    //---------Sending the mode of the button tapped -----------//
    
    [self setAnnotationMode:AnnotationViewControllerType_RedPen];
    
    UIViewController* popoverContent = [[UIViewController alloc]init];
    markerPopOverView = [[MarkerPopOverView alloc] initWithFrame:CGRectMake(0, 0, 310, 130)];
    markerPopOverView.MarkerColorViewDelegate = self;
    [markerPopOverView.aMarkerSlider addTarget:self action:@selector(markerSliderAction:) forControlEvents:UIControlEventValueChanged];
    markerPopOverView.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
    popoverContent.view = markerPopOverView;
    popoverContent.contentSizeForViewInPopover = CGSizeMake(310, 130);
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popoverContent];
    self.popoverController.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
    [self.popoverController presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionUp animated:YES];
        // new code
        
        theScrollView.scrollEnabled=NO;
        NSInteger page = [document.pageNumber integerValue];
        NSNumber *key = [NSNumber numberWithInteger:page];
        ReaderContentView *targetView = [contentViews objectForKey:key];
        targetView.scrollEnabled=NO;
        // end
        
    }
    else
    {
        [self alertshow];
        return;
    }
    

}
-(void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar eraserButton:(UIButton *)button
{
   
    // new code
     [annotateToolbar makeHandiconchange];
    // new code
    [annotateToolbar makedoneiconchange];
    
    if(tracktoolbutton!=button)
    {
        [tracktoolbutton setImage:[UIImage imageNamed:@"hand.png"] forState:UIControlStateNormal];
        tapimagehannd=NO;
    }
    // end
    
    [self layercountcheck]; // to check layer added or not
    
    // new code
    if(!layerIndex.layerName)
    {
        [self alertshow1];
        return;
    }
    
    // end
    
    if(layeraddcount)
    {
        [button setImage:[UIImage imageNamed:@"eraser_hi"] forState:UIControlStateNormal];
        //iserasepress=YES;
        
        [self setAnnotationMode:AnnotationViewControllerType_Eraser];
        UIViewController* popoverContent = [[UIViewController alloc]init];
        eraserPopOverView = [[EraserPopOverView alloc] initWithFrame:CGRectMake(0, 0, 300, 60)];
        eraserPopOverView.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
        [eraserPopOverView.aEraserSlider addTarget:self action:@selector(eraserSliderAction:) forControlEvents:UIControlEventValueChanged];
        popoverContent.view = eraserPopOverView;
        popoverContent.contentSizeForViewInPopover = CGSizeMake(300, 60);
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:popoverContent];
        self.popoverController.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
        [self.popoverController presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionUp animated:YES];
        
        // new code
        
        theScrollView.scrollEnabled=NO;
        NSInteger page = [document.pageNumber integerValue];
        NSNumber *key = [NSNumber numberWithInteger:page];
        ReaderContentView *targetView = [contentViews objectForKey:key];
        targetView.scrollEnabled=NO;
        // end
    }
    else
    {
        [self alertshow];
        return;
    }
   
}

- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar layerButton:(UIButton *)button
{
    // new code
    [annotateToolbar makeHandiconchange];
    // new code
  //  [annotateToolbar makedoneiconchange];

    
    // new code
    [self.annotationController dismisskeyboard];
    if([self.layerCount count] >0)
    {
        [self hudstart];
        NSLog(@"layername=%@", layerIndex.layerName);
        // new code
        if(handimageenable==YES)
        {
            
            if(tapimagehannd==YES)
            {
             //tapimagehannd=NO;
                
            [annotateToolbar makeHandActive];
            }
        }
        
    
    }
    else
    {
        NSLog(@"Layer is added");
    }
    
    // end
    
    // to check fetched data
   // self.layerCount=[[NSUserDefaults standardUserDefaults]valueForKey:@"layercount"];
    // new code hide
   /* [tracktoolbutton setImage:[UIImage imageNamed:@"hand.png"] forState:UIControlStateNormal];
    tapimagehannd=NO;
    [self.annotationController deactivatetextbox];*/
    // new added
    [self setAnnotationMode:AnnotationViewControllerType_None];
    // end
    
    trackbutton=[[UIButton alloc]init];
    trackbutton=button;
    
    // new code
    justview=NO;
    
    // new code
    [button setImage:[UIImage imageNamed:@"layer_hi"] forState:UIControlStateNormal];
    
    [self performSelector:@selector(createtablesdata) withObject:self afterDelay:0.00001];
    
   // [self performSelector:@selector(editing) withObject:self afterDelay:0.00001];
    
    
    
    
    
}

// to test
-(void)createtablesdata
{
    UIViewController *layerTableViewController = [[UIViewController alloc] init];
    self.layerTableView = [[UITableView alloc] initWithFrame:CGRectMake(205,420,0,0) style:UITableViewStylePlain];
    layerTableViewController.contentSizeForViewInPopover = CGSizeMake(200, 420);
    layerTableViewController.view = self.layerTableView;
    self.layerTableView.delegate = self;
    self.layerTableView.dataSource = self;
    self.layerTableView.pagingEnabled = NO;
    
    
    // new code
    self.editing=FALSE;
    self.layerTableView.allowsSelectionDuringEditing=YES;
    // end
    
    
    self.popoverController.delegate = self;
    self.popoverController.popoverContentSize = CGSizeMake(300, 50);
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:layerTableViewController];
    [self.popoverController presentPopoverFromRect:trackbutton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionUp animated:YES];
    
}

//// to test 20/12/2015
//-(void)createtablesdata1
//{
//    UIViewController *layerTableViewController = [[UIViewController alloc] init];
//    self.layerTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0,0,0) style:UITableViewStylePlain];
//    layerTableViewController.contentSizeForViewInPopover = CGSizeMake(0, 0);
//    layerTableViewController.view = self.layerTableView;
//    self.layerTableView.delegate = self;
//    self.layerTableView.dataSource = self;
//    self.layerTableView.pagingEnabled = NO;
//    // new code
//    self.layerTableView.allowsSelectionDuringEditing=YES;
//    // end
//    
//    
//    self.popoverController.delegate = self;
//    self.popoverController.popoverContentSize = CGSizeMake(0, 0);
//    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:layerTableViewController];
//    
////    [self.layerTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[layerCount count]-1 inSection:0]];
////    [self.popoverController presentPopoverFromRect:CGRectMake(-100, -100, 0, 0) inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionUp animated:YES];
//////    
////    [self.popoverController dismissPopoverAnimated:YES];
//    
//    
//    //[self reloadInputViews];
//    
//}

// new method
- (BOOL)booltake
{
    if(![self.annotationController.annotationType isEqualToString:@"None"])
    {
        return YES;
    }
    else
    {
    return tapimagehannd;
    }
}


-(void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar handButton:(UIButton *)button
{
   
    
    @try
    {
        [self layercountcheck]; // to check layer added or not
    }
    @catch (NSException *exception) {
        NSLog(@"Crash");
    }
    
    
    
    if(layeraddcount)
    {
        if(!layerIndex.layerName)
        {
            [self alertshow1];
            return;
        }
        
        if(handimageenable==YES)
        {
            
            if(tapimagehannd==NO)
            {
                 self.annotationController.handchecktrack=NO;
                [button setImage:[UIImage imageNamed:@"hand_hi"] forState:UIControlStateNormal];
                [self setAnnotationMode:AnnotationViewControllerType_None];
                [self.annotationController activateTheTextBox];
                tapimagehannd=YES;
                
                
                
                theScrollView.scrollEnabled=NO;
                NSInteger page = [document.pageNumber integerValue];
                NSNumber *key = [NSNumber numberWithInteger:page];
                ReaderContentView *targetView = [contentViews objectForKey:key];
                targetView.scrollEnabled=NO;
                
                // new code
                [annotateToolbar makedoneiconchange];
                
                //SPUserResizableView *view;
                // view.userInteractionEnabled=YES;
            }
            else
            {
                // new code
                [annotateToolbar makeHandiconchange];
                
                self.annotationController.handchecktrack=YES;
                [button setImage:[UIImage imageNamed:@"hand.png"] forState:UIControlStateNormal];
                [self.annotationController deactivatetextbox];
                tapimagehannd=NO;
                
                 theScrollView.scrollEnabled=YES;
                NSInteger page = [document.pageNumber integerValue];
                NSNumber *key = [NSNumber numberWithInteger:page];
                ReaderContentView *targetView = [contentViews objectForKey:key];
                targetView.scrollEnabled=YES;
                // new code
                //SPUserResizableView *view;
                //view.userInteractionEnabled=NO;
                
            }
        }
        
        else
        {
            // new code
            
            self.annotationController.handchecktrack=YES;
            [button setImage:[UIImage imageNamed:@"hand.png"] forState:UIControlStateNormal];
            // end
            [self.annotationController deactivatetextbox];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required atleast one textbox" message:@"Please add atleast one textbox" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    else
    {
        [self alertshow];
        return;
        // button.userInteractionEnabled=NO;
    }
    
    // new code
    if(!layerIndex.layerName)
    {
        [self alertshow1];
        return;
    }
    
    
    // end
    
    // new code
    int positionlayer=-1;
    // new code for reorder
    for(int i=0;i<self.layerCount.count;i++)
    {
        LayerIndex *trackposition=[self.layerCount objectAtIndex:i];
        if(trackposition.layerindex==layerIndex.layerindex)
        {
            positionlayer=i;
        }
    }
    
    // end
    
    
    if(notdidselect==NO)
    {
        if(positionlayer>=0)
        {
            LayerIndex *layer1=[self.layerCount objectAtIndex:positionlayer];
            if([layer1.aTextViewIndexArray count]>0)
            {
                handimageenable=YES;
            }
            else
            {
                handimageenable=NO;
            }
        }
    }
    
    
    tracktoolbutton=[[UIButton alloc]init];
    tracktoolbutton=button;

}


// new code because on done it was crashing
- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar doneButton:(UIButton *)button
{

    self.annotationController.handchecktrack=YES;
    
    [annotateToolbar makeHandiconchange];
    //[button setImage:[UIImage imageNamed:@"done_hi"] forState:UIControlStateNormal];
    
   // [self cancelAnnotation];
}



- (void) tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar shareButton:(UIButton *)button
{
    // new code
     [annotateToolbar makeHandiconchange];
    
    
    UIViewController* popoverContent = [[UIViewController alloc]init];
    SharePopOverView *sharePopOverView = [[SharePopOverView alloc] initWithFrame:CGRectMake(0, 0, 310, 130)];
    // new code write here
     [sharePopOverView.draftImage addTarget:self action:@selector(savedraft:) forControlEvents:UIControlEventTouchUpInside];
    
    [sharePopOverView.exportImage addTarget:self action:@selector(imagepdf:) forControlEvents:UIControlEventTouchUpInside];
    
    // new one
    [sharePopOverView.emailimage addTarget:self action:@selector(emailpdf:) forControlEvents:UIControlEventTouchUpInside];
    
    // end
    
    
    
    sharePopOverView.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
    popoverContent.view = sharePopOverView;
    popoverContent.contentSizeForViewInPopover = CGSizeMake(310, 130);
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popoverContent];
    self.popoverController.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
    [self.popoverController presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionUp animated:YES];
    
    
}

// new method

-(void)imagepdf:(id)sender
{
    // here will export pdf
    NSLog(@"Export pdf thing");
    // here will export pdf
    NSLog(@"Export pdf thing");
    
    // modify code
    @try
    {
        [self.annotationController saveToexport];
        
        [self.popoverController dismissPopoverAnimated:YES];
        
        // new code
        dispatch_queue_t defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(defaultQueue, ^{
            [document saveReaderDocumentWithAnnotations]; // Save any ReaderDocument object changes
            document.password = nil; // Clear out any document password
            
        });
        
        [[ReaderThumbQueue sharedInstance] cancelOperationsWithGUID:document.guid];
        
        //[[ReaderThumbCache sharedInstance] removeAllObjects]; // Empty the thumb cache
        
        if ([delegate respondsToSelector:@selector(dismissReaderViewController:)] == YES)
        {
            [delegate dismissReaderViewController:self]; // Dismiss the ReaderViewController
        }
        else // We have a "Delegate must respond to -dismissReaderViewController: error"
        {
            NSAssert(NO, @"Delegate must respond to -dismissReaderViewController:");
        }
        
        
// end
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"exception:-%@",exception.description);
    }

}

// new for email
-(void)emailpdf:(id)sender
{
    NSLog(@"Here we will have to write the code for save");
    // modify code
    @try
    {
        //[self.annotationController saveToDraft];
        
        [self performSelectorOnMainThread:@selector(dismissback) withObject:self waitUntilDone:YES];
        
        [self.popoverController dismissPopoverAnimated:YES];
        
        //        // new code
        //        dispatch_queue_t defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        //        dispatch_async(defaultQueue, ^{
        //            [document saveReaderDocumentWithAnnotations]; // Save any ReaderDocument object changes
        //            document.password = nil; // Clear out any document password
        //
        //        });
        //
        //        [[ReaderThumbQueue sharedInstance] cancelOperationsWithGUID:document.guid];
        //
        //        //[[ReaderThumbCache sharedInstance] removeAllObjects]; // Empty the thumb cache
        //
        //        if ([delegate respondsToSelector:@selector(dismissReaderViewController:)] == YES)
        //        {
        //            [delegate dismissReaderViewController:self]; // Dismiss the ReaderViewController
        //        }
        //        else // We have a "Delegate must respond to -dismissReaderViewController: error"
        //        {
        //            NSAssert(NO, @"Delegate must respond to -dismissReaderViewController:");
        //        }
        
        
        // end
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"exception:-%@",exception.description);
    }
}

// new code
-(void)dismissback
{
   [self.annotationController savetoemail];
}


-(void)savedraft:(id)sender
{
    NSLog(@"Here we will have to write the code for save");
    // modify code
    @try
    {
        [self.annotationController saveToDraft];
        
//        [self.popoverController dismissPopoverAnimated:YES];
//        
//        // new code
//        dispatch_queue_t defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//        dispatch_async(defaultQueue, ^{
//            [document saveReaderDocumentWithAnnotations]; // Save any ReaderDocument object changes
//            document.password = nil; // Clear out any document password
//            
//        });
//        
//        [[ReaderThumbQueue sharedInstance] cancelOperationsWithGUID:document.guid];
//        
//        //[[ReaderThumbCache sharedInstance] removeAllObjects]; // Empty the thumb cache
//        
//        if ([delegate respondsToSelector:@selector(dismissReaderViewController:)] == YES)
//        {
//            [delegate dismissReaderViewController:self]; // Dismiss the ReaderViewController
//        }
//        else // We have a "Delegate must respond to -dismissReaderViewController: error"
//        {
//            NSAssert(NO, @"Delegate must respond to -dismissReaderViewController:");
//        }
//        
//        
//        // end
//        
    }
    @catch (NSException *exception)
    {
        NSLog(@"exception:-%@",exception.description);
    }
}


// new method to return layer count
-(void)layercountcheck
{
       // new code
    if([self.layerCount count]>0)
    {
        layeraddcount=YES;
        // new code
        if(notdidselect==YES)
        {
            LayerIndex *layer=[layerCount objectAtIndex:testtextview];
            if([layer.aTextViewIndexArray count]>0)
            {
                handimageenable=YES;
                // new code
                [annotateToolbar makedoneiconchange];
            }
            else
            {
                handimageenable=NO;
            }
        }
        else
        {
            
            if([layerIndex.aTextViewIndexArray count]>0)
            {
                handimageenable=YES;
            }
            else
            {
                handimageenable=NO;
               
            }
        }
        
    }
    else
    {
        layeraddcount=NO;
    }
}


// new method
-(void)alertshow1
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"!Selection Required" message:@"Please Select any layer" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    return;
    
}
// new method
-(void)alertshow
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required layer" message:@"Please add any layer" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    return;

}

- (void) tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar backButton:(UIButton *)button
{
    [self cancelAnnotation];
}

- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar zutton:(UIButton *)button
{
    
    NSLog(@"Done Tapped");
    for (UIView *View in self.view.subviews)
    {
        for (ReaderContentView *readerContentView in View.subviews){
            if ([readerContentView isKindOfClass:[UIScrollView class]]){
                readerContentView.userInteractionEnabled = NO;
            }
        }
    }
    NSInteger page = [document.pageNumber integerValue];
    NSNumber *key = [NSNumber numberWithInteger:page];
    ReaderContentView *targetView = [contentViews objectForKey:key];
    [targetView zoomReset];
    

}

// slider Value Selector methods
- (void)markerSliderAction:(id)sender
{
    UISlider *aslider = (UISlider *)sender;
    NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
    [def setObject:[NSNumber numberWithInt:aslider.value] forKey:@"MarkerValue"];
    self.annotationController.markerLineWidth = lround(aslider.value);
}

-(void)pencilSliderAction:(id)sender
{
    
    
    UISlider *aslider = (UISlider *)sender;
    NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
    [def setObject:[NSNumber numberWithInt:aslider.value] forKey:@"PencileValue"];
    [def synchronize];
    NSLog(@"%f",aslider.value);
    
    self.annotationController.pencilLineWidth = lround(aslider.value);
    
}
//------------color of the line annotations for pdf---------------//

- (void)pencilColorViewChangeColor:(CGColorRef)_color
{
    self.annotationController.pencilColor = _color;
    
}

-(void)markerColorViewChangeColor:(CGColorRef)_color
{
    self.annotationController.markerColor = _color;
    
}

-(void)eraserSliderAction:(id)sender
{
    
    UISlider *aslider = (UISlider *)sender;
    NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
    [def setObject:[NSNumber numberWithInt:aslider.value] forKey:@"EraserValue"];
    self.annotationController.eraserLineWith = lround(aslider.value);

}

- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar aTextButton:(UIButton *)button
{
    // new code
   // [annotateToolbar makeHandiconchange];
    
    [self layercountcheck];// to check layer added or not
    // new code
    LayerIndex *test=layerIndex;
    NSLog(@"%@",test);
    
    if(layeraddcount==YES)
    {
        if(!layerIndex.layerName)
        {
            [self alertshow1];
            return;
        }
    }
    // end
    
    if(layeraddcount==YES)
    {
        // new code
        // new code
        [annotateToolbar makedoneiconchange];
        [button setImage:[UIImage imageNamed:@"type_hi"] forState:UIControlStateNormal];
        
        UIViewController* popoverContent = [[UIViewController alloc]init];
        TextPopOverView *textPopOverView = [[TextPopOverView alloc] initWithFrame:CGRectMake(0, 0, 300, 250)];
        textPopOverView.FontDelegate = self;
        [textPopOverView.aNewImage addTarget:self action:@selector(addTextViewToLayer:) forControlEvents:UIControlEventTouchUpInside];
        [textPopOverView.incTextSize addTarget:self action:@selector(incTextSizeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [textPopOverView.decTextSize addTarget:self action:@selector(decTextSizeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        textPopOverView.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
        popoverContent.view = textPopOverView;
        popoverContent.contentSizeForViewInPopover = CGSizeMake(300, 250);
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:popoverContent];
        self.popoverController.backgroundColor = [UIColor colorWithRed:0.180 green:0.176 blue:0.176 alpha:1.0];
        [self.popoverController presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionUp animated:YES];
    }
    else
    {
        [self alertshow];
        return;
        
    }
    
}

-(void)handbuttonchange:(BOOL)take
{
    handimageenable=YES;
    tapimagehannd=YES;
    deletecase=YES;
    
    self.annotationController.handchecktrack=YES;
    [self.annotationController deactivatetextbox];
    tapimagehannd=NO;
    [annotateToolbar makeHandiconchange];
    
}

- (void)tappedInAnnotateToolbar:(ReaderAnnotateToolbar *)toolbar sendbutton:(UIButton *)button
{
    [button setImage:[UIImage imageNamed:@"hand.png"] forState:UIControlStateNormal];
    [self.annotationController deactivatetextbox];
    [self setAnnotationMode:AnnotationViewControllerType_None];
    tapimagehannd=NO;
    handimageenable=NO;
    
    // new code
    
    
    
    theScrollView.scrollEnabled=YES;
    NSInteger page = [document.pageNumber integerValue];
    NSNumber *key = [NSNumber numberWithInteger:page];
    ReaderContentView *targetView = [contentViews objectForKey:key];
    targetView.scrollEnabled=YES;
    // end
    
}


-(void)addTextViewToLayer:(id)sender
{
    
    int count=[self.annotationController addingTextView];
    
    
    if(count>0)
    {
        handimageenable=YES;
        tapimagehannd=NO;
    }

    // new code
    NSInteger page = [document.pageNumber integerValue];
    NSNumber *key = [NSNumber numberWithInteger:page];
    ReaderContentView *targetView = [contentViews objectForKey:key];
    [targetView zoomReset];
    
    // end
     
    [annotateToolbar makeHandActive];
}

// Increase and decrease the textview font size

-(void)incTextSizeButtonTapped:(id)sender
{
    [self.annotationController incTextSize];
}

-(void)decTextSizeButtonTapped:(id)sender
{
   [self.annotationController decTextSize];
}
-(void) FontChangeForText:(NSString *)_font
{
    [self.annotationController setFontForText:_font];
}
-(void) TextColorChange:(CGColorRef)textColor
{
    [self.annotationController changingTextColor:textColor];
}
-(void) addLayerButtonTapped:(id)sender
{
    //[SVProgressHUD dismiss];
    [self hudstart];
    notdidselect=NO;
    justview=YES;
    
//    [self.annotationController layerAddToPdf];
//    [SVProgressHUD dismiss];
    
    [self.annotationController performSelector:@selector(layerAddToPdf) withObject:nil afterDelay:0.00001];
    
   
   
    //[self.annotationController performSelectorOnMainThread:(@selector(layerAddToPdf)) withObject:self.annotationController waitUntilDone:YES];
      
     

}
// new code
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row)
    {
        [SVProgressHUD dismiss];
        //end of loading
        //for example [activityIndicator stopAnimating];
    }
}

-(void)hudstart
{
    [SVProgressHUD showWithStatus:@"Loading"];
}
#pragma mark LayerCountDelegate

-(void)getLayers:(NSMutableArray*)fechedResult
{
    
    if([fechedResult count]>0)
    {
        layeraddcount=YES;
    }
    self.layerCount = fechedResult;
    // want to save array in core data
//    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:fechedResult];
//    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"layercount"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self.layerTableView reloadData];
}

#pragma mark TableViewDelegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0)
    {
        return 0;
    }
    else
    {
      //  NSLog(@"%d",self.layerCount.count);
        // new code
      
    return self.layerCount.count;
    }
}
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    if (section == 0)
    {
        return @"";
    }
    else
    {
        return @"no.of layers";
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    
    
    static NSString *MyIdentifier = @"MyIdentifier";
    
   // NSLog(@"%@",self.annotationController.containerView.subviews);
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    // new code
    int position=-1;
    int otherposition;
    
    // new code for reorder
    for(int i=0;i<self.layerCount.count;i++)
    {
        LayerIndex *trackposition=[self.layerCount objectAtIndex:i];
        if(trackposition.layerindex==layerIndex.layerindex)
        {
            position=i;
            // new code
            annotateToolbar->currentlayername.hidden=FALSE;
            annotateToolbar->currentlayername.text=[NSString stringWithFormat:@"%@ ( %@ )",layerIndex.layerName,[NSString stringWithFormat:@"%lu",(unsigned long)[layerCount count]]];
            
        }
        
    }
    
    // end
    
    
   // LayerIndex *test=layerIndex;
    // NSLog(@"%d",test.layerindex);
    if(layerIndex.pageIndex==currentPage)
    {
        if(position==indexPath.row)
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            
            
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            
        }
    }
    else
    {
        layerIndex=[[LayerIndex alloc]init];
        self.annotationController.checkmark=YES;
    }
    
    // new code
    NSLog(@"indexpathrow=%d",indexPath.row);
    NSLog(@"totallayer=%d",[self.layerCount count]);
    
    
    // new code
   // NSLog(@"afterdidselect=%@",self.annotationController.containerView.subviews);
    UIImageView *used;
    
    // this code when not didselect
    
    if(layerIndex.layerindex)
    {
   
    for(int k=0;k< self.annotationController.containerView.subviews.count;k++)
    {
    LayersView *checkView = self.annotationController.containerView.subviews[k];
    LayerIndex *checkView1=[layerCount objectAtIndex:indexPath.row];
    if([checkView tag]==checkView1.layerindex)
    {
    if([checkView isKindOfClass:[LayersView class]])
    {
   
        // track other position
        for(int i=0;i<self.layerCount.count;i++)
        {
            LayerIndex *trackposition=[self.layerCount objectAtIndex:i];
            if(trackposition.layerindex==[checkView tag])
            {
                otherposition=i;
            }
            
        }
        
        //
        
        
        
    if ([checkView tag] == layerIndex.layerindex)
    {
           // NSLog(@"currentsubviews=%@",self.annotationController.containerView.subviews);
           [self.annotationController.containerView bringSubviewToFront:checkView];
           // NSLog(@"currentsubviewsaftersendfront=%@",self.annotationController.containerView.subviews);
            self.annotationController.index = layerIndex.layerindex;
            self.annotationController.layer = checkView;
            layerIndex=layerIndex;
            if(layerIndex.layerName)
            {
             if([self.annotationController.containerView.subviews count]>0)
             {
             for (UIImageView *v in self.annotationController.containerView.subviews)
             {
             if([v tag]==[checkView tag])
             {
              if([v isKindOfClass:[UIImageView class]])
              {
                  v.hidden=TRUE;
              }
             }
             }
             }
                //NSLog(@"finalresult=%@",self.annotationController.containerView.subviews);
                checkView.hidden=FALSE;
            }
        }
        else
        {
            BOOL bluredadded;
          
            if(layerIndex.layerName)
            {
            if([checkView tag]!=0)
            {
                bluredadded=NO;
               // int multiple=[layerCount count]-[checkView tag];
                
                if([self.annotationController.containerView.subviews count]>0)
                {
                    for (UIImageView *v in self.annotationController.containerView.subviews)
                    {
                        if([v tag]==[checkView tag])
                        {
                            if([v isKindOfClass:[UIImageView class]])
                            {
                               // [v removeFromSuperview];
                                v.hidden=FALSE;
                                checkView.hidden=FALSE;
                                // will have to remove border before making image
                                for (SPUserResizableView *sp in checkView.subviews)
                                {
                                    if([sp isKindOfClass:[SPUserResizableView class]])
                                    {
                                        
                                    }
                                }
                                
                                // end
                                
                                
                                UIImage *testimage=[self getUIImageFromThisUIView:checkView];
                                NSData *pngData = UIImagePNGRepresentation(testimage);
                                bluredimage=[UIImage imageWithData:pngData];
                                NSData *image=UIImageJPEGRepresentation(bluredimage, 1.0);
                                bluredimage=[UIImage imageWithData:image];
                                const CGFloat colorMasking[6] = {222,255,222,255,222,255};
                                bluredimage = [UIImage imageWithCGImage: CGImageCreateWithMaskingColors(bluredimage.CGImage, colorMasking)];
                                
                                // new code
                                // new code
                                long multiple=[layerCount count]-(otherposition+1);
                                if(multiple==0)
                                {
                                    multiple=1;
                                }
                                
                                for(int k=0;k<multiple;k++)
                                {
                                    bluredimage=[self imageWithGaussianBlur:bluredimage];
                                }
                                // end
                                
                                v.image=bluredimage;
                                
                                checkView.hidden=TRUE;
                                bluredadded=YES;
                                
                            }
                        }
                    }
                }
                
                
               if(bluredadded==NO)
               {
                checkView.hidden=FALSE;
                
                UIImage *testimage=[self getUIImageFromThisUIView:checkView];
                NSData *pngData = UIImagePNGRepresentation(testimage);
                bluredimage=[UIImage imageWithData:pngData];
                NSData *image=UIImageJPEGRepresentation(bluredimage, 1.0);
                bluredimage=[UIImage imageWithData:image];
                const CGFloat colorMasking[6] = {222,255,222,255,222,255};
              //  const CGFloat *colorMasking = {222,255,222,255,222,255};
                   
                bluredimage = [UIImage imageWithCGImage: CGImageCreateWithMaskingColors(bluredimage.CGImage,colorMasking)];
                   
                // new code
                int multiple=[layerCount count]-(otherposition+1);
                   if(multiple==0)
                   {
                       multiple=1;
                   }
                for(int k=0;k<multiple;k++)
                {
                     bluredimage=[self imageWithGaussianBlur:bluredimage];
                }
            // end
               
                used=[[UIImageView alloc]initWithFrame:checkView.bounds];
                used.image=bluredimage;
                used.tag=[checkView tag];
                checkView.hidden=TRUE;
                // NSLog(@"subview count before add=%d",self.annotationController.containerView.subviews.count);
                [self.annotationController.containerView addSubview:used];
               // NSLog(@"subview count after add=%d",self.annotationController.containerView.subviews.count);
                
            }
                else
                {
                    // new code
                    for (UIImageView *take in self.annotationController.containerView.subviews)
                    {
                        if([take isKindOfClass:[UIImageView class]])
                        {
                        if([checkView tag]==[take tag])
                        {
                            // new code
                            if(notdidselect==NO)
                            {
                                if(justview==YES)
                                {
                            bluredimage=take.image;
                            bluredimage=[self imageWithGaussianBlur:bluredimage];
                            take.image=bluredimage;
                                }
                            }
                        }
                        }
                    }
                    
                    // end
                   
                    checkView.hidden=TRUE;
                }
            }
        }
        }
        break;
        }
        else
        {
            // new one
            //checkView.hidden=TRUE;
            continue;
        }
        break;
    }
        else
        {
            continue;
        }
    }
    }

    LayerIndex *layerIndex = [self.layerCount objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",layerIndex.layerName];
    return cell;
}

// new blur method
- (UIImage *)imageWithGaussianBlur:(UIImage *)image
{
    float weight[5] = {0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162};
    for (int i = 0 ; i < 5 ; i++) {
        weight[i] = weight[i] * .9;
    }
    // Blur horizontally
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
    for (int x = 1; x < 5; ++x) {
        [image drawInRect:CGRectMake(x, 0, image.size.width, image.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[x]];
        [image drawInRect:CGRectMake(-x, 0, image.size.width, image.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[x]];
    }
    UIImage *horizBlurredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // Blur vertically
    UIGraphicsBeginImageContext(image.size);
    [horizBlurredImage drawInRect:CGRectMake(0, 0, image.size.width, image.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
    for (int y = 1; y < 5; ++y) {
        [horizBlurredImage drawInRect:CGRectMake(0, y, image.size.width, image.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
        [horizBlurredImage drawInRect:CGRectMake(0, -y, image.size.width, image.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
    }
    UIImage *blurredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //
    return blurredImage;
}

// new one
-(UIImage *)changeWhiteColorTransparent: (UIImage *)image
{
    CGImageRef rawImageRef=image.CGImage;
    
    const CGFloat colorMasking[6] = {222, 255, 222, 255, 222, 255};
    
    UIGraphicsBeginImageContext(image.size);
    CGImageRef maskedImageRef=CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    {
        //if in iphone
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, image.size.height);
        CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    }
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, image.size.width, image.size.height), maskedImageRef);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    return result;
}


// new code for bluring view

-(UIImage*)getUIImageFromThisUIView:(UIView*)aUIView
{
    UIGraphicsBeginImageContext(aUIView.bounds.size);
   // UIGraphicsBeginImageContextWithOptions(aUIView.bounds.size, aUIView.opaque, 0.0);
    [aUIView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}

- (UIImage*) blur:(UIImage*)theImage :(int)multiplier
{
    // ***********If you need re-orienting (e.g. trying to blur a photo taken from the device camera front facing camera in portrait mode)
    // theImage = [self reOrientIfNeeded:theImage];
    
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:theImage.CGImage];
    
    // setting up Gaussian Blur (we could use one of many filters offered by Core Image)
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:(multiplier*5)] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage];//create a UIImage for this function to "return" so that ARC can manage the memory of the blur... ARC can't manage CGImageRefs so we need to release it before this function "returns" and ends.
    CGImageRelease(cgImage);//release CGImageRef because ARC doesn't manage this on its own.
    
    return returnImage;
    
    // *************** if you need scaling
    // return [[self class] scaleIfNeeded:cgImage];
}





-(UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView;
    NSLog(@"True1");
    if (tableView == self.layerTableView)
    {
        if (section == 0)
        {
            sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.layerTableView.bounds.size.width, self.layerTableView.bounds.size.height)];
            
            UILabel *label = [[UILabel alloc] init];
            [label setFrame:CGRectMake(20, 5, 150, 35)];
            label.font = [UIFont systemFontOfSize:22.0f];
            label.text = @"Layers";
            [sectionView addSubview:label];
            
            UIButton *addLayerButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [addLayerButton setFrame:CGRectMake(80,5, 100, 35)];
            [addLayerButton addTarget:self action:@selector(addLayerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [addLayerButton setImage:[UIImage imageNamed:@"add_layers"] forState:UIControlStateNormal];
            [sectionView addSubview:addLayerButton];
            
            //============== Edit Button===============//
//            editLayerButton = [UIButton buttonWithType:UIButtonTypeCustom];
            // new code
           // self.editing=FALSE;
            
            [editLayerButton setFrame:CGRectMake(145,5, 100, 35)];
            [editLayerButton addTarget:self action:@selector(editLayerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [editLayerButton setImage:[UIImage imageNamed:@"swap_layers"] forState:UIControlStateNormal];
            [sectionView addSubview:editLayerButton];
            
            
            
            
            
// new code
            if(self.editing)
            {
                NSLog(@"True");
                [editLayerButton setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
            }
            else
            {
                 NSLog(@"False");
                [editLayerButton setImage:[UIImage imageNamed:@"swap_layers.png"] forState:UIControlStateNormal];
            }
            
            
        }
    }
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return  45;
    }
    else
    {
        return  30;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSLog(@"%@",self.annotationController.containerView.subviews);
    // new code
    
    tracktable=[[UITableView alloc]init];
    tracktable=tableView;
    
    trackindexpath=-1;
    
    if (tableView.editing == YES)
    {
        // Table view is editing - run code here
        alertx=[[UIAlertView alloc]initWithTitle:@"" message:@"\nEnter New Layer name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alertx.tag=501;
        [alertx setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alertx textFieldAtIndex:0].autocapitalizationType = UITextAutocapitalizationTypeWords;
        [alertx textFieldAtIndex:0].delegate=(id)self;
        [alertx show];
        trackindexpath=indexPath.row;
    }
else
{
    notdidselect=YES;
    // new one
    self.annotationController.checkmark=NO;
    if (tableView == self.layerTableView)
    {
        if (indexPath.section == 0)
        {
            
        }
        else
        {
            // new code
            [self hudstart];
            LayerIndex *layerindex = [self.layerCount objectAtIndex:indexPath.row];
            for (LayersView *checkView in self.annotationController.containerView.subviews)
            {
            
                // new code
                if([checkView isKindOfClass:[LayersView class]])
                {
                if ([checkView tag] == layerindex.layerindex)
                {
                   // NSLog(@"%@",self.annotationController.containerView.subviews);
                    [self.annotationController.containerView bringSubviewToFront:checkView];
                    self.annotationController.index = layerindex.layerindex;
                    self.annotationController.layer = checkView;
                    layerIndex=layerindex;
                    
                }
                else
                {
                    NSLog(@"Other view");
                }
                
                }
            }
           
            testtextview=indexPath.row;
            if([layerindex.aTextViewIndexArray count]>0)
            {
                handimageenable=YES;
            }
            else
            {
                handimageenable=NO;
            }
            
            
            /*// to test
            for (LayersView *checkView in self.annotationController.containerView.subviews)
            {
                if([checkView isKindOfClass:[LayersView class]])
                {
                    [self.annotationController.containerView bringSubviewToFront:checkView];
                }
            }*/
            
        }
    }
    
    [self performSelector:@selector(reloadtable) withObject:self afterDelay:0.00001];
}
     // NSLog(@"%@",self.annotationController.containerView.subviews);
    // new code
  
}
// new code
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if ([textField.text length] >= 15)
//    {
//        textField.text = [textField.text substringToIndex:15-1];
//        NSLog(@"calling Limit input");
//        return NO;
//    }
//    return YES;
//}

// end

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
     NSString *trimmedString = [[[alertx textFieldAtIndex:0] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if([alertx textFieldAtIndex:0])
    {
        NSUInteger newLength = [trimmedString length] + [string length] - range.length;
        return (newLength > 20) ? NO : YES;
    }
    return YES; // suppress warning about control reaching end of non-void function
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ((alertView.tag=501))
    {
        
        if (buttonIndex==1)
        {
            NSString *trimmedString = [[[alertView textFieldAtIndex:0] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            
            if ([trimmedString length]>0)
            {
                
                NSString *newlayer=[[NSString alloc]init];
                newlayer=[[alertView textFieldAtIndex:0]text];
                LayerIndex *layerIndex = [self.layerCount objectAtIndex:trackindexpath];
                layerIndex.layerName=newlayer;
                
                // new code
                annotateToolbar->currentlayername.hidden=FALSE;
                annotateToolbar->currentlayername.text=[NSString stringWithFormat:@"%@ ( %@ )",layerIndex.layerName,[NSString stringWithFormat:@"%lu",(unsigned long)[layerCount count]]];
                
                
                
                [self performSelector:@selector(reloadtable) withObject:self afterDelay:0.00001];
             
            }
            else
            {
                    alertx=[[UIAlertView alloc]initWithTitle:@"" message:@"Please put any layername" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertx show];
               
            }
        }
        
        
    }
}

// new method for reload
-(void)reloadtable
{
    [tracktable reloadData];
}


-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if (indexPath.section == 1)
        {
            LayerIndex *layerindex = [self.layerCount objectAtIndex:indexPath.row];
            for (LayersView *checkView in self.annotationController.containerView.subviews)
            {
                
                if ([checkView tag] == layerindex.layerindex)
                {
                    
                    [self.annotationController.containerView bringSubviewToFront:checkView];
                    [checkView removeFromSuperview];
                    // new code
                    if([checkView isKindOfClass:[LayersView class]])
                    {
                        [self.layerCount removeObjectAtIndex:indexPath.row];
                        [self deleteTheLayerInStore:layerindex.layerindex];
                    }
                    // new code
                  //  NSLog(@"%d",layerindex.layerindex);
                    
                    // new code
                    layerIndex=[[LayerIndex alloc]init];
                    // end
                    [layerTableView reloadData];
                }
            }
        }
    }
}

-(void) deleteTheLayerInStore:(NSInteger)index
{
    NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PdfDoc" inManagedObjectContext:mainMOC];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [mainMOC executeFetchRequest:fetchRequest error:nil];
    for (PdfDoc *allPdf in fetchedObjects)
    {
        if ([document.fileName isEqualToString:[allPdf valueForKey:@"pdfName"]])
        {
            NSArray *allPages = [allPdf.page allObjects];
            for (int i = 0; i< allPages.count; i++)
            {
                PdfPage *page = [allPages objectAtIndex:i];
                if ([page.pageNumber isEqual:[NSNumber numberWithInteger:currentPage]])
                {
                    NSArray *allLayers = [page.layers allObjects];
                    for (int j = 0; j< allLayers.count; j++)
                    {
                        PdfLayers *aLayer = [allLayers objectAtIndex:j];
                        if ([aLayer.layerindex isEqual:[NSNumber numberWithInt:index]])
                        {
                            [mainMOC deleteObject:aLayer];
                        }
                    }
                    NSError *error;
                    if (![mainMOC save:&error])
                    {
                        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                    }
                }
            }
        }
    }
}
//=========== Reording the TableViewCells Delegate Methods=================//
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return NO;
    }
    return YES;
}
-(void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    [self hudstart];
    
    LayerIndex *layerindex = [self.layerCount objectAtIndex:sourceIndexPath.row];
    // NSLog(@"sourceindex>>>>>>:%d",layerindex.layerindex);
   // NSLog(@"viewsource :%@",[self.annotationController.containerView.subviews objectAtIndex:sourceIndexPath.row]);
    
    //LayerIndex *destinationLayerindex = [self.layerCount objectAtIndex:destinationIndexPath.row];
   // NSLog(@"desindex>>>>>>:%d",destinationLayerindex.layerindex);
   // NSLog(@"viewdessss :%@",[self.annotationController.containerView.subviews objectAtIndex:destinationIndexPath.row]);
    [self.layerCount removeObjectAtIndex:sourceIndexPath.row];
    [self.layerCount insertObject:layerindex atIndex:destinationIndexPath.row];
    [self.annotationController.containerView exchangeSubviewAtIndex:sourceIndexPath.row withSubviewAtIndex:destinationIndexPath.row];
    
    // new code
    layerIndex=layerindex;
    
    tracktable=[[UITableView alloc]init];
    tracktable=tableView;
    [self performSelector:@selector(reloadtable) withObject:self afterDelay:0.00001];
    
    
}
-(void) editLayerButtonTapped:(id)sender
{
    
    if([self.layerCount count]>0)
    {
        //[self hudstart];
    [self performSelector:@selector(editing) withObject:self afterDelay:0.00001];
    }
    
    
   }

-(void)editing
{
    // new code
    notdidselect=NO;
    
    if (self.editing)
    {
        [super setEditing:NO animated:NO];
        [self.layerTableView setEditing:NO animated:NO];
        //[editLayerButton setImage:[UIImage imageNamed:@"swap_layers.png"] forState:UIControlStateNormal];
//        [editLayerButton setTitle:@"Done" forState:UIControlStateNormal];
//        [editLayerButton setBackgroundColor:[UIColor redColor]];
        
       
        
    }
    else
    {
        [super setEditing:YES animated:NO];
        [self.layerTableView setEditing:YES animated:NO];
      //  [editLayerButton setTitle:@"Done" forState:UIControlStateNormal];
       // [editLayerButton setBackgroundColor:[UIColor clearColor]];
//        [editLayerButton reloadInputViews];
       
      //  [editLayerButton setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
      
    }
    
    [self.layerTableView reloadData];

}

#pragma mark Annotation Flow

- (void) startAnnotation
{
    [annotateToolbar showToolbar];
    [mainToolbar hideToolbar];
    ReaderContentView *view = [contentViews objectForKey:document.pageNumber];
    [self.annotationController moveToPage:[document.pageNumber intValue] contentView:view];
    self.annotationController.delegate = self;
    [self.view insertSubview:self.annotationController.view belowSubview:annotateToolbar];
}

- (void) cancelAnnotation
{
    [annotateToolbar hideToolbar];
    [mainToolbar showToolbar];

    [self.annotationController clear];
    [self.annotationController hide];
}

- (void) finishAnnotation
{
    
    
    //[annotateToolbar hideToolbar];
    [mainToolbar showToolbar];
    AnnotationStore *annotations = [self.annotationController annotations];
    [document.annotations addAnnotations:annotations];
    ReaderContentView *view = [contentViews objectForKey:document.pageNumber];
    [[view contentView] setNeedsDisplay];
    [self.annotationController clear];
    [self.annotationController hide];

}

- (void)handleAnnotationModeNotification:(NSNotification *)notification
{
    if ([notification.name isEqualToString:DocumentsSetAnnotationModeSignNotification])
    {
        [self startAnnotation];
        [self setAnnotationMode:AnnotationViewControllerType_Sign];
    }
    if ([notification.name isEqualToString:DocumentsSetAnnotationModeRedPenNotification])
    {
        [self startAnnotation];
        [self setAnnotationMode:AnnotationViewControllerType_RedPen];
    }
    if ([notification.name isEqualToString:DocumentsSetAnnotationModeOffNotification])
    {
        [self cancelAnnotation];
        [self setAnnotationMode:AnnotationViewControllerType_None];
    }
}

#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	#ifdef DEBUG
		if ((result == MFMailComposeResultFailed) && (error != NULL)) NSLog(@"%@", error);
	#endif

	[self dismissViewControllerAnimated:YES completion:^{
        
    }]; // Dismiss
}

#pragma mark ThumbsViewControllerDelegate methods

- (void)dismissThumbsViewController:(ThumbsViewController *)viewController
{
	
	[self dismissViewControllerAnimated:NO completion:^{
        
    }]; // Dismiss
}

- (void)thumbsViewController:(ThumbsViewController *)viewController gotoPage:(NSInteger)page
{
	[self showDocumentPage:page]; // Show the page
}

#pragma mark ReaderMainPagebarDelegate methods

- (void)pagebar:(ReaderMainPagebar *)pagebar gotoPage:(NSInteger)page
{
	[self showDocumentPage:page]; // Show the page
}

#pragma mark UIApplication notification methods

- (void)applicationWill:(NSNotification *)notification
{
	[document saveReaderDocument]; // Save any ReaderDocument object changes

	if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
	{
		if (printInteraction != nil) [printInteraction dismissAnimated:NO];
	}
}

@end
