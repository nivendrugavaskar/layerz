//
//  SharePopOverView.h
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharePopOverView : UIView

@property (strong, nonatomic) UILabel  *exportLabel;
@property (strong, nonatomic) UILabel  *draftLabel;
@property (strong, nonatomic) UILabel  *emailLabel;
@property (strong, nonatomic) UIButton *exportImage;
@property (strong, nonatomic) UIButton *draftImage;
@property (strong, nonatomic) UIButton *emailimage;
@end
