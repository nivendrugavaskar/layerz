//
//  SharePopOverView.m
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "SharePopOverView.h"

@implementation SharePopOverView
@synthesize exportLabel;
@synthesize draftLabel,emailLabel;
@synthesize exportImage;
@synthesize draftImage,emailimage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    
        self.exportLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 120, 30)];
        self.exportLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.exportLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.exportLabel.text = @"Export";
        [self addSubview:self.exportLabel];
        
        self.draftLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 80, 300, 30)];
        self.draftLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.draftLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.draftLabel.text = @"Save as Draft";
        [self addSubview:self.draftLabel];
        
        // new code
        self.emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 140, 300, 30)];
        self.emailLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.emailLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.emailLabel.text = @"Send as email";
        [self addSubview:self.emailLabel];
    
        self.exportImage = [[UIButton alloc] initWithFrame:CGRectMake(250, 20, 35, 30)];
        //self.exportImage.image = [UIImage imageNamed:@"export"];
        [self.exportImage setImage:[UIImage imageNamed:@"export"] forState:UIControlStateNormal];
        [self addSubview:self.exportImage];
        
        self.draftImage = [[UIButton alloc] initWithFrame:CGRectMake(250, 80, 35, 30)];
        [self.draftImage setImage:[UIImage imageNamed:@"save_draft"] forState:UIControlStateNormal];
        //self.draftImage. = [UIImage imageNamed:@"save_draft"];
        [self addSubview:self.draftImage];
        
        // new code
        self.emailimage = [[UIButton alloc] initWithFrame:CGRectMake(250, 140, 35, 30)];
        [self.emailimage setImage:[UIImage imageNamed:@"email"] forState:UIControlStateNormal];
        //self.draftImage. = [UIImage imageNamed:@"save_draft"];
        [self addSubview:self.emailimage];
        
    
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.274 green:0.274 blue:0.274 alpha:1.0].CGColor);
    CGContextMoveToPoint(context, 0, 65);
    CGContextAddLineToPoint(context, 360, 65);
    CGContextStrokePath(context);
}


@end
