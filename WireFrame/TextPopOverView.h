//
//  TextPopOverView.h
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorView.h"
#import "DropDownView.h"

@protocol FontDelegate
@required

@optional

- (void)FontChangeForText:(NSString*)_font;
- (void)TextColorChange:(CGColorRef)textColor;

@end


@interface TextPopOverView : UIView <ColorViewDelegate,DropDownViewDelegate>

@property (strong, nonatomic) id<FontDelegate> FontDelegate;
@property (strong, nonatomic) UILabel *aNewLabel;
@property (strong, nonatomic) UILabel *sizeLabel;
@property (strong, nonatomic) UILabel *styleLabel;
@property (strong, nonatomic) UILabel *textColorLabel;
@property (strong, nonatomic) UIButton *aNewImage;
@property (strong, nonatomic) UIButton *incTextSize;
@property (strong, nonatomic) UIButton *decTextSize;
@property (strong, nonatomic) UILabel  *fontStyleLabel;
@property (strong, nonatomic) UIButton *arrowButton;
@property (strong, nonatomic) NSArray *arrayData;
@property (strong, nonatomic) DropDownView *dropDownView;

@end
