//
//  TextPopOverView.m
//  WireFrame
//
//  Created by Raghavender  on 16/04/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "TextPopOverView.h"
#import "ColorView.h"

@implementation TextPopOverView
@synthesize aNewLabel;
@synthesize sizeLabel;
@synthesize styleLabel;
@synthesize textColorLabel;
@synthesize aNewImage;
@synthesize incTextSize;
@synthesize decTextSize;
@synthesize fontStyleLabel;
@synthesize arrowButton;
@synthesize arrayData;
@synthesize dropDownView;
@synthesize FontDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.aNewLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 30)];
        self.aNewLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.aNewLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.aNewLabel.text = @"New";
        [self addSubview:self.aNewLabel];
        
        self.sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 80, 300, 30)];
        self.sizeLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.sizeLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.sizeLabel.text = @"Size";
        [self addSubview:self.sizeLabel];
        
        self.styleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 140, 300, 30)];
        self.styleLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.styleLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.styleLabel.text = @"Style";
        [self addSubview:self.styleLabel];
        
        self.fontStyleLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 140, 150, 30)];
        self.fontStyleLabel.backgroundColor = [UIColor whiteColor];
        self.fontStyleLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.fontStyleLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        [self addSubview:self.fontStyleLabel];
        
        self.arrowButton = [[UIButton alloc] initWithFrame:CGRectMake(270, 140, 20, 30)];
        [self.arrowButton setImage:[UIImage imageNamed:@"dropdown_arrow"] forState:UIControlStateNormal];
        [self.arrowButton addTarget:self action:@selector(arrowButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.arrowButton];
        
        self.incTextSize  = [[UIButton alloc] initWithFrame:CGRectMake(200, 80, 30, 25)];
        [self.incTextSize setImage: [UIImage imageNamed:@"increase_textsize"] forState:UIControlStateNormal];
        [self addSubview:self.incTextSize];
        
        self.decTextSize  = [[UIButton alloc] initWithFrame:CGRectMake(255, 80, 30, 30)];
        [self.decTextSize setImage: [UIImage imageNamed:@"decrease_textsize"] forState:UIControlStateNormal];
        [self addSubview:self.decTextSize];

        
        self.textColorLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 200, 300, 30)];
        self.textColorLabel.textColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
        self.textColorLabel.font = [UIFont fontWithName:@"Arial" size:22.0f];
        self.textColorLabel.text = @"Text Color";
        [self addSubview:self.textColorLabel];
        
        self.aNewImage = [[UIButton alloc] initWithFrame:CGRectMake(250, 20, 25, 25)];
        [self.aNewImage setImage:[UIImage imageNamed:@"add_text"] forState:UIControlStateNormal];
        
        [self addSubview:self.aNewImage];
        
        ColorView *colorView = [[ColorView alloc] initWithFrame:CGRectMake(130, 200, 170, 30)];
        colorView.ColorViewDelegate = self;
        [self addSubview:colorView];
        
        arrayData = [[NSArray alloc] initWithObjects:@"Helvetica",@"Georgia",@"Futura", nil];
        dropDownView = [[DropDownView alloc] initWithArrayData:arrayData cellHeight:30 heightTableView:800 paddingTop:-8 paddingLeft:-5 paddingRight:-40 refView:self.fontStyleLabel animation:BOTH openAnimationDuration:2 closeAnimationDuration:2];
        dropDownView.delegate = self;
        
        [self addSubview:dropDownView.view];
        [self.fontStyleLabel setText:[arrayData objectAtIndex:0]];
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.274 green:0.274 blue:0.274 alpha:1.0].CGColor);
    CGContextMoveToPoint(context, 0, 65);
    CGContextAddLineToPoint(context, 360, 65);
    CGContextMoveToPoint(context, 0, 125);
    CGContextAddLineToPoint(context, 360, 125);
    CGContextMoveToPoint(context, 0, 185);
    CGContextAddLineToPoint(context, 360, 185);
    CGContextStrokePath(context);
}
-(void) ColorViewChangeColor:(CGColorRef)_color{
    
    [FontDelegate TextColorChange:_color];
}
-(void)dropDownCellSelected:(NSInteger)returnIndex{
	
	[self.fontStyleLabel setText:[arrayData objectAtIndex:returnIndex]];
    [FontDelegate FontChangeForText:[arrayData objectAtIndex:returnIndex]];
	
}
-(void)arrowButtonTapped:(id)sender
{
    [dropDownView openAnimation];
    
}

@end
