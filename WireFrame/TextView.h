//
//  TextView.h
//  Layerz
//
//  Created by Raghavender  on 02/06/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PdfLayers;

@interface TextView : NSManagedObject

@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSNumber * textviewCoordinateX;
@property (nonatomic, retain) NSNumber * textviewCoordinateY;
@property (nonatomic, retain) NSString * textViewForLayer;
@property (nonatomic, retain) NSNumber * textviewIndex;
@property (nonatomic, retain) NSString * textViewStirng;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSString * fontName;
@property (nonatomic, retain) NSString * fontColor;
@property (nonatomic, retain) NSNumber * fontSize;
@property (nonatomic, retain) PdfLayers *layer;

@end
