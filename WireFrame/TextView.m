//
//  TextView.m
//  Layerz
//
//  Created by Raghavender  on 02/06/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "TextView.h"
#import "PdfLayers.h"


@implementation TextView

@dynamic height;
@dynamic textviewCoordinateX;
@dynamic textviewCoordinateY;
@dynamic textViewForLayer;
@dynamic textviewIndex;
@dynamic textViewStirng;
@dynamic width;
@dynamic fontName;
@dynamic fontColor;
@dynamic fontSize;
@dynamic layer;

@end
