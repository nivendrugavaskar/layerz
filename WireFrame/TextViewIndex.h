//
//  TextViewIndex.h
//  Layerz
//
//  Created by Raghavender  on 28/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TextViewIndex : NSObject
@property NSInteger textViewIndex;
@property NSInteger fontSize;
@property (strong, nonatomic) NSString *fontName;
@property (strong, nonatomic) NSString *fontColor;

@end
