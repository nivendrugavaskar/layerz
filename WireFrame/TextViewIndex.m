//
//  TextViewIndex.m
//  Layerz
//
//  Created by Raghavender  on 28/05/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "TextViewIndex.h"

@implementation TextViewIndex

@synthesize textViewIndex;
@synthesize fontSize;
@synthesize fontName;
@synthesize fontColor;

@end
