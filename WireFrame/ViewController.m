//
//  ViewController.m
//  WireFrame
//
//  Created by Raghavender  on 10/03/14.
//  Copyright (c) 2014 Raghavender . All rights reserved.
//

#import "ViewController.h"
#import "LibraryDirectoryView.h"
#import "LibraryDocumentsView.h"
#import "CoreDataManager.h"
#import "ReaderConstants.h"
#import "ReaderDocument.h"
#import "DocumentFolder.h"
#import "DocumentsUpdate.h"
#import "ReaderViewController.h"
#import "CGPDFDocument.h"

@interface ViewController () <UIScrollViewDelegate,LibraryDocumentsDelegate,ReaderViewControllerDelegate>


@end

@implementation ViewController
{
    UIScrollView *theScrollView;
    LibraryDocumentsView *documentsView;
    LibraryDirectoryView *directoryView;
    ReaderViewController *readerViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];

	CGRect viewRect = self.view.bounds; // View controller's view bounds
    
	theScrollView = [[UIScrollView alloc] initWithFrame:viewRect]; // All
	theScrollView.bounces = NO;
	theScrollView.scrollsToTop = NO;
	theScrollView.pagingEnabled = YES;
	theScrollView.delaysContentTouches = NO;
	theScrollView.showsVerticalScrollIndicator = NO;
	theScrollView.showsHorizontalScrollIndicator = NO;
	theScrollView.contentMode = UIViewContentModeRedraw;
	theScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	theScrollView.backgroundColor = [UIColor clearColor];
	theScrollView.userInteractionEnabled = YES;
	theScrollView.autoresizesSubviews = NO;
	theScrollView.delegate = self;
    
	[self.view addSubview:theScrollView];
    //[documentsView reloadDocumentsUpdated];// debugger is not going there and if comment code no effect
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
	//BOOL reload = NO;
    
	
		CGRect viewRect = theScrollView.bounds; // Initial view frame
		  
    // designing LibraryDocumentsView
        documentsView = [[LibraryDocumentsView alloc] initWithFrame:viewRect];
        
		documentsView.delegate = self;
        documentsView.ownViewController = self;
		
        [theScrollView addSubview:documentsView];  // Add
        
		//reload = YES; // Reload content views

		//if (reload == YES) // Reload views
        //{
            // fetching datas
		[directoryView reloadDirectory]; // debugger not going here
            
        DocumentFolder *folder = nil;
        
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; // User defaults
        
		NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
        
		NSPersistentStoreCoordinator *mainPSC = [mainMOC persistentStoreCoordinator]; // Main PSC
        // NSString *const kReaderSettingsCurrentFolder = @"CurrentFolder";
    
		NSString *folderURL = [userDefaults objectForKey:kReaderSettingsCurrentFolder]; // Folder
        
		if (folderURL != nil) // Show default folder saved in settings
		{
			NSURL *folderURI = [NSURL URLWithString:folderURL]; // Folder URI
            
			NSManagedObjectID *objectID = [mainPSC managedObjectIDForURIRepresentation:folderURI];
            
			if (objectID != nil)
                folder = (id)[mainMOC existingObjectWithID:objectID error:NULL];
		}
        
		if (folder == nil) // Show default documents folder
		{
			folder = [DocumentFolder folderInMOC:mainMOC type:DocumentFolderTypeSamples];
			NSString *folderURI = [[[folder objectID] URIRepresentation] absoluteString]; // Folder URI
            
			[userDefaults setObject:folderURI forKey:kReaderSettingsCurrentFolder]; // Default folder
		}
    
    if(folder)
    {
		assert(folder != nil);
        [documentsView reloadDocumentsWithFolder:folder]; // Show folder contents
        NSString *documentURL = [userDefaults objectForKey:kReaderSettingsCurrentDocument]; // Document
        
        if (documentURL != nil) // Show default document saved in user defaults
        {
            NSURL *documentURI = [NSURL URLWithString:documentURL]; // Document URI
            
            NSManagedObjectID *objectID = [mainPSC managedObjectIDForURIRepresentation:documentURI];
            
            if (objectID != nil) // We have a valid NSManagedObjectID to request a fetch of
            {
                ReaderDocument *document = (id)[mainMOC existingObjectWithID:objectID error:NULL];
                
                if ((document != nil) && ([document isKindOfClass:[ReaderDocument class]]))
                {
                    [self showReaderDocument:document]; // Show the document
                }
            }
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Please add pdf from itunes and restart your app once to update" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
		
	//}
}
- (void)showReaderDocument:(ReaderDocument *)document
{
	if (document.fileExistsAndValid == YES) // Ensure the file exists
	{
		CFURLRef fileURL = (__bridge CFURLRef)document.fileURL; // Document file URL
        
		if (CGPDFDocumentNeedsPassword(fileURL, document.password) == NO) // Nope
		{
            //TODO: Brettcvz - crashing because dismiss calls viewWillAppear on the old document, which isn't there anymore
            if (self.presentedViewController != nil) // Check for active view controller(s)
			{
				[self dismissViewControllerAnimated:NO completion:^{
                    
                }]; // Dismiss any view controller(s)
			}
            
			NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; // User defaults
            
			if ([userDefaults boolForKey:kReaderSettingsHideStatusBar] == YES) // Status bar hide setting
			{
				UIApplication *sharedApplication = [UIApplication sharedApplication]; // UIApplication
                
				if (sharedApplication.statusBarHidden == NO) // The status bar is visible so hide it
				{
					[sharedApplication setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
				}
			}
            
			readerViewController = nil; // Release any old ReaderViewController first
            
			readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
            
			readerViewController.delegate = self; // Set the ReaderViewController delegate to self
            
			readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
			readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
            
			[self presentViewController:readerViewController animated:NO completion:^{
                
                
            }];
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
		return UIInterfaceOrientationIsPortrait(interfaceOrientation);
	else
		return YES;
}

/*
 - (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
 {
 //if (isVisible == NO) return; // iOS present modal bodge
 }
 */

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    
}

#pragma mark LibraryDocumentsDelegate methods
- (void)documentsView:(LibraryDocumentsView *)documentsView didSelectReaderDocument:(ReaderDocument *)document
{
    NSLog(@"name:%@",document.fileURL);
    
	if (document.fileExistsAndValid == YES) // Ensure the file exists
	{
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; // User defaults
        
        // NSString *const kReaderSettingsHideStatusBar = @"HideStatusBar";
        
		if ([userDefaults boolForKey:kReaderSettingsHideStatusBar] == YES) // Status bar hide setting
		{
			[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
		}
        
		if (document.password == nil) // Only remember default documents that do not require a password
		{
			NSString *documentURI = [[[document objectID] URIRepresentation] absoluteString]; // Document URI
            //NSString *const kReaderSettingsCurrentDocument = @"CurrentDocument";
            
			[userDefaults setObject:documentURI forKey:kReaderSettingsCurrentDocument]; // Default document
		}
        
		readerViewController = nil; // Release any old ReaderViewController first
        
		readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        
		readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        
		readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
		readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        
		[self presentViewController:readerViewController animated:NO completion:^{
            
                      
        }];
	}
}
- (void)enableContainerScrollView:(BOOL)enabled
{
	theScrollView.scrollEnabled = enabled;
}
#pragma mark ReaderViewControllerDelegate methods

- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; // User defaults
    
	[userDefaults removeObjectForKey:kReaderSettingsCurrentDocument]; // Clear default document
    // new code
    //[userDefaults setObject:nil forKey:kReaderSettingsCurrentFolder];
    
    [userDefaults synchronize];
    
	if ([userDefaults boolForKey:kReaderSettingsHideStatusBar] == YES) // Status bar hide setting
	{
		[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
	}
    
	[self dismissViewControllerAnimated:NO completion:^{
        readerViewController = nil; // Release ReaderViewController
    }];
    
	[documentsView refreshRecentDocuments]; // Refresh if recent folder is visible
    // new code
    [self viewDidAppear:NO];
    
}




- (void)tappedInToolbar:(UIXToolbarView *)toolbar addFileButton:(UIButton *)button
{
    
   
    
    
   /* NSString *documentsPath = [DocumentsUpdate documentsPath]; // Documents path
     NSError *error;
     NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
     if (files == nil) {
     NSLog(@"Error reading contents of documents directory: %@", [error localizedDescription]);
     }
     
     for (NSString *file in files) {
     }
    
    
    NSString *filename = @"Apple.pdf";
    NSString *documentFilePath = [documentsPath stringByAppendingPathComponent:filename];
    NSString *ndaPath = [[NSBundle mainBundle] pathForResource:@"Apple" ofType:@"pdf"];
    [[NSFileManager defaultManager] copyItemAtPath:ndaPath toPath:documentFilePath error:nil];
    
    
    NSManagedObjectContext *mainMOC = [[CoreDataManager sharedInstance] mainManagedObjectContext];
    
    NSArray *documentList = [ReaderDocument allInMOC:mainMOC withName:filename];
    
    ReaderDocument *document = nil; // ReaderDocument object
    
    if (documentList.count > 0) // Document exists
    {
        document = [documentList objectAtIndex:0];
    }
    else // Insert the new document into the object store
    {
        document = [ReaderDocument insertInMOC:mainMOC name:filename path:documentsPath];
        
        [[CoreDataManager sharedInstance] saveMainManagedObjectContext]; // Save changes
    }
    [documentsView reloadDocumentsUpdated];*/
    
}
@end
